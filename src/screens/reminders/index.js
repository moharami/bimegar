
import React, {Component} from 'react';
import { View, TouchableOpacity, Text, TextInput,ImageBackground, Dimensions, ScrollView, BackHandler, Alert, AsyncStorage} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux';
import background from '../../assets/bg.png'
import Axios from 'axios'
    ;
export const url = 'http://bimegaronline.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import Icon from 'react-native-vector-icons/dist/Feather';
import FooterMenu from "../../components/footerMenu";
import Remind from "../../components/remind";
import AlertView from '../../components/modalMassage'
import LinearGradient from 'react-native-linear-gradient'

class Reminders extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            login: true,
            loading: true,
            modalVisible: false,
            reminders: []
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        if(this.props.profileBack) {
            Actions.pop({refresh: {refresh:Math.random()}});
        }
        else {
            Actions.insuranceBuy({openDrawer: this.props.openDrawer})
        }
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        AsyncStorage.getItem('token').then((info) => {
            const newInfo = JSON.parse(info);
            Axios.post('/request/remember/list', {user_id: newInfo.user_id}).then(response => {
                console.log('reminders ', response.data.data)
                this.setState({loading: false, reminders: response.data.data});
            })
            .catch((error) =>{
                // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                // this.setState({loading: false});
                this.setState({modalVisible: true, loading: false});

            });
        });
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        }
        else return (
            <View style={styles.container}>
                <LinearGradient
                    start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['rgb(0, 114, 255)', 'rgb(0, 128, 255)', 'rgb(0, 142, 255)']}>
                    <View style={styles.image} source={background}>
                        <View style={styles.imageRow}>
                            <TouchableOpacity onPress={() => Actions.addReminder()}>
                                <Icon name="plus" size={20} color="white" style={{paddingRight: 20}} />
                            </TouchableOpacity>
                            <Text style={styles.label}>یادآوری ها</Text>
                            <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                                <Icon name="menu" size={26} color="white"  />
                            </TouchableOpacity>
                        </View>
                    </View>
                </LinearGradient>
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>
                        {
                            this.state.reminders ? this.state.reminders.map((item, index)=> <Remind item={item} key={index} openDrawer={this.props.openDrawer} />
                            ): <Text style={{paddingTop: '50%'}}>یادآوری برای نمایش وجود ندارد</Text>
                        }
                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}
                            title='مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'
                        />
                    </View>
                </ScrollView>
                <FooterMenu active="profile" openDrawer={this.props.openDrawer}/>
                {/*<LinearGradient*/}
                    {/*start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['rgb(0, 114, 255)', 'rgb(0, 128, 255)', 'rgb(0, 142, 255)']}>*/}
                {/*<View style={[styles.TrapezoidStyle, {borderRightWidth: Dimensions.get('window').width}]} />*/}
                {/*</LinearGradient>*/}

            </View>

        );
    }
}
export default Reminders;