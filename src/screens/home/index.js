import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, BackAndroid, TextInput, ActivityIndicator, AsyncStorage, BackHandler, Alert} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bimegaronline.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import FooterMenu from "../../components/footerMenu/index";
// import {List, SearchBar} from "react-native-elements";
import SearchBar from 'react-native-material-design-searchbar';
import SlideItems from '../../components/slideItems';
import NewsItem from '../../components/newsItem';
import moment_jalaali from 'moment-jalaali'
import moment from 'moment'
import AlertView from '../../components/modalMassage'

class Home extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            loadingMore: false,
            active: 4,
            page: 2,
            catPage: 2,
            catId: null,
            searchedItem: [],
            search: false,
            searchResponse: false,
            text: '',
            moreAlert: false,
            modalVisible: false,
            empty: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.insuranceBuy({openDrawer: this.props.openDrawer})
        // Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    closeModal() {
        this.setState({modalVisible: false});
    }
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

        // if(this.props.loged) {
        //
        // }
        try {
            if (this.props.posts.blog) {
                this.setState({loading: false });
            }
            else {
                Axios.post('/request/blog/list').then(response => {
                    store.dispatch({type: 'USER_POSTS_FETCHED', payload: response.data.data.data});
                    Axios.post('/request/blog/special').then(response => {
                        store.dispatch({type: 'SPACIAL_POSTS_FETCHED', payload: response.data.data});
                        Axios.post('/request/blog/category').then(response => {
                            store.dispatch({type: 'CATEGORIES_POSTS_FETCHED', payload: response.data.data});
                            this.setState({loading: false });
                        })
                        .catch((error) => {
                            console.log(error);
                            // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                            // this.setState({loading: false});
                            this.setState({modalVisible: true, loading: false});

                        });
                    })
                    .catch((error) => {
                        console.log(error);
                        // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        // this.setState({loading: false});
                        this.setState({modalVisible: true, loading: false});
                    });
                })
                .catch((error) => {
                    console.log(error);
                    // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});

                });
            }
        }
        catch (error) {
            console.log(error);
            // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
            // this.setState({loading: false});
            this.setState({modalVisible: true, loading: false});

        }
    }
    getCategoryPost(id) {
        this.setState({loading: true, catId: id});
        Axios.post('/request/blog/post_cat', {category_id: id}).then(response => {
            store.dispatch({type: 'CATEGORY_POSTS_FETCHED', payload: response.data.data.data});
            console.log('categories',  response.data.data.data)
            this.setState({loading: false });
        })
        .catch((error) => {
            // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
            // this.setState({loading: false});
            this.setState({modalVisible: true, loading: false});

        });
    }
    handleLoadMore() {
        if(this.state.active === 4) {
            this.setState({loadingMore: true});
            Axios.post('/request/blog/list', {page: this.state.page}).then(response => {
                console.log(response.data.data.data);
                store.dispatch({type: 'NEW_POSTS_FETCHED', payload: response.data.data.data});
                this.setState({loadingMore: false, page: ++this.state.page });
            })
            .catch( (error) =>{
                console.log(error.response);
            });
        }
        else {
            console.log('catttttt more', this.props.categories)
            // try {
            console.log('cat id', this.state.catId)
            Axios.post('/request/blog/post_cat', {
                page: this.state.catPage,
                category_id: this.state.catId
            }).then(response=> {
                if( response.data.data !== "") {
                    store.dispatch({type: 'NEW_CATEGORIES_FETCHED', payload: response.data.data.data});
                    this.setState({loadingMore: false, page: ++this.state.catPage });

                }
                else {
                    // Alert.alert('','موارد بیشتری یافت نشد');
                    // this.setState({moreAlert: true, loadingMore: false, page: ++this.state.catPage });
                    this.setState({modalVisible: true, loading: false, moreAlert: true, loadingMore: false, page: ++this.state.catPage});

                }
            })
            .catch((error) => {
                // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                // this.setState({loadingMore: false});
                this.setState({modalVisible: true, loading: false});

            });
        }
    }
    startSearch(text) {
        if(text.length === 0) {
            this.setState({modalVisible: true, loading: false, empty: true});
        }
        else {
            this.setState({loading: true, search: true});
            Axios.get('/post/search/'+text).then(response=> {
                this.setState({loading: false, searchedItem: response.data.data.data, searchResponse: true});
            })
                .catch((error) => {
                    // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    // this.setState({loading: false, search: false});
                    this.setState({modalVisible: true, loading: false});

                });
        }
    }
    render() {
        const {posts, spacialPosts, headerCategories, categories} = this.props;
        console.log('latest category', this.props.categories)
        if(this.state.loading){
            return (<Loader />)
        } else return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.container}>
                        <View style={styles.top}>
                            <View style={{width: '80%', }}>

                                {/*<View style={{position: 'relative', zIndex: 0}}>*/}
                                    <TextInput
                                        maxLength={15}
                                        placeholder={'جستجو در بلاگ'}
                                        placeholderTextColor={'gray'}
                                        underlineColorAndroid='transparent'
                                        value={this.state.text}
                                        style={{
                                            height: 35,
                                            width: '90%',
                                            color: 'gray',
                                            fontSize: 14,
                                            textAlign: 'right',
                                            // elevation: 4,
                                            shadowColor: 'lightgray',
                                            borderRadius: 20,
                                            backgroundColor: 'white',
                                            elevation: 5,

                                            paddingRight: 10
                                        }}
                                        onChangeText={(text) => this.setState({text: text})}
                                    />
                                {/*<TouchableOpacity onPress={() => this.startSearch(this.state.text)} style={{position: 'absolute', zIndex: 880, top: 10, left: 50}}>*/}

                                {/*</View>*/}
                            </View>
                            <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                                <Icon name="bars" size={20} color="rgba(122, 130, 153, 1)" style={{paddingTop: 15}} />
                            </TouchableOpacity>
                        </View>
                        <ScrollView style={{ transform: [
                            { scaleX: -1}

                        ],}}
                                    horizontal={true} showsHorizontalScrollIndicator={false}
                        >
                            <View style={styles.catContainer}>
                                {
                                    this.props.headerCategories.map((item, index)=>
                                        <TouchableOpacity key={index} onPress={() => this.setState({active: item.id, search: false}, () => {this.getCategoryPost(item.id)})}>
                                            <View style={styles.navContainer}>
                                                <Text style={[styles.text, {color: this.state.active === item.id ? 'black': 'rgb(170, 170, 170)', fontSize: this.state.active === item.id ? 16 : 14}]}>{item.title}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    )
                                }
                                <TouchableOpacity onPress={() => this.setState({active: 4})}>
                                    <View style={styles.navContainer}>
                                        <Text style={[styles.text, {color: this.state.active === 4 ? 'black': 'rgb(170, 170, 170)', fontSize: this.state.active === 4 ? 16 : 14}]}>همه</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        {
                            this.state.search ? this.state.searchResponse && (this.state.searchedItem.length !== 0 ?
                                    <View style={{width: '100%'}}>
                                        <View style={styles.body}>
                                            {
                                                this.state.searchedItem.map((item, index)=> {return <NewsItem item={item} key={index} />})
                                            }
                                        </View>
                                    </View>
                                    : <Text style={{textAlign: 'center'}}>موردی برای نمایش وجود ندارد</Text>)

                                :(
                            this.state.active ===4 ?
                                <View style={{width: '100%'}}>
                                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ transform: [{ scaleX: -1}]}}>
                                        <View style={styles.slideContainers}>
                                            {
                                                this.props.posts ?
                                                    this.props.posts.map((item) => {return <SlideItems item={item} key={item.id}  />}): null
                                            }
                                        </View>
                                    </ScrollView>
                                    {
                                        this.props.posts ?
                                            <View style={styles.labelContainer}>
                                                <TouchableOpacity  onPress={() => Actions.latestNews({openDrawer: this.props.openDrawer, posts: this.props.posts})}>
                                                    <Text style={styles.seeMoreText}>مشاهده همه</Text>
                                                </TouchableOpacity>
                                                <Text style={styles.topLabel}>آخرین اخبار</Text>
                                            </View> : null
                                    }
                                    <View style={styles.body}>
                                        {
                                            this.props.posts ?
                                                this.props.posts.map((item) => {return <NewsItem item={item} key={item.id} />}): null
                                        }
                                    </View>
                                    {
                                        this.props.spacialPosts ?
                                            <View style={styles.labelContainer}>
                                                <TouchableOpacity onPress={() => Actions.importantNews({openDrawer: this.props.openDrawer, spacialPosts: this.props.spacialPosts})}>
                                                    <Text style={styles.seeMoreText}>مشاهده همه</Text>
                                                </TouchableOpacity>
                                                <Text style={styles.topLabel}>مهم ترین اخبار</Text>
                                            </View> : null
                                    }

                                    <View style={styles.body}>
                                        {
                                            this.props.spacialPosts ?
                                                this.props.spacialPosts.map((item) => {return <NewsItem item={item} key={item.id} hasCat={false} />}): null
                                        }
                                    </View>
                                    {/*{*/}
                                        {/*this.state.loadingMore ?*/}
                                            {/*<View style={{paddingBottom: 50}}>*/}
                                                {/*<ActivityIndicator*/}
                                                    {/*size='large'*/}
                                                    {/*animating={true}*/}
                                                    {/*color='red'*/}
                                                {/*/>*/}
                                            {/*</View>*/}
                                            {/*: null*/}
                                    {/*}*/}
                                </View>:

                                <View style={{width: '100%', paddingBottom: 200}} >
                                    <View style={styles.body}>
                                        {
                                            this.props.categories ?
                                                this.props.categories.map((item) => {return <NewsItem item={item} key={item.id} />}): null
                                        }
                                        <View style={{flexDirection: 'row', width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                        <TouchableOpacity  onPress={() => this.setState({loadingMore: true}, () => {this.handleLoadMore()})}>
                                            <Text style={{textAlign: 'center', color: 'rgb(39, 133, 228)'}}>مشاهده بیشتر</Text>
                                        </TouchableOpacity>
                                            {
                                                this.state.loadingMore ?
                                                    <View >
                                                        <ActivityIndicator
                                                            size={30}
                                                            animating={true}
                                                            color='rgb(39, 133, 228)'
                                                        />
                                                    </View>
                                                    : null
                                            }
                                        </View>
                                    </View>
                                </View>)
                        }
                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}
                            // onChange={() => this.setState({modalVisible: false})}
                            title={this.state.moreAlert ? 'موارد بیشتری یافت نشد' : (this.state.empty ? 'عبارت مورد نظر نباید خالی باشد' : 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید  ')}
                        />
                    </View>
                </ScrollView>
                <TouchableOpacity onPress={() => this.startSearch(this.state.text)} style={{position: 'absolute', zIndex: 9992, top: 25, left: 25}}>
                    <FIcon name="search" size={20} color="gray"  />
                </TouchableOpacity>
              <FooterMenu active="blog" openDrawer={this.props.openDrawer} />
            </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        posts: state.posts.posts,
        spacialPosts: state.posts.spacialPosts,
        headerCategories: state.posts.headerCategories,
        categories: state.posts.categories
    }
}
export default connect(mapStateToProps)(Home);
