
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bimegaronline.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import AlertView from '../../components/modalMassage'
import pic2 from '../../assets/insurances/2.png'
import pic3 from '../../assets/insurances/3.png'
import pic4 from '../../assets/insurances/4.png'
import pic5 from '../../assets/insurances/5.png'
import pic6 from '../../assets/insurances/6.png'
import pic7 from '../../assets/insurances/7.png'
import pic8 from '../../assets/insurances/8.png'
import pic9 from '../../assets/insurances/9.png'
import pic10 from '../../assets/insurances/10.png'
import pic11 from '../../assets/insurances/11.png'
import pic12 from '../../assets/insurances/12.png'
import pic13 from '../../assets/insurances/13.png'
import pic14 from '../../assets/insurances/14.png'
import pic15 from '../../assets/insurances/15.png'
import pic18 from '../../assets/insurances/18.png'
import pic19 from '../../assets/insurances/19.png'
import pic20 from '../../assets/insurances/20.png'
import pic21 from '../../assets/insurances/21.png'
import pic22 from '../../assets/insurances/22.png'
import pic23 from '../../assets/insurances/23.png'

class Comment extends Component {
    constructor(props){
        super(props);
        this.state = {
            status: 1,
            activeButton: 1,
            activeVacle: 1,
            showPicker: false,
            selectedStartDate: null,
            aboutData: '',
            data: null,

            text: '',
            row1Star1: false,
            row1Star2: false,
            row1Star3: false,
            row1Star4: false,
            row1Star5: false,

            row2Star1: false,
            row2Star2: false,
            row2Star3: false,
            row2Star4: false,
            row2Star5: false,

            row3Star1: false,
            row3Star2: false,
            row3Star3: false,
            row3Star4: false,
            row3Star5: false,
            loading: false,
            confirm: false,
            modalVisible: false,
            userId: null,
            wrongData: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.insuranceBuy({openDrawer: this.props.openDrawer})
        // Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    closeModal() {
        this.setState({modalVisible: false});
    }
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        // this.setState({loading: true});
        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);
                this.setState({userId: newInfo.user_id});
            }
        });
    }
    sendComment() {
        let numberOfrow1 = 0;
        if(this.state.row1Star1) {
            ++numberOfrow1;
        }
        if(this.state.row1Star2) {
            ++numberOfrow1;
        }
        if(this.state.row1Star3) {
            ++numberOfrow1;
        }
        if(this.state.row1Star4) {
            ++numberOfrow1;
        }
        if(this.state.row1Star5) {
            ++numberOfrow1;
        }


        let numberOfrow2 = 0;
        if(this.state.row2Star1) {
            ++numberOfrow2;
        }
        if(this.state.row2Star2) {
            ++numberOfrow2;
        }
        if(this.state.row2Star3) {
            ++numberOfrow2;
        }
        if(this.state.row2Star4) {
            ++numberOfrow2;
        }
        if(this.state.row2Star5) {
            ++numberOfrow2;
        }

        let numberOfrow3 = 0;
        if(this.state.row3Star1) {
            ++numberOfrow3;
        }
        if(this.state.row3Star2) {
            ++numberOfrow3;
        }
        if(this.state.row3Star3) {
            ++numberOfrow3;
        }
        if(this.state.row3Star4) {
            ++numberOfrow3;
        }
        if(this.state.row3Star5) {
            ++numberOfrow3;
        }
        console.log('id', numberOfrow1)
        console.log('id', numberOfrow2)
        console.log('id', numberOfrow3)
        console.log('user id', this.state.userId)


        this.setState({loading: true});
        Axios.post('/request/star', {
            text_content: this.state.text,
            star_price: numberOfrow1 ,
            star_payment: numberOfrow2,
            star_request: numberOfrow3,
            user_id: this.state.userId,
            insurance_id: this.props.insId,
            insurance_type_id: null

        }).then(response=> {
            this.setState({loading: false, modalVisible: true, confirm: true});
            AsyncStorage.removeItem('insuranceInfo');

                // this.setState({loading: false});
            // Alert.alert('نظر شما با موفقیت ثبت شد')
            // alert('موفق')
            console.log(response.data.data)
        })
        .catch((response) => {
        if(response.response.data.msg === 'ErorrInput') {
            this.setState({modalVisible: true, loading: false, wrongData: true});
        }
        else {
            this.setState({modalVisible: true, loading: false});
        }
            console.log('ffsdfsdf',response)
        });
    }
    render() {
        const id = this.props.insId;
        console.log('id', id)
        let imgSrc = null;
        switch (id) {
            case 2:
                imgSrc = pic2;
                break;
            case 3:
                imgSrc = pic3;
                break;
            case 4:
                imgSrc = pic4;
                break;
            case 5:
                imgSrc = pic5;
                break;
            case 6:
                imgSrc = pic6;
                break;
            case 7:
                imgSrc = pic7;
                break;
            case 8:
                imgSrc = pic8;
                break;
            case 9:
                imgSrc = pic9;
                break;
            case 10:
                imgSrc = pic10;
                break;
            case 11:
                imgSrc = pic11;
                break;
            case 12:
                imgSrc = pic12;
                break;
            case 13:
                imgSrc = pic13;
                break;
            case 14:
                imgSrc = pic14;
                break;
            case 15:
                imgSrc = pic5;
                break;
            case 18:
                imgSrc = pic18;
                break;
            case 19:
                imgSrc = pic19;
                break;
            case 20:
                imgSrc = pic20;
                break;
            case 21:
                imgSrc = pic21;
                break;
            case 22:
                imgSrc = pic22;
                break;
            case 23:
                imgSrc = pic23;
                break;
        }
        // const {posts, categories, user} = this.props;
        if(this.state.loading){
            return (<Loader />)
        }
        else return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgba(60, 177, 232, 1)', 'rgba(62, 64, 219, 1)']} style={styles.linearcontainer}>
                        <View style={styles.top}>
                            <TouchableOpacity onPress={() => this.onBackPress()}>
                                <FIcon name="arrow-left" size={20} color="white"  />
                            </TouchableOpacity>
                            <Text style={styles.headerTitle}>امتیاز دهی</Text>
                            <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                                <Icon name="bars" size={20} color="white" />
                            </TouchableOpacity>
                        </View>
                    </LinearGradient>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <View style={styles.headerContainer}>
                            <Text style={styles.headerText}>نظر و امتیاز</Text>
                        </View>
                        <Text style={styles.title}>امتیاز و نظر شما درباره {this.props.insTitle}</Text>
                        <View style={styles.info}>
                            <View style={styles.rowsContainer}>
                                <View style={styles.row}>
                                    <View style={{flexDirection: 'row'}}>
                                        <TouchableOpacity onPress={() => this.setState({row1Star1: !this.state.row1Star1})}>
                                            <Icon name={this.state.row1Star1 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.setState({row1Star2: !this.state.row1Star2})}>
                                            <Icon name={this.state.row1Star2 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.setState({row1Star3: !this.state.row1Star3})}>
                                            <Icon name={this.state.row1Star3 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.setState({row1Star4: !this.state.row1Star4})}>
                                            <Icon name={this.state.row1Star4 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.setState({row1Star5: !this.state.row1Star5})}>
                                            <Icon name={this.state.row1Star5 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                        </TouchableOpacity>
                                    </View>
                                    <Text style={styles.title}>قیمت</Text>
                                </View>
                                <View style={styles.row}>
                                    <View style={{flexDirection: 'row'}}>
                                        <TouchableOpacity onPress={() => this.setState({row2Star1: !this.state.row2Star1})}>
                                            <Icon name={this.state.row2Star1 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.setState({row2Star2: !this.state.row2Star2})}>
                                            <Icon name={this.state.row2Star2 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.setState({row2Star3: !this.state.row2Star3})}>
                                            <Icon name={this.state.row2Star3 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.setState({row2Star4: !this.state.row2Star4})}>
                                            <Icon name={this.state.row2Star4 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.setState({row2Star5: !this.state.row2Star5})}>
                                            <Icon name={this.state.row2Star5 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                        </TouchableOpacity>
                                    </View>
                                    <Text style={styles.title}>رضایت از پرداخت</Text>
                                </View>
                                <View style={styles.row}>
                                    <View style={{flexDirection: 'row'}}>
                                        <TouchableOpacity onPress={() => this.setState({row3Star1: !this.state.row3Star1})}>
                                            <Icon name={this.state.row3Star1 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.setState({row3Star2: !this.state.row3Star2})}>
                                            <Icon name={this.state.row3Star2 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.setState({row3Star3: !this.state.row3Star3})}>
                                            <Icon name={this.state.row3Star3 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.setState({row3Star4: !this.state.row3Star4})}>
                                            <Icon name={this.state.row3Star4 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.setState({row3Star5: !this.state.row3Star5})}>
                                            <Icon name={this.state.row3Star5 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                        </TouchableOpacity>
                                    </View>
                                    <Text style={styles.title}>پاسخگویی</Text>
                                </View>
                            </View>
                            <View style={styles.imageContainer}>
                                <Image source={imgSrc} style={{width: 50, height: 50, resizeMode: 'contain'}} />
                            </View>
                        </View>
                        <TextInput
                            multiline = {true}
                            numberOfLines = {4}
                            value={this.state.text}
                            onChangeText={(text) => this.setState({text: text})}
                            placeholderTextColor={'rgb(142, 142, 142)'}
                            underlineColorAndroid='transparent'
                            placeholder="نظر شما..."
                            style={{
                                // height: 45,
                                paddingRight: 15,
                                width: '90%',
                                fontSize: 14,
                                color: 'rgb(50, 50, 50)',
                                textAlign:'right',
                                fontFamily: 'IRANSansMobile(FaNum)',
                                borderWidth: 1,
                                borderColor: 'rgb(150, 150, 150)',
                                borderRadius: 10,
                                marginTop: 20,
                                marginBottom: 20
                            }}
                        />
                        <TouchableOpacity onPress={() => this.sendComment()} style={styles.advertise}>
                            <Text style={styles.buttonTitle}>تایید</Text>
                        </TouchableOpacity>
                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}
                            title={this.state.confirm ? 'نظر شما با موفقیت ثبت شد' :this.state.wrongData ? 'لطفا تمامی موارد را پر نمایید': 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید' }
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}
export default Comment;

