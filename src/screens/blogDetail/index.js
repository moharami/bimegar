
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, ImageBackground, AsyncStorage, BackHandler, Alert, Share, Dimensions,  Linking} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIIcon from 'react-native-vector-icons/dist/Feather';
import FIcon from 'react-native-vector-icons/dist/FontAwesome5';
import MIcon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios'
;
export const url = 'http://bimegaronline.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import FooterMenu from "../../components/footerMenu/index";
// import {List, SearchBar} from "react-native-elements";
import SearchBar from 'react-native-material-design-searchbar';
import NewsItem from '../../components/newsItem';
import HTML from 'react-native-render-html';
import LinearGradient from 'react-native-linear-gradient';
import moment_jalaali from 'moment-jalaali'

class BlogDetail extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            active: 1,
            share: true,
            // promptVisible: true
        };
        this.onBackPress = this.onBackPress.bind(this);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    shareCode() {
        console.log('https://telegram.me/share/url?url='+this.props.item.category.title+'-'+'http://bimegaronline.com/api/v1'+'/services/'+this.props.item.category.id)
        Share.share(
            {
                title:   "اشتراک مقالات بیمه بانی" ,
                message: 'http://bimegar.ir/blogs',
                url: 'http://bimegar.ir/blogs',
            },
        )
    }
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        // Alert.alert(
        //     'یادآوری',
        //     'آیا از حذف یادآوری مطمئن هستید؟',
        //     [
        //         {text: 'خیر', onPress: () => console.warn('NO Pressed'), style: 'cancel'},
        //         {text: 'بله', onPress: () => console.warn('YES Pressed')},
        //     ]
        // );
        // const {posts, categories, user} = this.props;
        const myHtml = '<p>' + this.props.item.content + '</p>';
        return (
            <View style={styles.container}>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        {
                            this.props.item.attachments[0] !== undefined ?
                                <ImageBackground source={{uri: 'https://bimegar.azarinpro.info/files?uid='+this.props.item.attachments[0].uid+'&width=350&height=350'}}
                                    style={styles.subImage}>
                                    <View style={{backgroundColor:'rgba(0,0,0,.4)',
                                        height: 270,
                                        width: "100%",
                                        padding: 20,
                                        flexDirection: 'row',
                                        alignItems: "flex-start",
                                        justifyContent: "space-between",
                                    }}>
                                        <TouchableOpacity onPress={() => this.onBackPress()}>
                                            <Icon name="angle-left" size={30} color="white" />
                                        </TouchableOpacity>
                                        <Icon name="heart" size={0} color="red" style={{paddingLeft: '7%'}} />
                                        <View style={styles.rightContainer}>
                                            {/*<Text style={styles.imageLabel}>اخبار بیمه</Text>*/}
                                            <Text style={styles.imageLabel}>{this.props.hasCat ? this.props.item.category.title : 'اخبار بیمه'}</Text>
                                        </View>
                                    </View>
                                </ImageBackground>
                                :
                                <ImageBackground source={{uri: 'http://jewel1067.com/wp-content/uploads/news.jpg'}}
                                                 style={styles.subImage}>
                                    <View style={{backgroundColor:'rgba(0,0,0,.4)',
                                        height: 270,
                                        width: "100%",
                                        padding: 20,
                                        flexDirection: 'row',
                                        alignItems: "flex-start",
                                        justifyContent: "space-between",
                                    }}>
                                        <TouchableOpacity onPress={() => this.onBackPress()}>
                                            <Icon name="angle-left" size={30} color="white" />
                                        </TouchableOpacity>
                                        <Icon name="heart" size={0} color="red" style={{paddingLeft: '7%'}} />
                                        <View style={styles.rightContainer}>
                                            {/*<Text style={styles.imageLabel}>اخبار بیمه</Text>*/}
                                            <Text style={styles.imageLabel}>{this.props.hasCat ? this.props.item.category.title : 'اخبار بیمه'}</Text>
                                        </View>
                                    </View>
                                </ImageBackground>

                        }
                        <View style={styles.body}>
                            <Text style={styles.headerText}>{this.props.item.title}</Text>
                            <View style={styles.advRow}>
                                <View style={styles.fContainer}>
                                    <Text style={styles.footerText}>{moment_jalaali(this.props.item.created_at).format('jYYYY/jM/jD')}</Text>
                                    {/*<Text style={styles.bodyText}>{this.props.item.category.title}</Text>*/}
                                    <Text style={styles.bodyText}>{this.props.hasCat ? this.props.item.category.title : 'اخبار بیمه'}</Text>

                                </View>
                            </View>
                            <HTML html={myHtml} tagsStyles={{
                                p: {
                                    direction: "rtl",
                                    fontFamily: 'IRANSansMobile(FaNum)',
                                    color: 'rgba(51, 54, 64, 1)',
                                    lineHeight: 30,
                                    paddingBottom: 10,
                                    paddingTop: 20,
                                    fontSize: 14
                                },
                                img: {
                                    width: 300,
                                    height: 200,
                                    marginRight: 'auto',
                                    marginLeft: 'auto'
                                }
                            }} />
                            {/*<View style={[styles.TrapezoidStyle, {borderRightWidth: Dimensions.get('window').width}]} />*/}
                        </View>
                    </View>
                </ScrollView>
                {
                    this.state.share ?
                <TouchableOpacity onPress={() => this.setState({share: false})} style={styles.shareBtn}>
                    <FIIcon name="share-2" size={20} color="white" />
                </TouchableOpacity>
                :
                    <View style={styles.footer}>
                        <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgba(60, 177, 232, 1)', 'rgba(62, 64, 219, 1)']} style={styles.footerMenu} >
                            <TouchableOpacity onPress={() => this.shareCode()}>
                                <Icon name="facebook-official" size={20} color="white" style={{paddingLeft: 15}} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.shareCode()}>
                                <Icon name="twitter" size={20} color="white" style={{paddingLeft: 15}} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() =>  this.shareCode()}>
                                <FIcon name="telegram-plane" size={20} color="white" style={{paddingLeft: 15}} />
                            </TouchableOpacity>
                            {/*<TouchableOpacity onPress={() =>  Linking.openURL('http://http://bimegaronline.com/api/v1/')}>*/}
                                {/*<FIcon name="instagram" size={20} color="white" style={{paddingLeft: 15}} />*/}
                            {/*</TouchableOpacity>*/}
                            <TouchableOpacity onPress={() => this.shareCode()} style={{paddingRight: '35%'}}>
                                <FIcon name="google-plus" size={20} color="white" style={{paddingLeft: 15}} />
                            </TouchableOpacity>
                            {/*<TouchableOpacity onPress={() => Linking.openURL('http://http://bimegaronline.com/api/v1/')}>*/}
                                {/*<Icon name="chain" size={20} color="white" style={{paddingLeft: 15}} />*/}
                            {/*</TouchableOpacity>*/}
                            <TouchableOpacity onPress={() => this.setState({share: true})} style={{paddingRight: 10}}>
                                <MIcon name="close" size={20} color="white" />
                            </TouchableOpacity>
                        </LinearGradient>
                    </View>
                }
            </View>
        );
    }
}
export default BlogDetail;
