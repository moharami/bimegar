import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, Picker, AsyncStorage,  BackHandler, Alert, KeyboardAvoidingView, Image, TextInput} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import EIcon from 'react-native-vector-icons/dist/EvilIcons';
import FIcon from 'react-native-vector-icons/dist/Feather';
import Axios from 'axios';
export const url = 'http://bimegaronline.com/api/v1';
Axios.defaults.baseURL = url;
import HomeHeader from "../../components/homeHeader/index";
import PersianCalendarPicker from 'react-native-persian-calendar-picker';
import moment_jalaali from 'moment-jalaali'
import {connect} from 'react-redux';
import AlertView from '../../components/modalMassage'
import MapView, { Marker } from 'react-native-maps';
import marker from '../../assets/marker.png'
import ModalFilterPicker from 'react-native-modal-filter-picker'
import me from '../../assets/me-my.png'
import help from '../../assets/help.png'
import Selectbox from 'react-native-selectbox'

class CustomerInfo extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading:true,
            loading2:false,
            status:1,
            showPicker:false,
            selectedStartDate:null,
            fname:this.props.user.fname,
            lname:this.props.user.lname,
            nationalId:this.props.user.national_id !== null ? this.props.user.national_id : '',
            province:0,
            town:0,
            address:"",
            tel:"",
            telCode:0,
            postalCode:"",
            new_province:0,
            new_provinceTitle:'',
            new_town:0,
            new_townTitle:'',
            new_address:"",
            new_tel:"",
            new_telCode:0,
            new_postalCode:"",
            reciver:"",
            new_reciver:"",
            nextStep:false,
            newAddress:false,
            activeBime:-1,
            activeUser:-1,
            mobile:this.props.user.mobile,
            new_mobile:"",
            // birthdayYear:this.props.birthdayYear,
            // birthdayMonth:this.props.birthdayMonth,
            // birthdayDay:this.props.birthdayDay,
            birthdayYear:this.props.birthdayYear !== '' ? {key: parseInt(this.props.birthdayYear), label: this.props.birthdayYear, value: this.props.birthdayYear} : {key: 0, label: 'سال', value: 0},
            birthdayMonth:this.props.birthdayMonth !== '' ? {key: parseInt(this.props.birthdayMonth), label: this.props.birthdayMonth, value: this.props.birthdayMonth} : {key: 0, label: 'ماه', value: 0},
            birthdayDay: this.props.birthdayDay !== '' ? {key: parseInt(this.props.birthdayDay), label: this.props.birthdayDay, value: this.props.birthdayDay} : {key: 0, label: 'روز', value: 0},
            provinces: [],
            city: [],
            modalVisible: false,
            nationalIdMassage: false,
            mobileMassage: false,
            postalMassage: false,
            redBorder: false,
            markers: [],
            lat: null,
            lng: null,
            region: null,
            stringFname: null,
            stringLname: null,
            provinceTitle: '',
            towns: [],
            townTitle: '',
            initialLat: 35.715298,
            initialLng: 51.404343,
            visibleinput2: false,
            visibleinput3: false,
            visibleinput5: false,
            visibleinput6: false,
            nationalIdValidation: false,
        };
        this.onBackPress = this.onBackPress.bind(this);
        this.handlePress = this.handlePress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    closeModal() {
        this.setState({modalVisible: false});
    }
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        Axios.get('/request/get_insurance').then(response => {
            this.setState({insurances: response.data.data});
            Axios.post('/request/get-province', {}).then(response => {
                // console.log('provinces', response.data.data)

                this.setState({loading: false, provinces: response.data.data});
                let towns = [];
                let res = response.data.data;
                res.map((item,index)=>towns.push(item.name))
                this.setState({towns: towns})
                // console.log('towns', towns)
            })
                .catch((error) => {
                    this.setState({modalVisible: true, loading: false});
                    console.log(error);
                });
        })
            .catch((error) => {
                this.setState({modalVisible: true, loading: false});
                console.log(error);
            });

    }
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false})}, 100);
        this.setState({ selectedStartDate: date });
    }
    getCat(id) {
        // console.log('provinceTitle', this.state.provinceTitle)
        this.setState({loading2:true});
        Axios.post('/request/get-city', {id: id}).then(response => {
            this.setState({city: response.data.data});
            // console.log('kind cars', response.data.data);
            // Axios.defaults.baseURL = '';
            // Axios.defaults.headers = {
            //     'Content-Type': 'application/json; charset=UTF-8',
            // };

            // console.log('MMMMAPPPPP', 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input='+ this.state.provinceTitle+'&inputtype=textquery&fields=geometry&key=AIzaSyACjNKR5CCk0nGL5uBGm2IME_c6FfML9eg');
            Axios.get('https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input='+this.state.provinceTitle+'&inputtype=textquery&fields=geometry&key=AIzaSyACjNKR5CCk0nGL5uBGm2IME_c6FfML9eg').then(response => {
                // console.log('res map', response)
                this.setState({loading2: false, initialLat: response.data.candidates[0].geometry.location.lat, initialLng: response.data.candidates[0].geometry.location.lng});
                // console.log('MMMMAPPPPP', 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input='+ this.state.provinceTitle+'&inputtype=textquery&fields=geometry&key=AIzaSyACjNKR5CCk0nGL5uBGm2IME_c6FfML9eg');
                // console.log('initialLat', this.state.initialLat);
                // console.log('initialLat', this.state.initialLng);
            })
                .catch((error) =>{
                    // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    // this.setState({loading2: false});
                    console.log(error)
                    this.setState({modalVisible: true, loading2: false});
                });
        })
            .catch((error) =>{
                // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                // this.setState({loading2: false});
                this.setState({modalVisible: true, loading2: false});
            });
    }
    test() {
        if(this.state.newAddress) {
            if(/^[-]?\d+$/.test(this.state.fname) === true){
                // console.log('(!/^[-]?+$/.test(this.state.fname)', /^[-]?\d+$/.test(this.state.fname))

                this.setState({nextStep: false, stringFname: <Text style={{color: 'red', fontSize: 10}}>نام نمی تواند مقدار عددی باشد</Text>});
            }
            else {
                this.setState({stringFname: null});
            }
            if(/^[-]?\d+$/.test(this.state.lname) === true){
                // console.log('!/^[-]?$/.test(this.state.lname', /^[-]?\d+$/.test(this.state.lname))
                this.setState({nextStep: false, stringLname: <Text style={{color: 'red', fontSize: 10}}>نام خانوادگی نمی تواند مقدار عددی باشد</Text>});
            }
            else {
                this.setState({stringLname: null});
            }
            // this.setState({stringLname: null, stringFname: null});
            if(this.state.new_postalCode !== "" && this.state.new_tel !== "" && this.state.new_address !== "" && this.state.new_town !== 0 && this.state.new_province !== 0 && this.state.fname !== "" && this.state.lname !== ""  && this.state.nationalId !== "" && this.state.birthdayYear.value !== 0 && this.state.birthdayMonth.value !== 0 &&  this.state.birthdayDay.value !== 0 &&  this.state.province !== 0  && this.state.town !== 0 && this.state.address !== "" && this.state.tel !== ""  && this.state.postalCode !== "" && this.state.stringFname === null && this.state.stringLname === null ) {
                this.setState({ nextStep: true });
            }
            else {
                this.setState({ nextStep: false});
            }
        }
        else{
            if(/^[-]?\d+$/.test(this.state.fname) === true){
                // console.log('(!/^[-]?+$/.test(this.state.fname)', /^[-]?\d+$/.test(this.state.fname))

                this.setState({stringFname: <Text style={{color: 'red', fontSize: 10}}>نام نمی تواند مقدار عددی باشد</Text>});
            }
            else {
                this.setState({stringFname: null});
            }
            if(/^[-]?\d+$/.test(this.state.lname) === true){
                // console.log('!/^[-]?$/.test(this.state.lname', /^[-]?\d+$/.test(this.state.lname))
                this.setState({stringLname: <Text style={{color: 'red', fontSize: 10}}>نام خانوادگی نمی تواند مقدار عددی باشد</Text>});
            }
            else {
                this.setState({stringLname: null});
            }
            // this.setState({stringLname: null, stringFname: null});
            if(this.state.nationalId !== "" && this.state.birthdayYear.value !== 0 && this.state.birthdayMonth.value !== 0 &&  this.state.birthdayDay.value !== 0 && this.state.province !== 0 && this.state.town !== 0 && this.state.address !== "" && this.state.tel !== "" && this.state.postalCode !== "" && this.state.stringFname === null && this.state.stringLname === null  ) {
                this.setState({ nextStep: true });
            }
            else {
                this.setState({ nextStep: false});
            }
        }
    }
    nextStep() {
        if(this.state.nextStep) {

            // this.isValidIranianNationalCode(this.state.nationalId);

            const input = this.state.nationalId;
            if (!/^\d{10}$/.test(input))
                this.setState({modalVisible: true, nationalIdMassage: true});
            let check = parseInt(input[9],10);
            let sum = [0, 1, 2, 3, 4, 5, 6, 7, 8]
                    .map(function (x) {
                        return parseInt(input[x],10) * (10 - x);
                    })
                    .reduce(function (x, y) {
                        return x + y;
                    }) % 11;

            if (!((sum < 2 && check === sum) || (sum >= 2 && check + sum === 11))){
                this.setState({modalVisible: true, nationalIdMassage: true});
            }
            else if(this.state.mobile.length !== 11) {
                this.setState({modalVisible: true, mobileMassage: true, nationalIdMassage: false});
            }
            else if(this.state.postalCode.length !== 10 || ( this.state.newAddress && this.state.new_postalCode.length !== 10)) {
                this.setState({modalVisible: true, postalMassage: true, nationalIdMassage: false});
            }
            else{
                if ((sum < 2 && check === sum) || (sum >= 2 && check + sum === 11)){
                    this.setState({nationalIdValidation: true}, () => {console.log('nationalIdValidation', this.state.nationalIdValidation)});
                }
                let stttr = "";
                let Birth = "";
                if(this.state.birthdayYear.value !== 0 && this.state.birthdayMonth.value !== 0 && this.state.birthdayDay.value  !== 0 ){
                    let Birth = this.state.birthdayYear.value+"/"+this.state.birthdayMonth.value +"/"+this.state.birthdayDay.value ;
                    // console.log('bbbbitrh',moment_jalaali(Birth, 'jYYYY/jM/jD').format('YYYY-M-D'))
                    // let stttr = moment_jalaali(Birth, 'jYYYY/jM/jD').format('YYYY-M-D')

                    stttr = moment_jalaali(Birth, 'jYYYY/jMM/jDD').format('YYYY-MM-DD')
                    stttr = stttr.replace(/۰/g, "0");
                    stttr = stttr.replace(/۱/g, "1");
                    stttr = stttr.replace(/۲/g, "2");
                    stttr = stttr.replace(/۳/g, "3");
                    stttr = stttr.replace(/۴/g, "4");
                    stttr = stttr.replace(/۵/g, "5");
                    stttr = stttr.replace(/۶/g, "6");
                    stttr = stttr.replace(/۷/g, "7");
                    stttr = stttr.replace(/۸/g, "8");
                    stttr = stttr.replace(/۹/g, "9");
                    // console.log('neW STTR ', stttr )
                }

                let factor = this.props.factor;
                this.props.user_details.fname=this.state.fname;
                this.props.user_details.lname=this.state.lname;
                this.props.user_details.national_id=this.state.nationalId;
                // this.props.user_details.birthday=moment(this.state.selectedStartDate).format("Y-M-D");
                // this.props.user_details.birthday=this.state.selectedStartDate;
                this.props.user_details.birthday=stttr;
                this.props.user_details.mobile=this.state.mobile;
                this.props.user_details.tel=this.state.tel;
                this.props.user_details.state=this.state.province;
                this.props.user_details.city=this.state.town;
                this.props.user_details.post_code=this.state.postalCode;
                this.props.user_details.address=this.state.address;
                // this.props.user_details.reciver=this.state.reciver;
                this.props.user_details.address_selected=this.state.activeBime;
                this.props.user_details.lat=this.state.lat;
                this.props.user_details.lng=this.state.lng;

                if(this.state.newAddress) {
                    this.props.user_details.new_state=this.state.new_province;
                    this.props.user_details.new_city=this.state.new_town;
                    this.props.user_details.new_tel=this.state.new_tel;
                    this.props.user_details.new_post_code=this.state.new_postalCode;
                    this.props.user_details.new_address=this.state.new_address;
                    // this.props.user_details.new_reciver=this.state.new_reciver;
                    this.props.user_details.new_mobile=this.state.new_mobile;
                }
                else {
                    this.props.user_details.new_state=this.state.province;
                    this.props.user_details.new_city=this.state.town;
                    this.props.user_details.new_tel=this.state.tel;
                    this.props.user_details.new_post_code=this.state.postalCode;
                    this.props.user_details.new_address=this.state.address;
                    // this.props.user_details.new_reciver=this.state.reciver;
                    this.props.user_details.new_mobile=this.props.user_details.mobile;
                }
                // console.log('ffff', factor)
                const bir = this.state.birthdayYear.value +'/'+this.state.birthdayMonth.value +'/'+this.state.birthdayDay.value ;
                Actions.pictureUpload({openDrawer: this.props.openDrawer, pageTitle: 'بارگذاری تصاویر', factor:factor, user_details:this.props.user_details, insurType:this.props.insurType, Birth:Birth, instalment: this.props.instalment, birth: bir})
            }
        }
        else {
            this.setState({ redBorder: true });
        }
        // this.state.nextStep && Actions.pictureUpload({openDrawer: this.props.openDrawer, pageTitle: 'بارگذاری تصاویر'})
    }

    handlePress(e) {
        // Axios.get('http://maps.google.com/maps/api/geocode/json?address=' + 'turkey').then(res =>   console.log(res));
        this.setState({
            markers: [
                {
                    coordinate: e.nativeEvent.coordinate,
                }
            ],
            lat: e.nativeEvent.coordinate.latitude,
            lng: e.nativeEvent.coordinate.longitude
        })
    }
    onShowinput2 = () => {
        this.setState({ visibleinput2: true });
    }

    onSelectInput2 = (picked) => {
        // console.log('picked', picked)
        this.setState({
            province: picked,
            visibleinput2: false
        }, () =>{
            // console.log('ppppicked', picked)
            this.state.provinces.map((item) => {
                if(picked === item.id) {
                    this.setState({provinceTitle: item.name}, () => {this.getCat(picked);console.log(this.state.provinceTitle)});
                }
            })
            this.test();
        })
    }
    onCancelinput2 = () => {
        this.setState({
            visibleinput2: false
        })
    }
    onShowinput3 = () => {
        this.setState({ visibleinput3: true });
    }

    onSelectInput3 = (picked) => {
        // console.log('picked', picked)
        this.setState({
            town: picked,
            visibleinput3: false
        }, () =>{
            // console.log('ppppicked', picked)
            this.state.city.map((item) => {
                if(picked === item.id) {
                    this.setState({townTitle: item.name}, () => {console.log(this.state.townTitle)});
                }
            })
            this.test();
        })
    }
    onCancelinput3 = () => {
        this.setState({
            visibleinput3: false
        })
    }
    onShowinput5 = () => {
        this.setState({visibleinput5: true });
    }
    onSelectInput5 = (picked) => {
        // console.log('picked', picked)
        this.setState({
            new_province: picked,
            visibleinput5: false
        }, () =>{
            // console.log('ppppicked', picked)
            this.state.provinces.map((item) => {
                if(picked === item.id) {
                    this.setState({new_provinceTitle: item.name}, () => {this.getCat(picked);console.log(this.state.new_provinceTitle)});
                }
            })
            this.test();
        })
    }
    onCancelinput5 = () => {
        this.setState({
            visibleinput5: false
        })
    }
    onShowinput6 = () => {
        this.setState({visibleinput6: true });
    }
    onSelectInput6 = (picked) => {
        // console.log('picked', picked)
        this.setState({
            new_town: picked,
            visibleinput6: false
        }, () =>{
            // console.log('ppppicked', picked)
            this.state.city.map((item) => {
                if(picked === item.id) {
                    this.setState({new_townTitle: item.name}, () => {console.log(this.state.new_townTitle)});
                }
            })
            this.test();
        })
    }
    onCancelinput6 = () => {
        this.setState({
            visibleinput6: false
        })
    }
    // isValidIranianNationalCode = (input) => {
    // if (!/^\d{10}$/.test(input))
    //     this.setState({modalVisible: true, nationalIdMassage: true});
    //     let check = parseInt(input[9],10);
    //     let sum = [0, 1, 2, 3, 4, 5, 6, 7, 8]
    //         .map(function (x) {
    //             return parseInt(input[x],10) * (10 - x);
    //         })
    //         .reduce(function (x, y) {
    //             return x + y;
    //         }) % 11;
    //
    //     if ((sum < 2 && check === sum) || (sum >= 2 && check + sum === 11)){
    //         this.setState({nationalIdValidation: true}, () => {console.log('nationalIdValidation', this.state.nationalIdValidation)});
    //     }
    //     else {
    //         this.setState({modalVisible: true, nationalIdMassage: true});
    //     }
    // }
    render() {
        const {user} = this.props;
        let yearArray = [], monthArray = [], dayArray = [];
        for(let i=1397; i>=1300 ; i--){
            yearArray.push({key: i, label: i.toString(), value: i})
        }
        const month = ["01", "02", "03", "04", "05", "06", "07", "08", "09","10", "11","12"]
        monthArray = month.map((item)=> {return {key: parseInt(item), label: item, value: item}})
        const day = ["01", "02", "03", "04", "05", "06", "07", "08", "09","10", "11","12", "13", "14", "15", "16", "17", "18", "19", "20", "21","22", "23","24", "25", "26", "27", "28", "29", "30", "31"];
        dayArray = day.map((item)=> {return {key: parseInt(item), label: item, value: item}})

        console.log('this.props.birthdayYear in customerinfo', this.props.birthdayYear)
        // const locations = [sshrgi:{lat:38.076805, lng: 46.235191 }, garbi:{lat: 37.552673, lng: 45.076046}, tehran:{lat: 35.715298, lng: 51.404343}, esfahan: {lat: 32.661343, lng: 51.680374}, khrazavi: {lat: 36.321247, lng: 59.532639}, shiraz: {lat: 29.591768, lng: 52.583698}, mashhad: {lat:36.310699, lng: 59.599457}, shhrekord: {lat: 32.326721, lng: 50.859081}, zanjan: {lat: 32.326721, lng: 50.859081}, kerman: {lat: 30.283937, lng: 57.083363}, yasoj: {lat:30.666813, lng: 51.595127}, birjand: {lat: 32.872379, lng: 59.221375}, gotgan: {lat: 36.841644, lng: 54.432922}]
        // const {posts, categories, user} = this.props;
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <View style={styles.header}>
                    <HomeHeader active={3} openDrawer={this.props.openDrawer} pageTitle={this.props.pageTitle} />
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <View style={{width: '100%', zIndex: 0, paddingBottom: 70}}>
                            <View style={styles.selectContainer}>
                                <TouchableOpacity onPress={() => this.setState({activeUser: -1, fname: this.props.user.fname, lname: this.props.user.lname, nationalId: this.props.user.national_id !== null ? this.props.user.national_id : '', birthdayYear: this.props.birthdayYear !== '' ? {key: parseInt(this.props.birthdayYear), label: this.props.birthdayYear, value: this.props.birthdayYear} : {key: 0, label: 'سال', value: 0} , birthdayMonth: this.props.birthdayMonth !== '' ? {key: parseInt(this.props.birthdayMonth), label: this.props.birthdayMonth, value: this.props.birthdayMonth} : {key: 0, label: 'ماه', value: 0} , birthdayDay: this.props.birthdayDay !== '' ? {key: parseInt(this.props.birthdayDay), label: this.props.birthdayDay, value: this.props.birthdayDay} : {key: 0, label: 'روز', value: 0} }, () => this.test())} style={[styles.addressContainer, {backgroundColor: this.state.activeUser === -1 ? '#38d0e6': 'rgb(237, 237, 237)', flexDirection: 'row', width: '48%', padding: 2 }]}>
                                    <Text style={[styles.addressLabel, {color: this.state.activeUser === -1 ? 'white': 'gray'}]}>مشخصات خودم</Text>
                                    <Image source={me} style={{width: 22, marginLeft: 3, resizeMode: 'contain'}} />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({activeUser: -2, fname: '', lname: '', nationalId: '', birthdayYear:  {key: 0, label: 'سال', value: 0}, birthdayMonth:  {key: 0, label: 'ماه', value: 0}, birthdayDay:  {key: 0, label: 'روز', value: 0}}, () => this.test())}  style={[styles.addressContainer, {backgroundColor: this.state.activeUser === -2 ? '#38d0e6': 'rgb(237, 237, 237)', flexDirection: 'row', width: '48%', padding: 2}]}>
                                    <Text style={[styles.addressLabel, {color: this.state.activeUser === -2 ? 'white': 'gray'}]}>مشخصات فرد دیگر </Text>
                                    <Image source={help} style={{width: 22, marginLeft: 3, resizeMode: 'contain'}} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.labelContainer}>
                                <Text style={styles.topLabel}>مشخصات فردی</Text>
                                <FIcon name="user" size={20} color="gray" />
                            </View>
                            <Text style={styles.text}>نام{this.state.stringFname}</Text>
                            {/*<View style={{position: 'relative', zIndex: 3, width: '100%', backgroundColor: 'yellow'}}>*/}
                            <View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10, borderColor: this.state.redBorder && (this.state.fname === '' || this.state.stringFname !== null) ? 'red' : (this.state.fname !== '' ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1}}>
                                <TextInput
                                    maxLength={12}
                                    keyboardType={'email-address'}
                                    placeholder={'نام'}
                                    placeholderTextColor={'gray'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.fname}
                                    style={{
                                        height: 40,
                                        paddingRight: 15,
                                        width: '100%',
                                        color: 'gray',
                                        fontSize: 14,
                                        textAlign: 'right',
                                        // elevation: 4,
                                    }}
                                    onChangeText={(text) => {this.setState({fname: text}, () => {this.test(); })}}
                                />
                            </View>

                            <Text style={styles.text}>نام خانوادگی {this.state.stringLname}</Text>
                            <View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10, borderColor: this.state.redBorder && (this.state.lname === '' || this.state.stringLname !== null) ? 'red' : (this.state.lname !== '' ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1}}>
                                <TextInput
                                    maxLength={16}
                                    placeholder={'نام خانوادگی'}
                                    placeholderTextColor={'gray'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.lname}
                                    style={{
                                        height: 40,
                                        paddingRight: 15,
                                        width: '100%',
                                        color: 'gray',
                                        fontSize: 14,
                                        textAlign: 'right',
                                    }}
                                    onChangeText={(text) => {this.setState({lname: text}, () => {this.test();})}}
                                />
                            </View>
                            <Text style={styles.text}>کد ملی</Text>
                            <View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10, borderColor: this.state.redBorder && this.state.nationalId === '' ? 'red' : (this.state.nationalId !== '' ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1}}>
                                <TextInput
                                    maxLength={10}
                                    keyboardType={"numeric"}
                                    placeholder="کد ملی"
                                    placeholderTextColor={'gray'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.nationalId}
                                    style={{
                                        height: 40,
                                        paddingRight: 15,
                                        width: '100%',
                                        color: 'gray',
                                        fontSize: 14,
                                        textAlign: 'right',

                                        // elevation: 4
                                    }}
                                    onChangeText={(text) => {this.setState({nationalId: text}, () => {this.test();})}}
                                />
                            </View>
                            <Text style={styles.text}>تاریخ تولد</Text>

                            <View style={{flexDirection: 'row', padding: 3, zIndex: 3, width: '100%', height: 42, borderColor: this.state.redBorder && (this.state.birthdayYear.value  === 0 || this.state.birthdayMonth.value  === 0 || this.state.birthdayDay.value  === 0) ? 'red' : (this.state.birthdayYear.value  !== 0 && this.state.birthdayMonth.value  !== 0 && this.state.birthdayDay.value  !== 0  ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1, borderRadius: 10,  backgroundColor: 'white'}}>
                                <Selectbox
                                    style={{width: '30%', height: 42, paddingTop: '3%', paddingRight: 10}}
                                    selectLabelStyle={{textAlign: 'right', color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                    optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                    selectedItem={this.state.birthdayYear}
                                    cancelLabel="لغو"
                                    onChange={(itemValue) =>{
                                        this.setState({
                                            birthdayYear: itemValue
                                        }, () => {
                                            this.test();
                                        })}}
                                    items={yearArray} />
                                <Selectbox
                                    style={{width: '30%', height: 42, paddingTop: '3%', paddingRight: 10}}
                                    selectLabelStyle={{textAlign: 'right', color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                    optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                    selectedItem={this.state.birthdayMonth}
                                    cancelLabel="لغو"
                                    onChange={(itemValue) =>{
                                        this.setState({
                                            birthdayMonth: itemValue
                                        }, () => {
                                            this.test();
                                        })}}
                                    items={monthArray} />
                                <Selectbox
                                    style={{width: '30%', height: 42, paddingTop: '3%', paddingRight: 10}}
                                    selectLabelStyle={{textAlign: 'right', color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                    optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                    selectedItem={this.state.birthdayDay}
                                    cancelLabel="لغو"
                                    onChange={(itemValue) =>{
                                        this.setState({
                                            birthdayDay: itemValue
                                        }, () => {
                                            this.test();
                                        })}}
                                    items={dayArray} />
                            </View>

                            {/*<Text style={styles.text}>نام تحویل گیرنده</Text>*/}
                            {/*<View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10, borderColor: this.state.reciver !== '' ? 'rgb(50, 197, 117)' : 'transparent', borderWidth: 1}}>*/}
                            {/*<TextInput*/}
                            {/*placeholder="نام تحویل گیرنده"*/}
                            {/*placeholderTextColor={'gray'}*/}
                            {/*underlineColorAndroid='transparent'*/}
                            {/*value={this.state.reciver}*/}
                            {/*style={{*/}
                            {/*height: 40,*/}
                            {/*paddingRight: 15,*/}
                            {/*width: '100%',*/}
                            {/*color: 'gray',*/}
                            {/*fontSize: 14,*/}
                            {/*textAlign: 'right',*/}
                            {/*}}*/}
                            {/*onChangeText={(text) => {this.setState({reciver: text}, () => {this.test();})}}*/}
                            {/*/>*/}
                            {/*</View>*/}

                            <View style={[styles.labelContainer, {paddingTop: 50}]}>
                                <Text style={styles.topLabel}>آدرس ارسال بیمه نامه </Text>
                                <EIcon name="location" size={20} color="black" />
                            </View>

                            <Text style={styles.text}>استان</Text>
                            <TouchableOpacity onPress={this.onShowinput2} style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderColor: this.state.redBorder && this.state.province === 0 ? 'red' : (this.state.province !== 0 ? 'rgb(50, 197, 117)' : '#ccc') , borderWidth: 1, borderRadius: 10}}>
                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                <Text style={{paddingTop: 5, paddingRight: 10,  fontFamily: 'IRANSansMobile(FaNum)', color: 'gray' }}>{this.state.loading ? "در حال بارگذاری..." : this.state.province === 0 ? "انتخاب نشده" : this.state.provinceTitle}</Text>
                                <ModalFilterPicker
                                    visible={this.state.visibleinput2}
                                    onSelect={this.onSelectInput2}
                                    onCancel={this.onCancelinput2}
                                    options={ this.state.provinces.map((item) => {return {key: item.id, label: item.name, value: item.id}})}
                                    placeholderText="جستجو ..."
                                    cancelButtonText="لغو"
                                    filterTextInputStyle={{textAlign: 'right', fontFamily: 'IRANSansMobile(FaNum)'}}
                                    optionTextStyle={{textAlign: 'right', width: '100%', fontFamily: 'IRANSansMobile(FaNum)'}}
                                    titleTextStyle={{textAlign: 'right', fontFamily: 'IRANSansMobile(FaNum)'}}
                                />
                            </TouchableOpacity>

                            <Text style={styles.text}>شهر</Text>
                            <TouchableOpacity onPress={this.onShowinput3} style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderColor: this.state.redBorder && this.state.town === 0 ? 'red' : (this.state.town !== 0 ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1, borderRadius: 10}}>
                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                <Text style={{paddingTop: 5, paddingRight: 10,  fontFamily: 'IRANSansMobile(FaNum)', color: 'gray' }}>{this.state.loading2 ? "در حال بارگذاری..." : this.state.town === 0 ? "انتخاب نشده" : this.state.townTitle}</Text>
                                <ModalFilterPicker
                                    visible={this.state.visibleinput3}
                                    onSelect={this.onSelectInput3}
                                    onCancel={this.onCancelinput3}
                                    options={ this.state.city.map((item) => {return {key: item.id, label: item.name, value: item.id}})}
                                    placeholderText="جستجو ..."
                                    cancelButtonText="لغو"
                                    filterTextInputStyle={{textAlign: 'right', fontFamily: 'IRANSansMobile(FaNum)'}}
                                    optionTextStyle={{textAlign: 'right', width: '100%', fontFamily: 'IRANSansMobile(FaNum)'}}
                                    titleTextStyle={{textAlign: 'right', fontFamily: 'IRANSansMobile(FaNum)'}}
                                />
                            </TouchableOpacity>
                            <Text style={styles.text}>آدرس</Text>
                            <View style={{position: 'relative', zIndex: 3, width: '100%', height: 42, borderColor: this.state.redBorder && this.state.address === "" ? 'red' : (this.state.address !== '' ? 'rgb(50, 197, 117)' : '#ccc'),  borderRadius: 10, backgroundColor: 'white',  borderWidth: 1}}>
                                <TextInput
                                    maxLength={200}
                                    placeholder="آدرس"
                                    placeholderTextColor={'gray'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.address}
                                    style={{
                                        height: 42,
                                        paddingRight: 15,
                                        width: '100%',
                                        color: 'gray',
                                        fontSize: 14,
                                        textAlign: 'right',
                                    }}
                                    onChangeText={(text) => { this.setState({address: text}, () => this.test())}}
                                />
                            </View>
                            <Text style={styles.text}>شماره تلفن</Text>
                            <View style={{position: 'relative', zIndex: 3, width: '100%', height: 42, borderColor: this.state.redBorder && this.state.tel === "" ? 'red' : (this.state.tel !== '' ? 'rgb(50, 197, 117)' : '#ccc'),  borderRadius: 10, backgroundColor: 'white',  borderWidth: 1}}>
                                <TextInput
                                    maxLength={11}
                                    keyboardType={"numeric"}
                                    placeholder="تلفن ثابت"
                                    placeholderTextColor={'gray'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.tel}
                                    style={{
                                        height: 40,
                                        paddingRight: 15,
                                        width: '100%',
                                        color: 'gray',
                                        fontSize: 14,
                                        textAlign: 'right',
                                    }}
                                    onChangeText={(text) => {this.setState({tel: text}, () => this.test())}}
                                />
                            </View>

                            <Text style={styles.text}>موبایل</Text>
                            <View style={{position: 'relative', zIndex: 3, width: '100%', height: 42, borderColor: this.state.redBorder && this.state.mobile === "" ? 'red' : (this.state.mobile !== '' ? 'rgb(50, 197, 117)' : '#ccc'),  borderRadius: 10, backgroundColor: 'white',  borderWidth: 1}}>
                                <TextInput
                                    maxLength={11}
                                    keyboardType={"numeric"}
                                    placeholder={user.mobile}
                                    placeholderTextColor={'gray'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.mobile}
                                    style={{
                                        height: 40,
                                        paddingRight: 15,
                                        width: '100%',
                                        color: 'gray',
                                        fontSize: 14,
                                        textAlign: 'right',
                                    }}
                                    onChangeText={(text) => {this.setState({mobile: text}, () => this.test())}}
                                />
                            </View>
                            <Text style={styles.text}>کد پستی</Text>
                            <View style={{position: 'relative', zIndex: 3, width: '100%', height: 42, borderColor: this.state.redBorder && this.state.postalCode === "" ? 'red' : (this.state.postalCode !== '' ? 'rgb(50, 197, 117)' : '#ccc'),  borderRadius: 10, backgroundColor: 'white',  borderWidth: 1}}>
                                <TextInput
                                    maxLength={10}
                                    keyboardType={"numeric"}
                                    placeholder="کد پستی"
                                    placeholderTextColor={'gray'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.postalCode}
                                    style={{
                                        height: 40,
                                        paddingRight: 15,
                                        width: '100%',
                                        color: 'gray',
                                        fontSize: 14,
                                        textAlign: 'right',
                                    }}
                                    onChangeText={(text) => { this.setState({postalCode: text}, () => this.test())}}
                                />
                            </View>
                            {/*<Text style={styles.text}>مکان تحویل بیمه نامه را بر روی نقشه پیدا کنید و کافی است فقط روی نقشه کلیک کنید.</Text>*/}
                            {/*<MapView*/}
                            {/*style={styles.mapStyle}*/}
                            {/*region={{*/}
                            {/*latitude: this.state.initialLat,*/}
                            {/*longitude: this.state.initialLng,*/}
                            {/*latitudeDelta: 0.0922,*/}
                            {/*longitudeDelta: 0.0421,*/}
                            {/*}}*/}
                            {/*onPress={this.handlePress}*/}
                            {/*>*/}
                            {/*{this.state.markers.map((marker, index) => {*/}
                            {/*return (*/}
                            {/*<Marker key={index} {...marker} >*/}
                            {/*<Image source={marker} style={{width: 50, height: 50, resizeMode: 'contain'}} />*/}
                            {/*</Marker>*/}
                            {/*)*/}
                            {/*})}*/}
                            {/*</MapView>*/}
                            <View style={[styles.labelContainer, {paddingTop: 50, paddingBottom: 10}]}>
                                <Text style={styles.topLabel}>آدرس درج روی بیمه نامه </Text>
                                <EIcon name="location" size={20} color="black" />
                            </View>
                            <View style={styles.selectContainer}>
                                <TouchableOpacity onPress={() => this.setState({activeBime: -1, newAddress: false}, () => this.test())} style={[styles.addressContainer, {backgroundColor: this.state.activeBime === -1 ? '#38d0e6': 'rgb(237, 237, 237)', padding: 10 }]}>
                                    <Text style={[styles.addressLabel, {color: this.state.activeBime === -1 ? 'white': 'gray'}]}>همان آدرس ارسال باشد</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({activeBime: -2, newAddress: false}, () => this.test())}  style={[styles.addressContainer, {backgroundColor: this.state.activeBime === -2 ? '#38d0e6': 'rgb(237, 237, 237)', padding: 10 }]}>
                                    <Text style={[styles.addressLabel, {color: this.state.activeBime === -2 ? 'white': 'gray'}]}>مطابق بیمه نامه قبلی باشد </Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({activeBime: 1, newAddress: true }, () => this.test())}  style={[styles.addressContainer, {backgroundColor: this.state.activeBime === 1 ? '#38d0e6': 'rgb(237, 237, 237)',padding: 10 }]}>
                                    <Text style={[styles.addressLabel, {color: this.state.activeBime === 1 ? 'white': 'gray'}]}>آدرس جدید وارد میکنم </Text>
                                </TouchableOpacity>
                            </View>
                            {
                                this.state.newAddress ?
                                    <View>
                                        <Text style={styles.text}>استان</Text>
                                        <TouchableOpacity onPress={this.onShowinput5} style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderColor: this.state.redBorder && this.state.new_province === 0 ? 'red' : (this.state.new_province !== 0 ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1, borderRadius: 10}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                            <Text style={{paddingTop: 5, paddingRight: 10,  fontFamily: 'IRANSansMobile(FaNum)', color: 'gray' }}>{this.state.loading ? "در حال بارگذاری..." : this.state.new_province === 0 ? "انتخاب نشده" : this.state.new_provinceTitle}</Text>
                                            <ModalFilterPicker
                                                visible={this.state.visibleinput5}
                                                onSelect={this.onSelectInput5}
                                                onCancel={this.onCancelinput5}
                                                options={ this.state.provinces.map((item) => {return {key: item.id, label: item.name, value: item.id}})}
                                                placeholderText="جستجو ..."
                                                cancelButtonText="لغو"
                                                filterTextInputStyle={{textAlign: 'right', fontFamily: 'IRANSansMobile(FaNum)'}}
                                                optionTextStyle={{textAlign: 'right', width: '100%', fontFamily: 'IRANSansMobile(FaNum)'}}
                                                titleTextStyle={{textAlign: 'right', fontFamily: 'IRANSansMobile(FaNum)'}}
                                            />
                                        </TouchableOpacity>
                                        <Text style={styles.text}>شهر</Text>
                                        <TouchableOpacity onPress={this.onShowinput6} style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderColor: this.state.redBorder && this.state.new_town === 0 ? 'red' : (this.state.new_town !== 0 ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1, borderRadius: 10}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                            <Text style={{paddingTop: 5, paddingRight: 10,  fontFamily: 'IRANSansMobile(FaNum)', color: 'gray' }}>{this.state.loading2 ? "در حال بارگذاری..." : this.state.new_town === 0 ? "انتخاب نشده" : this.state.new_townTitle}</Text>
                                            <ModalFilterPicker
                                                visible={this.state.visibleinput6}
                                                onSelect={this.onSelectInput6}
                                                onCancel={this.onCancelinput6}
                                                options={ this.state.city.map((item) => {return {key: item.id, label: item.name, value: item.id}})}
                                                placeholderText="جستجو ..."
                                                cancelButtonText="لغو"
                                                filterTextInputStyle={{textAlign: 'right', fontFamily: 'IRANSansMobile(FaNum)'}}
                                                optionTextStyle={{textAlign: 'right', width: '100%', fontFamily: 'IRANSansMobile(FaNum)'}}
                                                titleTextStyle={{textAlign: 'right', fontFamily: 'IRANSansMobile(FaNum)'}}
                                            />
                                        </TouchableOpacity>
                                        <Text style={styles.text}>آدرس</Text>
                                        <View style={{position: 'relative', zIndex: 3, width: '100%', height: 42, borderColor: this.state.redBorder && this.state.new_address === '' ? 'red' : (this.state.new_address !== '' ? 'rgb(50, 197, 117)' : '#ccc'), borderRadius: 10, backgroundColor: 'white',  borderWidth: 1}}>
                                            <TextInput
                                                maxLength={200}
                                                placeholder="آدرس"
                                                placeholderTextColor={'gray'}
                                                underlineColorAndroid='transparent'
                                                value={this.state.new_address}
                                                style={{
                                                    height: 40,
                                                    paddingRight: 15,
                                                    width: '100%',
                                                    color: 'gray',
                                                    fontSize: 14,
                                                    textAlign: 'right',
                                                }}
                                                onChangeText={(text) => { this.setState({new_address: text}, () => this.test())}}
                                            />
                                        </View>
                                        <Text style={styles.text}>شماره تلفن</Text>
                                        <View style={{position: 'relative', zIndex: 3, width: '100%', height: 42, borderColor: this.state.redBorder && this.state.new_tel === '' ? 'red' : (this.state.new_tel !== '' ? 'rgb(50, 197, 117)' : '#ccc'),  borderRadius: 10, backgroundColor: 'white',  borderWidth: 1}}>
                                            <TextInput
                                                maxLength={11}
                                                keyboardType={"numeric"}
                                                placeholder="تلفن ثابت"
                                                placeholderTextColor={'gray'}
                                                underlineColorAndroid='transparent'
                                                value={this.state.new_tel}
                                                style={{
                                                    height: 40,
                                                    paddingRight: 15,
                                                    width: '100%',
                                                    color: 'gray',
                                                    fontSize: 14,
                                                    textAlign: 'right',
                                                }}
                                                onChangeText={(text) => {this.setState({new_tel: text}, () => this.test())}}
                                            />
                                        </View>
                                        <Text style={styles.text}>موبایل</Text>
                                        <View style={{position: 'relative', zIndex: 3, width: '100%', height: 42,borderColor: this.state.redBorder && this.state.new_mobile === '' ? 'red' : (this.state.new_mobile !== '' ? 'rgb(50, 197, 117)' : '#ccc'),  borderRadius: 10, backgroundColor: 'white',  borderWidth: 1}}>
                                            <TextInput
                                                maxLength={11}
                                                keyboardType={"numeric"}
                                                placeholder="موبایل"
                                                placeholderTextColor={'gray'}
                                                underlineColorAndroid='transparent'
                                                value={this.state.new_mobile}
                                                style={{
                                                    height: 40,
                                                    paddingRight: 15,
                                                    width: '100%',
                                                    color: 'gray',
                                                    fontSize: 14,
                                                    textAlign: 'right',
                                                }}
                                                onChangeText={(text) => {this.setState({new_mobile: text}, () => {this.test();})}}
                                            />
                                        </View>
                                        <Text style={styles.text}>کد پستی</Text>
                                        <View style={{position: 'relative', zIndex: 3, width: '100%', height: 42,borderColor: this.state.redBorder && this.state.new_postalCode === '' ? 'red' : (this.state.new_postalCode !== '' ? 'rgb(50, 197, 117)' : '#ccc'),  borderRadius: 10, backgroundColor: 'white',  borderWidth: 1}}>
                                            <TextInput
                                                maxLength={10}
                                                keyboardType={"numeric"}
                                                placeholder="کد پستی"
                                                placeholderTextColor={'gray'}
                                                underlineColorAndroid='transparent'
                                                value={this.state.new_postalCode}
                                                style={{
                                                    height: 40,
                                                    paddingRight: 15,
                                                    width: '100%',
                                                    color: 'gray',
                                                    fontSize: 14,
                                                    textAlign: 'right',
                                                }}
                                                onChangeText={(text) => {this.setState({new_postalCode: text}, () => {this.test();})}}
                                            />
                                        </View>
                                        {/*<Text style={styles.text}>نام تحویل گیرنده</Text>*/}
                                        {/*<View style={{position: 'relative', zIndex: 3, width: '100%', height: 42,  borderRadius: 10, backgroundColor: 'white',  borderColor: this.state.new_reciver !== '' ? 'rgb(50, 197, 117)' : '#ccc', borderWidth: 1}}>*/}
                                        {/*<TextInput*/}
                                        {/*maxLength={15}*/}
                                        {/*placeholder="نام تحویل گیرنده"*/}
                                        {/*placeholderTextColor={'gray'}*/}
                                        {/*underlineColorAndroid='transparent'*/}
                                        {/*value={this.state.new_reciver}*/}
                                        {/*style={{*/}
                                        {/*height: 40,*/}
                                        {/*paddingRight: 15,*/}
                                        {/*width: '100%',*/}
                                        {/*color: 'gray',*/}
                                        {/*fontSize: 14,*/}
                                        {/*textAlign: 'right',*/}
                                        {/*}}*/}
                                        {/*onChangeText={(text) => {this.setState({new_reciver: text})}}*/}
                                        {/*/>*/}
                                        {/*</View>*/}
                                    </View>
                                    : false
                            }
                        </View>
                        {
                            this.state.showPicker ?
                                <View style={{
                                    position: 'absolute',
                                    bottom: '65%',
                                    zIndex: 9999,
                                    backgroundColor: 'white'
                                }}>
                                    <PersianCalendarPicker
                                        onDateChange={(date) => this.onDateChange(date)}
                                    />
                                </View>
                                : null
                        }
                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}
                            onChange={() => this.setState({modalVisible: false})}

                            title={this.state.nationalIdMassage ? 'لطفا کد ملی را صحیح وارد کنید' :(this.state.mobileMassage ? 'لطفا شماره موبایل را درست وارد کنید':(this.state.postalMassage ? 'کد پستی باید 10 رقمی باشد' : 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید' ) )}
                        />
                    </View>
                </ScrollView>
                <View style={styles.footer}>
                    <TouchableOpacity onPress={() => this.nextStep()} style={[styles.iconLeftContainer, {backgroundColor: this.state.nextStep ? 'rgba(255, 193, 39, 1)' : 'rgba(200, 200, 200, 1)' }]}>
                        <FIcon name="arrow-left" size={18} color="white" style={{borderColor: 'white', borderWidth: 1, borderRadius: 20, padding: 3, marginRight: 5,}} />
                        <Text style={styles.label}>بعدی</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.iconRightContainer} onPress={() => this.onBackPress()}>
                        <FIcon name="arrow-right" size={18} color="rgba(17, 103, 253, 1)" />
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        );
    }
}
function mapStateToProps(state) {
    return {
        user: state.auth.user,
    }
}
export default connect(mapStateToProps)(CustomerInfo);
