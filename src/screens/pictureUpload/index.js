
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import SIcon from 'react-native-vector-icons/dist/SimpleLineIcons';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'https://bimegaronline.com/api/v1';
Axios.defaults.baseURL = url;
import HomeHeader from "../../components/homeHeader/index";
import PictureInfo from '../../components/pictureInfo'

class PictureUpload extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            status: 1,
            activeButton: 1,
            activeVacle: 1,
            showPicker: false,
            selectedStartDate: null,
            src1: "",
            src2: "",
            src3: "",
            src4: "",
            src5: "",
            nextStep: false,
            temp: [],
            temp2: [],
            insurances: []
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillMount(){
        // console.log('here is pppppicturrrrrrrrrrrrrrrrrrre upload ');
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    pickerImage(src, id) {
        if(id === 1) {
            this.setState({src1: src})
            // console.log(src)
        }
        else if(id === 2) {
            this.setState({src2: src})
        }
        else if(id === 3) {
            this.setState({src3: src})
        }
        else if (id === 4) {
            this.setState({src4: src})
        }
        else if (id === 5) {
            this.setState({src5: src})
        }
    }
    test() {
        if(this.props.insurType === 'body' || this.props.insurType === 'motor') {
            if(this.state.src1 !== "" && this.state.src2 !== "" && this.state.src5 !== "") {
                this.setState({nextStep: true})
            }
        }
        if(this.props.insurType === 'Third') {
            if(this.state.src1 !== "" && this.state.src2 !== "") {
                this.setState({nextStep: true})
            }
        }
        else if(this.props.insurType === 'fire' || this.props.insurType === 'earth') {
            if(this.state.src1 !== "") {
                this.setState({nextStep: true})
            }
        }
        else if(this.props.insurType === 'complete') {
            if(this.state.src4 !== "") {
                this.setState({nextStep: true})
            }
        }
        else if(this.props.insurType === 'travel') {
            if(this.state.src1 !== "") {
                this.setState({nextStep: true})
            }
        }
        else if(this.props.insurType === 'responsible') {
            if(this.state.src3 !== "") {
                this.setState({nextStep: true})
            }
        }
        else if(this.props.insurType === 'life') {
            if(this.state.src1 !== "") {
                this.setState({nextStep: true})
            }
        }
    }
    nextStep() {
        if(this.state.nextStep) {
            let images = null;
            if(this.props.insurType === 'body' || this.props.insurType === 'motor') {
                // images = [this.state.src1, this.state.src2, this.state.src4,this.state.src5 ];
                images = {src1: null, src2: null, src4: null, src5: null, src3: null};

                if(this.state.src1 !== "") {
                    images.src1 = this.state.src1;
                }
                else if(this.state.src2 !== "") {
                    images.src2 = this.state.src2;
                }
                else if(this.state.src4 !== "") {
                    images.src4 = this.state.src4;
                }
                else if(this.state.src5 !== "") {
                    images.src5 = this.state.src5;
                }
                else if(this.state.src3 !== "") {
                    images.src3 = this.state.src3;
                }
            }
            if(this.props.insurType === 'Third') {
                // images = [this.state.src1, this.state.src2, this.state.src4,this.state.src5 ];
                images = {src1: null, src2: null};

                if(this.state.src1 !== "") {
                    images.src1 = this.state.src1;
                }
                else if(this.state.src2 !== "") {
                    images.src2 = this.state.src2;
                }
            }
            else if(this.props.insurType === 'fire' || this.props.insurType === 'earth') {
                images = {src1: null};
                // images = [this.state.src1];
                if(this.state.src1 !== "") {
                    images.src1 = this.state.src1;
                }
            }
            else if(this.props.insurType === 'complete') {
                images = {src1: null, src2: null, src4: null, src3: null};

                // images = [this.state.src1, this.state.src2,this.state.src3,this.state.src4];
                if(this.state.src1 !== "") {
                    images.src1 = this.state.src1;
                }
                else if(this.state.src2 !== "") {
                    images.src2 = this.state.src2;
                }
                else if(this.state.src4 !== "") {
                    images.src4 = this.state.src4;
                }
                else if(this.state.src3 !== "") {
                    images.src3 = this.state.src3;
                }
            }
            else if(this.props.insurType === 'travel') {
                images = {src1: null};
                // images = [this.state.src1];
                if(this.state.src1 !== "") {
                    images.src1 = this.state.src1;
                }
            }
            else if(this.props.insurType === 'responsible') {

                images = {src1: null, src2: null, src3: null};

                // images = [this.state.src1, this.state.src2,this.state.src3,this.state.src4];
                if(this.state.src1 !== "") {
                    images.src1 = this.state.src1;
                }
                else if(this.state.src2 !== "") {
                    images.src2 = this.state.src2;
                }
                else if(this.state.src3 !== "") {
                    images.src3 = this.state.src3;
                }
            }
            else if(this.props.insurType === 'life') {
                images = {src1: null};
                // images = [this.state.src1];
                if(this.state.src1 !== "") {
                    images.src1 = this.state.src1;
                }
            }
            Actions.timeSelecting({openDrawer: this.props.openDrawer, pageTitle: 'انتخاب زمان', factor: this.props.factor, user_details: this.props.user_details, birth: this.props.birth, images: images, insurType:this.props.insurType, Birth:this.props.Birth, instalment: this.props.instalment})
        }
    }
    render() {
        // const {posts, categories, user} = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <HomeHeader active={4} openDrawer={this.props.openDrawer} pageTitle={this.props.pageTitle} />
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <View style={styles.labelContainer}>
                            <Text style={styles.topLabel}>بارگذاری تصاویر</Text>
                            <SIcon name="picture" size={20} color="gray" />
                        </View>
                        {
                            (this.props.insurType === 'body' || this.props.insurType === 'motor') ?
                                <View>
                                    <PictureInfo label="تصویر روی کارت ماشین" pic="pic1" selected={1} pickerImage={(src) => {this.pickerImage(src, 1); this.test();}} req />
                                    <PictureInfo label="تصویر پشت کارت ماشین" pic="pic1" selected={2} pickerImage={(src) => {this.pickerImage(src, 2); this.test();}} req />
                                    <PictureInfo label="تصویر برگ سبز ماشین" coin  pic="pic3" selected={4} pickerImage={(src) => { this.pickerImage(src, 4); this.test();}} />
                                    <PictureInfo label="تصویر کارت ملی" coin  pic="pic3" selected={5} pickerImage={(src) => { this.pickerImage(src, 5); this.test();}} req />
                                    <PictureInfo label="تصویر بیمه نامه قبلی" coin  pic="pic3" selected={3} pickerImage={(src) => { this.pickerImage(src, 3); this.test();}} />
                                </View>
                                : null
                        }
                        {
                            (this.props.insurType === 'Third') ?
                                <View>
                                    <PictureInfo label="تصویر روی کارت ماشین" pic="pic1" selected={1} pickerImage={(src) => {this.pickerImage(src, 1); this.test(); }} req />
                                    <PictureInfo label="تصویر پشت کارت ماشین" pic="pic1" selected={2} pickerImage={(src) => { this.pickerImage(src, 2); this.test(); }} req />
                                </View>
                                : null
                        }
                        {
                            this.props.insurType === 'fire' || this.props.insurType === 'earth' ? <PictureInfo label="تصویر کارت ملی" coin  pic="pic3" selected={1} pickerImage={(src) => { this.pickerImage(src, 1); this.test(); }} req />
                                : null
                        }
                        {
                            this.props.insurType === 'complete' ?
                                <View>
                                    <PictureInfo label="تصویر دفترچه بیمه گر پایه" pic="pic1" selected={1} pickerImage={(src) => {this.pickerImage(src, 1); this.test(); }} />
                                    <PictureInfo label="اسکن فرم پرسش نامه سلامت" pic="pic1" selected={2} pickerImage={(src) => { this.pickerImage(src, 2); this.test(); }} />
                                    <PictureInfo label="تصویر صفحه اول شناسنامه" coin  pic="pic3" selected={3} pickerImage={(src) => { this.pickerImage(src, 3); this.test(); }} />
                                    <PictureInfo label="تصویر کارت ملی" coin  pic="pic3" selected={4} pickerImage={(src) => { this.pickerImage(src, 4); this.test(); }} req />
                                </View>  : null
                        }
                        {
                            this.props.insurType === 'travel' ?
                                <PictureInfo label="تصویر گذرنامه" coin  pic="pic3" selected={1} pickerImage={(src) => { this.pickerImage(src, 1); this.test(); }} />
                                : null
                        }
                        {
                            this.props.insurType === 'responsible' ?
                                <View>
                                    <PictureInfo label="تصویر صفحه اول شناسنامه" coin  pic="pic3" selected={1} pickerImage={(src) => { this.pickerImage(src, 1); this.test(); }} />
                                    <PictureInfo label="اسکن فرم پرسش نامه سلامت" pic="pic1" selected={2} pickerImage={(src) => { this.pickerImage(src, 2); this.test(); }} />
                                    <PictureInfo label="تصویر کارت ملی" coin  pic="pic3" selected={3} pickerImage={(src) => { this.pickerImage(src, 3); this.test(); }} req />
                                </View>  : null
                        }
                        {
                            this.props.insurType === 'life' ?
                                <PictureInfo label="تصویر کارت ملی" coin  pic="pic3" selected={1} pickerImage={(src) => { this.pickerImage(src, 1); this.test(); }} req />
                                : null
                        }
                        {
                            this.props.insurType === 'asansor' ?
                                <PictureInfo label="تصویر کارت ملی" coin  pic="pic3" selected={1} pickerImage={(src) => { this.pickerImage(src, 1); this.test(); }} req />
                                : null
                        }
                        {
                            this.props.instalment?
                                <View>
                                    <PictureInfo label="تصویر برگه اول چک " pic="pic1" selected={1} pickerImage={(src) => { this.pickerImage(src, 1); this.test(); }} />
                                    <PictureInfo label="تصویر برگه دوم چک" coin  pic="pic3" selected={2} pickerImage={(src) => { this.pickerImage(src, 2); this.test(); }} />
                                </View>
                                : null
                        }
                    </View>
                </ScrollView>
                <View style={styles.footer}>
                    <TouchableOpacity style={[styles.iconLeftContainer, {backgroundColor: this.state.nextStep ? 'rgba(255, 193, 39, 1)' : 'rgba(200, 200, 200, 1)' }]} onPress={() => this.nextStep()}>
                        <FIcon name="arrow-left" size={18} color="white" style={{borderColor: 'white', borderWidth: 1, borderRadius: 20, padding: 3, marginRight: 5}} />
                        <Text style={styles.label}>بعدی</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.iconRightContainer} onPress={() => this.onBackPress()}>
                        <FIcon name="arrow-right" size={18} color="rgba(17, 103, 253, 1)" />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
export default PictureUpload;

