import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative'
    },
    image: {
        width: '100%',
        // resizeMode: 'contain',
        // height: '100%',
        height: 195,
        // resizeMode: 'contain',
        // backgroundColor: 'rgb(61, 99, 223)',
    },
    header: {
        width: '100%',
        // resizeMode: 'contain',
        // height: '100%',
        height: 65
    },
    // TrapezoidStyle: {
    //     flex: 1,
    //     position: 'absolute',
    //     bottom: '75%',
    //     right: 0,
    //     left: 0,
    //     zIndex: 100,
    //
    //     borderTopWidth: 50,
    //     borderLeftWidth: 0,
    //     borderBottomWidth: 0,
    //     borderTopColor: 'rgb(61, 99, 223)',
    //     borderRightColor: 'rgb(233, 233, 233)',
    //     borderLeftColor: 'transparent',
    //     borderBottomColor: 'transparent',
    // },
    imageRow: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        paddingTop: 20
    },
    profileImage: {
        width: 85,
        height: 85,
        borderRadius: 85,
    },
    profile: {
        // width: 90,
        // marginRight: '30%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 30,
        paddingBottom: 7
    },
    imageContainer: {
        width: 70,
        height: 70,
        backgroundColor: 'rgb(37, 133, 228)',
        alignItems: 'center',
        justifyContent: 'center',
        // padding: 20,
        borderRadius: 90,
        // elevation: 8,
        // borderWidth: 2,
        // borderTopColor: '#38d0e6',
        // borderRightColor: '#38d0e6',
        // borderLeftColor: '#24d1ba',
        // borderBottomColor: '#24d1ba',
    },
    name: {
        fontSize: 13,
        color: 'rgb(255, 255, 255)',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: 10,
        paddingLeft: 20,
        paddingBottom: 5
    },
    // item: {
    //     width: '32%',
    //     height: '55%',
    //     // backgroundColor: 'red'
    //     // borderRadius: 15,
    //
    //
    // },
    reminderItem: {
        width: '85%',
        backgroundColor: 'rgb(233, 233, 233)',
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        paddingBottom: 7

    },
    // bodyImage: {
    //     width: 50,
    //     resizeMode: 'contain',
    //     tintColor: 'rgb(82, 82, 82)'
    // },
    // bodyyImage: {
    //     width: 50,
    //     resizeMode: 'contain',
    //     tintColor: 'rgb(82, 82, 82)'
    // },
    scroll: {
        flex: 1,
        marginBottom: 70,
        backgroundColor: 'rgb(245, 245, 245)'
    },
    headerText: {
        fontSize: 18,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'center',
        color: 'white',
        paddingTop: 30
    },
    // body : {
    //     alignItems: 'center',
    //     justifyContent: 'flex-start',
    //     paddingRight: 30,
    //     paddingLeft: 30,
    //     paddingBottom: 40,
    //     paddingTop: 2,
    //     backgroundColor: 'rgb(245, 245, 245)'
    // },
    // row: {
    //     width: '100%',
    //     flexDirection: 'row',
    //     alignItems: 'center',
    //     justifyContent: 'space-between',
    //     height: 130
    // },
    // lastrow: {
    //     width: '100%',
    //     height: 130,
    //     flexDirection: 'row',
    //     alignItems: 'center',
    //     justifyContent: 'center',
    // },
    // iconContainer: {
    //     width: 30,
    //     height: 30,
    //     borderRadius: 30,
    //     backgroundColor: 'white',
    //     alignItems: 'center',
    //     justifyContent: 'center'
    // },
    // label: {
    //     fontSize: 11,
    //     fontFamily: 'IRANSansMobile(FaNum)',
    //     textAlign: 'center',
    //     paddingTop: 7,
    //     color: 'black'
    // },
    bodyLabel: {
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingTop: 40,
        paddingBottom: 20,
        color: 'black'
    },
    slideContainers: {
        flexDirection: 'row',
        transform: [
            {rotateY: '180deg'},
        ],
    },
    newContainer: {
        width: 34,
        backgroundColor: 'rgb(247, 148, 29)',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 23,
        left: 15
    },
    newText: {
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        color: 'white'
    },






    // scroll: {
    //     flex: 1,
    //     marginBottom: 80,
    //     paddingTop: 60
    // },
    body: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 20,
        paddingLeft: 20,
        paddingBottom: 30,
        paddingTop: 20,
        // backgroundColor: 'red'
    },
    row: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    lastrow: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    item: {
        width: '33%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15
    },
    bodyImage: {
        width: 30,
        resizeMode: 'contain',
        tintColor: 'white'
    },
    bodyyImage: {
        width: 30,
        resizeMode: 'contain',
        tintColor: 'white'
    },
    iconContainer: {
        width: 30,
        height: 30,
        borderRadius: 30,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    label: {
        fontSize: 12,
        color: 'rgb(32, 69, 117)',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: 10
    },
});
