import React, {Component} from 'react';
import { View, TouchableOpacity, Text, Linking, ImageBackground ,Alert, Image, BackHandler, NetInfo, Dimensions, AsyncStorage, ScrollView, ActivityIndicator} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux';
import Axios from 'axios';
export const url = 'http://bimegaronline.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import Icon from 'react-native-vector-icons/dist/Feather';
import FooterMenu from "../../components/footerMenu/index";
import AlertView from "../../components/modalMassage";
import bime1 from "../../assets/newInsurance/body.png";
import bime2 from "../../assets/newInsurance/third.png";
import bime3 from "../../assets/newInsurance/travel.png";
import bime4 from "../../assets/newInsurance/medical.png";
import bime5 from "../../assets/newInsurance/fire.png";
import bime6 from "../../assets/newInsurance/life.png";
import bime7 from "../../assets/newInsurance/indi.png";
import bime10 from "../../assets/newInsurance/earth.png";
import homeBg from "../../assets/homeBg2.png";
import motor from "../../assets/motor.png";
import mass from "../../assets/mssoliat.png";
import asansor from "../../assets/asansor.png";
import LinearGradient from "react-native-linear-gradient";
import SlideItems from "../../components/slideItems";
import BuyHeader from "../../components/buyHeader";
import SoonModal from './soonModal'
import {connect} from 'react-redux';
import {store} from '../../config/store';


class InsuranceBuy extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            login: true,
            loading: false,
            modalVisible: false,
            wifi: false,
            isConnected: false,
            spacialPosts: [],
            logout: true,
            modalVisible2: false
        };
    }
    // componentWillUnmount() {
    //     BackHandler.addEventListener("hardwareBackPress",  this.onBackPress);
    // }

    componentWillUpdate(){
        BackHandler.addEventListener("hardwareBackPress",  this.onBackPress.bind(this));
    }

    closeModal() {
        this.setState({modalVisible: false});
    }
    componentWillMount() {
        AsyncStorage.getItem('insuranceInfo').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);
                Linking.getInitialURL().then(url => {
                    console.log('url', url)
                    if(url !== null) {
                        Actions.comment({openDrawer: this.props.openDrawer, modalOpen: true, insTitle: newInfo.title, insId: newInfo.id});
                        console.log('userId', newInfo.user_id)
                        console.log('nnnnnnewinfo', newInfo)
                    }
                });
            }
            else {
                Linking.getInitialURL().then(url => {
                    console.log('url', url)
                    if(url !== null) {
                        Actions.insuranceBuy({openDrawer: this.props.openDrawer, modalOpen: false});
                    }
                });
            }
        });
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        NetInfo.getConnectionInfo().then((connectionInfo) => {
            this.setState({isConnected: connectionInfo.type !== 'none'}, ()=> {
                if(this.state.isConnected === false){
                    this.setState({modalVisible: true, loading: false, wifi: true})
                }
            })
        });
        console.log('heere is insBuy')
        this.setState({loading: true})

        Axios.post('/request/blog/list').then(response => {
            this.setState({spacialPosts: response.data.data.data});
            console.log('spacial posts', response.data.data.data)
            AsyncStorage.getItem('token').then((info) => {
                if(info !== null) {
                    const newInfo = JSON.parse(info);
                    console.log('iiiiiiiiinfo', newInfo)

                    Axios.post('/user', {
                        mobile: newInfo.mobile

                    }).then(response=> {
                        console.log('profile', newInfo.mobile)
                        store.dispatch({type: 'USER_INFO_FETCHED', payload: response.data.data});
                        this.setState({loading: false})


                    })
                        .catch((error) => {
                            // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                            // this.setState({loading: false});
                            this.setState({modalVisible: true, loading: false});
                        });
                }
                else{
                    this.setState({loading: false})
                }
            });
        })
        .catch((error) => {
            console.log(error);
            this.setState({modalVisible: true, loading: false});
        });
    }
    logout = () => {
        Alert.alert(
            'خروج از اپلیکیشن',
            'آیا از  خروج خود مطمئن هستید؟',
            [
                {text: 'خیر', onPress: () => this.setState({logout: false}), style: 'cancel'},
                {text: 'بله', onPress: () =>  this.setState({logout: true}, () => { BackHandler.exitApp();})},
            ]
        )
    }
    onBackPress() {
        console.log('some thing loged')
        this.logout()
        // this.setState({logout: false})
        // Alert.alert(
        //     'خروج از اپلیکیشن',
        //     'آیا از  خروج خود مطمئن هستید؟',
        //     [
        //         {text: 'خیر', onPress: () => this.setState({logout: false}), style: 'cancel'},
        //         {text: 'بله', onPress: () =>  this.setState({logout: true}, () => { BackHandler.exitApp();})},
        //     ]
        // )
        return true;
    };
    testReminder() {
        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);

                const expiresTime = newInfo.expires_at;
                const currentTime = new Date().getTime()/1000;
                console.log('expiresTime', expiresTime);
                console.log('currentTime', currentTime);
                if(expiresTime> currentTime){
                    Actions.addReminder()
                }
                else {
                    Actions.login({openDrawer: this.props.openDrawer, reminderReq: true})
                }
            }
            else{
                Actions.login({openDrawer: this.props.openDrawer, reminderReq: true})
            }
        });
    }
    closeModal2() {
        this.setState({modalVisible2: false});
    }
    testProfile() {
        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);

                const expiresTime = newInfo.expires_at;
                const currentTime = new Date().getTime()/1000;
                console.log('expiresTime', expiresTime);
                console.log('currentTime', currentTime);
                if(expiresTime> currentTime){
                    Actions.profile()
                }
                else {
                    Actions.login({openDrawer: this.props.openDrawer, reminderReq: true})
                }
            }
            else{
                Actions.login({openDrawer: this.props.openDrawer, reminderReq: true})
            }
        });
    }
    render() {
        const {user} = this.props;
        // if(this.state.loading){
        //     return (<Loader />)
        // }
        // else
            return (
            <View style={styles.container}>
                <ScrollView style={styles.scroll}>
                    {/*<LinearGradient start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['rgb(0, 114, 255)', 'rgb(0, 128, 255)', 'rgb(0, 142, 255)']} style={styles.header}>*/}
                        {/*<View style={styles.imageRow}>*/}
                            {/*<View style={styles.profile}>*/}
                                {/*/!*<View style={styles.iconContainer}>*!/*/}
                                    {/*/!*<Icon name="user" size={20} />*!/*/}
                                {/*/!*</View>*!/*/}
                                {/*/!*<Text style={styles.name}>{user !== null ? user.fname : ''} {user !== null ? user.lname : ''}</Text>*!/*/}
                            {/*</View>*/}
                            {/*<TouchableOpacity onPress={() => Actions.drawerOpen()}>*/}
                                {/*<Icon name="menu" size={30} color="white" style={{paddingRight: 15}} />*/}
                            {/*</TouchableOpacity>*/}
                        {/*</View>*/}
                        {/*<Image style={{position: 'absolute', top: 8, left: '39%', width: 50, height: 50, resizeMode: 'stretch'}}*/}
                               {/*source={require('../../assets/headerLogo2.png')}/>*/}
                    {/*</LinearGradient>*/}
                    <ImageBackground source={homeBg} style={styles.image}>
                        <View style={styles.imageRow}>
                            <View style={styles.profile}>
                                <TouchableOpacity onPress={()=> this.testProfile()} >
                                    <Icon name="user" size={22} color="white" />
                                </TouchableOpacity>
                                {/*<Text style={styles.name}>{user !== null ? user.fname : ''} {user !== null ? user.lname : ''}</Text>*/}
                            </View>
                            <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                                <Icon name="menu" size={30} color="white" style={{paddingRight: 15}} />
                            </TouchableOpacity>
                        </View>
                        <Image style={{position: 'absolute', top: 20, left: '42%', width: 60, height: 60,
                        resizeMode: 'stretch'}}
                               source={require('../../assets/logo2.png')}/>
                        <Text style={styles.headerText}>بیمه گر</Text>
                    </ImageBackground>
                    {/*<BuyHeader />*/}
                    <View style={styles.body}>
                        <View style={styles.row}>
                            <View style={styles.item}>
                                <View
                                                style={styles.imageContainer}>
                                    <TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, fire: true, pageTitle: 'بیمه آتش سوزی'})} >
                                        <View>
                                            <Image source={bime5} style={styles.bodyImage}  />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <Text style={styles.label}>آتش سوزی</Text>
                            </View>
                            <View style={styles.item}>
                                <View
                                                style={styles.imageContainer}>
                                    <TouchableOpacity onPress={() => Actions.vacleInfo({openDrawer: this.props.openDrawer, bodyBime: true, pageTitle: 'بیمه بدنه خودرو'})} >
                                        <View>
                                            <Image source={bime1} style={styles.bodyImage} />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <Text style={styles.label}>بدنه</Text>
                            </View>
                            <View style={styles.item}>
                                <View
                                                style={styles.imageContainer}>
                                    <TouchableOpacity onPress={() => Actions.vacleInfo({openDrawer: this.props.openDrawer, thirdBime: true,motor: false, pageTitle: 'بیمه شخص ثالث'})} >
                                        <View>
                                            <Image source={bime2} style={styles.bodyImage} />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <Text style={styles.label}>شخص ثالث</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.item}>
                                <View
                                                style={styles.imageContainer}>
                                    {/*<TouchableOpacity  onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, individual: true, pageTitle: 'بیمه درمان انفرادی'})} >*/}
                                    <TouchableOpacity  onPress={()=> this.setState({modalVisible2: true})} >
                                        <View>
                                            <Image source={bime7} style={styles.bodyImage} />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <Text style={styles.label}>درمانی </Text>
                            </View>
                            <View style={styles.item}>
                                <View
                                                style={styles.imageContainer}>
                                    {/*<TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, medical: true, pageTitle: 'بیمه مسئولیت پزشکی'})} >*/}
                                    <TouchableOpacity onPress={()=> this.setState({modalVisible2: true})} >
                                        <View>
                                            <Image source={bime4} style={styles.bodyImage} />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <Text style={styles.label}>پزشکی</Text>
                            </View>
                            <View style={styles.item}>
                                <View
                                                style={styles.imageContainer}>
                                    {/*<TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, age: true, pageTitle: 'بیمه عمر'})} >*/}
                                    <TouchableOpacity onPress={()=> this.setState({modalVisible2: true})} >
                                        <View>
                                            <Image source={bime6} style={styles.bodyImage} />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <Text style={styles.label}>عمر</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.item}>
                                <View
                                                style={styles.imageContainer}>
                                    {/*<TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, travel: true, pageTitle: 'بیمه مسافرتی'})} >*/}
                                    <TouchableOpacity onPress={()=> this.setState({modalVisible2: true})} >
                                        <View>
                                            <Image source={bime3} style={styles.bodyImage} />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <Text style={styles.label}>مسافرتی</Text>
                            </View>
                            <View style={styles.item}>
                                <View
                                                style={styles.imageContainer}>
                                    {/*<TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, earth: true,  pageTitle: 'بیمه زلزله'})} >*/}
                                    <TouchableOpacity onPress={()=> this.setState({modalVisible2: true})} >
                                        <View>
                                            <Image source={bime10} style={[styles.bodyImage, {width: 25}]} />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <Text style={styles.label}>زلزله</Text>
                            </View>
                            <View style={styles.item}>
                                <View
                                                style={styles.imageContainer}>
                                    {/*<TouchableOpacity onPress={()=> this.setState({modalVisible2: true})} >*/}
                                    <TouchableOpacity onPress={()=> this.setState({modalVisible2: true})} >
                                        <View>
                                            <Image source={mass} style={[styles.bodyImage, {width: 30}]} />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <Text style={styles.label}>مسئولیت</Text>
                            </View>
                        </View>
                        {
                            this.state.loading ?
                                <View style={{paddingTop: 30}}>
                                    <ActivityIndicator
                                        size={30}
                                        animating={true}
                                        color='rgb(39, 133, 228)'
                                    />
                                </View>
                                :
                                <View>
                                    <Text style={styles.bodyLabel}>آخرین مطالب و مقالات</Text>
                                    <View style={{width: '100%'}}>
                                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ transform: [{ scaleX: -1}]}}>
                                            <View style={styles.slideContainers}>
                                                {
                                                    this.state.spacialPosts ?
                                                        this.state.spacialPosts.map((item) => {return <SlideItems item={item} key={item.id}  />}): null
                                                }

                                            </View>
                                        </ScrollView>
                                    </View>
                                </View>
                        }

                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}
                            onChange={() => this.setState({modalVisible: false})}
                            title={(this.state.wifi ? 'لطفا وضعیت وصل بودن به اینترنت را بررسی کنید': 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید')}
                        />
                        <SoonModal
                            closeModal={(title) => this.closeModal2(title)}
                            onChange={(visible) => this.setState({modalVisible2: false})}
                            modalVisible={this.state.modalVisible2}
                        />
                    </View>
                </ScrollView>
                <FooterMenu active="bime" openDrawer={this.props.openDrawer}/>
                {/*<View style={[styles.TrapezoidStyle, {borderRightWidth: Dimensions.get('window').width}]} />*/}
            </View>

        );
    }
}
// export default InsuranceBuy;
function mapStateToProps(state) {
    return {
        user: state.auth.user
    }
}
export default connect(mapStateToProps)(InsuranceBuy);