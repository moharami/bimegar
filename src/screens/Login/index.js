import React, {Component} from 'react';
import {View, TouchableOpacity, Text, TextInput, Image,BackHandler, AsyncStorage, KeyboardAvoidingView, Picker, ScrollView} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import Axios from 'axios'
export const url = 'http://bimegaronline.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import AlertView from '../../components/modalMassage'
import TimerCountdown from 'react-native-timer-countdown';
import {store} from '../../config/store';
import FIcon from 'react-native-vector-icons/Feather';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import moment_jalaali from 'moment-jalaali'
import Selectbox from 'react-native-selectbox'

class Login extends Component {
    constructor(props) {
        super(props);
        this.backCount = 0;
        this.state = {
            text: '',
            code: '',
            login: true,
            loading: false,
            mobileCorrect: false,
            modalVisible: false,
            correct: false,
            isConnected: false,
            wifi: false,
            send: true,
            codeDetect: false,
            register: false,
            fill: false,
            fname: '',
            lname: '',
            nationalId: '',
            birthdayYear: {key: 0, label: 'سال', value: 0},
            birthdayMonth: {key: 0, label: 'ماه', value: 0},
            birthdayDay: {key: 0, label: 'روز', value: 0},
            password: '',
            national: false,
            passMassage: false,
            codeValidate: false
        };
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress",  this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress",  this.onBackPress.bind(this));
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    onLogin(){
        console.log(this.state.text)
        if(this.state.text === '' )
        {
            this.setState({ correct: true, modalVisible: true});
        }
        else{
            this.setState({loading: true});
            Axios.post('/send_login', {
                mobile: this.state.text
            }).then(response=> {
                this.setState({loading: false});
                console.log('send mobile', response.data);
                if(response.data.msg === 'CodeSendSuccess'){
                    this.setState({
                        loading: false,
                        send: false
                    })
                }
                else {
                    Actions.login({openDrawer: this.props.openDrawer, profile: true})
                }
            })
            .catch((response) => {
                console.log('response error', response.response)
                if(response.response.data.msg === 'NotRegister'){
                    Axios.post('/send_register', {
                        mobile: this.state.text
                    }).then(response=> {
                        if(response.data.msg === 'Register'){
                            console.log('register success', response.data);
                            this.setState({
                                loading: false,
                                send: false,
                                register: true
                            })
                        }
                        if(response.data.msg === 'BeforeRegister'){
                            this.setState({loading: false})
                        }
                    })
                        .catch((error) => {
                            this.setState({modalVisible: true, loading: false});
                        });
                }
                else if(response.response.data.msg === 'ErorrInput' || response.response.data.msg === 'CodeSendError'){
                    this.setState({
                        loading: false
                    }, () => {

                        this.setState({correct: true, modalVisible: true, loading: false});
                    })
                }
                else {
                    this.setState({modalVisible: true, loading: false});
                }
            });
        }
    }
    confirm() {
        // f = f + this.number[0] + this.number[1] +this.number[2] +this.number[3];
        if(this.state.register) {
            if(this.state.code === '' || this.state.fname === '' || this.state.lname === '' || this.state.nationalId === '' || this.state.birthdayDay.value === 0 || this.state.birthdayMonth.value === 0 || this.state.birthdayYear.value === 0 || this.state.password === '')
                this.setState({fill: true, modalVisible: true});
            else {
                this.setState({fill: false, codeValidate: false}, ()=> {
                    let input = this.state.nationalId;
                    if (!/^\d{10}$/.test(input))
                        this.setState({national: true, modalVisible: true, loading: false});
                    let check = parseInt(input[9],10);
                    let sum = [0, 1, 2, 3, 4, 5, 6, 7, 8]
                            .map(function (x) {
                                return parseInt(input[x],10) * (10 - x);
                            })
                            .reduce(function (x, y) {
                                return x + y;
                            }) % 11;
                    if (!((sum < 2 && check === sum) || (sum >= 2 && check + sum === 11))){
                        this.setState({national: true, modalVisible: true, loading: false});
                    }
                    else if(this.state.password.length < 6) {
                        this.setState({modalVisible: true, passMassage: true, national: false });
                    }
                    else {
                        let Birth = this.state.birthdayYear.value+"-"+this.state.birthdayMonth.value+"-"+this.state.birthdayDay.value;
                        // console.log('bbbbitrh',moment_jalaali(Birth, 'jYYYY/jM/jD').format('YYYY-M-D'))
                        // let stttr = moment_jalaali(Birth, 'jYYYY/jMM/jDD').format('YYYY-MM-DD')
                        // stttr = stttr.replace(/۰/g, "0");
                        // stttr = stttr.replace(/۱/g, "1");
                        // stttr = stttr.replace(/۲/g, "2");
                        // stttr = stttr.replace(/۳/g, "3");
                        // stttr = stttr.replace(/۴/g, "4");
                        // stttr = stttr.replace(/۵/g, "5");
                        // stttr = stttr.replace(/۶/g, "6");
                        // stttr = stttr.replace(/۷/g, "7");
                        // stttr = stttr.replace(/۸/g, "8");
                        // stttr = stttr.replace(/۹/g, "9");
                        console.log('neW STTR ', Birth )


                        this.setState({loading: true});
                        Axios.post('/lg/new/register', {
                            mobile: this.state.text,
                            code: this.state.code,
                            fname: this.state.fname,
                            lname: this.state.lname,
                            national_id: this.state.nationalId,
                            birthday: Birth,
                            invite_user: null,
                            password: this.state.password,
                            wasRecentlyCreated: null
                        }).then(response=> {
                            this.setState({
                                loading: false
                            }, () => {
                                console.log('register rrrrrrrrrrrr data', response);
                                if(response.data.msg === 'LoginSuccess') {
                                    store.dispatch({type: 'USER_INFO_FETCHED', payload: response.data.data});
                                    store.dispatch({type: 'USER_LOGED', payload: true});
                                    const info = {'token': response.data.access_token, 'mobile': this.state.text, 'code': this.state.code, 'expires_at': response.data.expires_at, 'user_id': response.data.data.id, 'wasRecentlyCreated': response.data.data.wasRecentlyCreated};
                                    const newwwwAsync = AsyncStorage.setItem('token', JSON.stringify(info));
                                    console.log('newwwwAsync', newwwwAsync);
                                    const info2 = {'loged': true};
                                    const newwwwAsync2 = AsyncStorage.setItem('loged', JSON.stringify(info2));
                                    console.log('newwwwAsync2', newwwwAsync2);
                                    // AsyncStorage.getItem('loged').then((info) => {
                                    //     if(info !== null) {
                                    //         const newInfo = JSON.parse(info);
                                    //         console.log('neeeew iiiiinfo', newInfo)
                                    //     }
                                    // })

                                    if(this.props.profile) {
                                        // if(response.data.data.wasRecentlyCreated === 0) {
                                        //     Actions.register({openDrawer: this.props.openDrawer, mobile: this.state.text, profile: true})
                                        // }
                                        // else {
                                        //     Actions.profile({openDrawer: this.props.openDrawer, mobile: this.state.text})
                                        // }
                                        Actions.profile({openDrawer: this.props.openDrawer, mobile: this.state.text})

                                    }
                                    if(this.props.reminderReq) {
                                        // if(response.data.data.wasRecentlyCreated === 0) {
                                        //     Actions.register({openDrawer: this.props.openDrawer, mobile: this.state.text, reminderReq: true})
                                        // }
                                        // else {
                                        //     Actions.addReminder({openDrawer: this.props.openDrawer})
                                        // }
                                        Actions.addReminder({openDrawer: this.props.openDrawer})

                                    }
                                    else if(this.props.insBuy) {
                                        let newFactor = this.props.factor;
                                        let newUserdetails = this.props.user_details;
                                        newUserdetails.user_id = response.data.data.id;
                                        newUserdetails._token = response.data.access_token;
                                        const id = response.data.data.id;

                                        if(this.props.insurType === 'body') {
                                            newFactor.user_id = id;
                                        }
                                        else if(this.props.insurType === 'Third') {
                                            newFactor.user_id = id;
                                        }
                                        else if(this.props.insurType === 'motor') {
                                            newFactor.user_id = id;
                                        }
                                        else if(this.props.insurType === 'fire') {
                                            newFactor.user_id = id;
                                        }
                                        else if(this.props.insurType === 'complete') {
                                            newFactor.user_id = id;
                                        }
                                        else if(this.props.insurType === 'travel') {
                                            newFactor.user_id = id;
                                            newFactor.insurance.user_id = id;
                                            newFactor.dataSelect.insurance.user_id = id;
                                        }
                                        else if(this.props.insurType === 'responsible') {
                                            newFactor.user_id = id;
                                        }
                                        else if(this.props.insurType === 'life') {
                                            newFactor.user_id = id;
                                            newFactor.insurance.user_id = id;
                                        }

                                        // if(response.data.data.wasRecentlyCreated === 0) {
                                        //     Actions.register({openDrawer: this.props.openDrawer, mobile: this.state.text, insBuy: true, pageTitle:'مشخصات خریدار', factor:newFactor, user_details:newUserdetails, insurType:this.props.insurType, instalment: this.props.instalment})
                                        // }
                                        // else {
                                        //     Actions.prices({openDrawer: this.props.openDrawer, pageTitle:'مشخصات خریدار', factor:newFactor, user_details:newUserdetails, insurType:this.props.insurType, instalment: this.props.instalment})
                                        // }
                                        Actions.prices({openDrawer: this.props.openDrawer, pageTitle:'مشخصات خریدار', factor:newFactor, user_details:newUserdetails, insurType:this.props.insurType, instalment: this.props.instalment, insBuy: this.props.insBuy})
                                    }
                                }
                                else {
                                    this.setState({modalVisible: true, codeValidate: true, loading: false});
                                }
                            })
                        })
                            .catch((response) => {
                                console.log(response.response)
                                if(response.response.data.msg === 'ErorrInput' || response.response.data.msg === 'IncorrectCode'){
                                    this.setState({
                                        loading: false
                                    }, () => {
                                        this.setState({codeDetect: true, modalVisible: true});
                                    })
                                }
                                else {
                                    this.setState({modalVisible: true, loading: false});
                                }
                            });
                    }
                })
            }
        }
        else{
            if(this.state.code === '')
                this.setState({codeDetect: true, modalVisible: true});
            else {
                console.log('code', this.state.code)
                this.setState({loading: true});
                Axios.post('/check_code', {
                    mobile: this.state.text,
                    code: this.state.code
                }).then(response=> {
                    this.setState({
                        loading: false
                    }, () => {
                        store.dispatch({type: 'USER_INFO_FETCHED', payload: response.data.data});
                        store.dispatch({type: 'USER_LOGED', payload: true});
                        console.log('confirm data', response);
                        const info = {'token': response.data.access_token, 'mobile': this.state.text, 'code': this.state.code, 'expires_at': response.data.expires_at, 'user_id': response.data.data.id, 'wasRecentlyCreated': response.data.data.wasRecentlyCreated};
                        const newwwwAsync = AsyncStorage.setItem('token', JSON.stringify(info));
                        console.log('newwwwAsync', newwwwAsync);
                        const info2 = {'loged': true};
                        const newwwwAsync2 = AsyncStorage.setItem('loged', JSON.stringify(info2));
                        console.log('newwwwAsync2', newwwwAsync2);
                        AsyncStorage.getItem('loged').then((info) => {
                            if(info !== null) {
                                const newInfo = JSON.parse(info);
                                console.log('neeeew iiiiinfo', newInfo)
                            }
                        })
                        // Actions.home({openDrawer: this.props.openDrawer, loged: true});
                        console.log('profilllllllllle true ? ', this.props.profile);
                        console.log('insbuy true ? ', this.props.insBuy);

                        if(this.props.profile) {
                            // if(response.data.data.wasRecentlyCreated === 0) {
                            //     Actions.register({openDrawer: this.props.openDrawer, mobile: this.state.text, profile: true})
                            // }
                            // else {
                            //     Actions.profile({openDrawer: this.props.openDrawer, mobile: this.state.text})
                            // }
                            Actions.profile({openDrawer: this.props.openDrawer, mobile: this.state.text})

                        }
                        if(this.props.reminderReq) {
                            // if(response.data.data.wasRecentlyCreated === 0) {
                            //     Actions.register({openDrawer: this.props.openDrawer, mobile: this.state.text, reminderReq: true})
                            // }
                            // else {
                            //     Actions.addReminder({openDrawer: this.props.openDrawer})
                            // }
                            Actions.addReminder({openDrawer: this.props.openDrawer})

                        }
                        else if(this.props.insBuy) {
                            let newFactor = this.props.factor;
                            let newUserdetails = this.props.user_details;
                            newUserdetails.user_id = response.data.data.id;
                            newUserdetails._token = response.data.access_token;
                            const id = response.data.data.id;

                            if(this.props.insurType === 'body') {
                                newFactor.user_id = id;
                            }
                            else if(this.props.insurType === 'Third') {
                                newFactor.user_id = id;
                            }
                            else if(this.props.insurType === 'motor') {
                                newFactor.user_id = id;
                            }
                            else if(this.props.insurType === 'fire') {
                                newFactor.user_id = id;
                            }
                            else if(this.props.insurType === 'complete') {
                                newFactor.user_id = id;
                            }
                            else if(this.props.insurType === 'travel') {
                                newFactor.user_id = id;
                                newFactor.insurance.user_id = id;
                                newFactor.dataSelect.insurance.user_id = id;
                            }
                            else if(this.props.insurType === 'responsible') {
                                newFactor.user_id = id;
                            }
                            else if(this.props.insurType === 'life') {
                                newFactor.user_id = id;
                                newFactor.insurance.user_id = id;
                            }

                            // if(response.data.data.wasRecentlyCreated === 0) {
                            //     Actions.register({openDrawer: this.props.openDrawer, mobile: this.state.text, insBuy: true, pageTitle:'مشخصات خریدار', factor:newFactor, user_details:newUserdetails, insurType:this.props.insurType, instalment: this.props.instalment})
                            // }
                            // else {
                            //     Actions.prices({openDrawer: this.props.openDrawer, pageTitle:'مشخصات خریدار', factor:newFactor, user_details:newUserdetails, insurType:this.props.insurType, instalment: this.props.instalment})
                            // }
                            Actions.prices({openDrawer: this.props.openDrawer, pageTitle:'مشخصات خریدار', factor:newFactor, user_details:newUserdetails, insurType:this.props.insurType, instalment: this.props.instalment})
                        }
                    })
                })
                    .catch((response) => {
                        console.log(response.response)
                        if(response.response.data.msg === 'ErorrInput' || response.response.data.msg === 'IncorrectCode'){
                            this.setState({
                                loading: false
                            }, () => {
                                this.setState({codeDetect: true, modalVisible: true});
                            })
                        }
                        else {
                            this.setState({modalVisible: true, loading: false});
                        }
                    });
            }
        }
    }
    resend() {
        this.setState({loading: true});
        Axios.post('/resend_activation', {
            mobile: this.state.text
        }).then(response=> {
            this.setState({
                loading: false
            }, () => {
                console.log('confirm data', response);
            })
        })
        .catch((error) => {
            this.setState({modalVisible: true, loading: false});
        });
    }
    render() {
        let yearArray2 = [], monthArray = [], dayArray = [];
        for(let i=1397; i>=1300 ; i--){
            yearArray2.push({key: i, label: i.toString(), value: i})
        }
        const month = ["01", "02", "03", "04", "05", "06", "07", "08", "09","10", "11","12"]
        monthArray = month.map((item)=> {return {key: parseInt(item), label: item, value: item}})
        const day = ["01", "02", "03", "04", "05", "06", "07", "08", "09","10", "11","12", "13", "14", "15", "16", "17", "18", "19", "20", "21","22", "23","24", "25", "26", "27", "28", "29", "30", "31"];
        dayArray = day.map((item)=> {return {key: parseInt(item), label: item, value: item}})

        if(this.state.loading){
            return (<Loader />)
        }
        else return (
            <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
                {this.state.loading ? <Loader send={false}/> :
                    <View style={styles.send}>
                        <Image style={{alignSelf: 'center', marginRight: 20, width: '30%', resizeMode: 'contain'}} resizeMode={'contain'}
                               source={require('../../assets/logo2.png')}/>
                        <ScrollView>
                            <View style={styles.body}>
                                <View style={styles.row}>
                                    <LinearGradient start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['rgb(0, 114, 255)', 'rgb(0, 128, 255)', 'rgb(0, 142, 255)']} style={{borderRadius: 5}}>
                                        <View style={styles.timerContainer}>
                                            { this.state.send ?
                                                <TouchableOpacity onPress={() => this.onLogin()}>
                                                    <Text style={styles.buttonTitle}>ارسال</Text>
                                                </TouchableOpacity>
                                                :
                                                <TimerCountdown
                                                    initialSecondsRemaining={1000*60}
                                                    // onTick={secondsRemaining => console.log('tick', secondsRemaining)}
                                                    onTimeElapsed={() => console.log('complete')}
                                                    allowFontScaling={true}
                                                    style={{ fontSize: 14, fontFamily: 'IRANSansMobile(FaNum)', color: 'white'}}
                                                />
                                            }
                                        </View>
                                    </LinearGradient>
                                    <TextInput
                                        // maxLength={1}
                                        placeholder="شماره موبایل"
                                        maxLength={11}
                                        keyboardType='numeric'
                                        placeholderTextColor={'gray'}
                                        underlineColorAndroid='transparent'
                                        style={{
                                            textAlign: 'center',
                                            height: 45,
                                            backgroundColor: 'white',
                                            paddingRight: 10,
                                            paddingLeft: 10,
                                            fontSize: 16,
                                            color: '#7A8299',
                                            width: '75%',
                                            fontFamily: 'IRANSansMobile(FaNum)'
                                            // marginBottom: 20
                                        }}
                                        value={this.state.text}
                                        onChangeText={(text) => this.setState({text})}/>
                                </View>
                                <TextInput
                                    // maxLength={1}
                                    keyboardType='numeric'
                                    placeholder={'کد ارسالی را وارد نمایید'}
                                    placeholderTextColor={'gray'}
                                    underlineColorAndroid='transparent'
                                    style={{
                                        textAlign: 'center',
                                        height: 45,
                                        backgroundColor: 'white',
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        width: '90%',
                                        borderRadius: 7,
                                        fontSize: 16,
                                        color: '#7A8299',
                                        fontFamily: 'IRANSansMobile(FaNum)',
                                        // marginBottom: 20,
                                        marginTop: 20
                                    }}
                                    value={this.state.code}
                                    onChangeText={(text) => {
                                        this.setState({code: text})}}
                                />
                                {
                                    this.state.register ?
                                        <View style={styles.registerContainer}>
                                            <View style={{display: 'flex', flexDirection: 'row', marginTop: 20}}>
                                                <TextInput
                                                    placeholder="نام"
                                                    placeholderTextColor={'#C8C8C8'}
                                                    underlineColorAndroid='transparent'
                                                    value={this.state.fname}
                                                    style={{
                                                        textAlign: 'right',
                                                        borderWidth: 1,
                                                        borderColor: '#C8C8C8',
                                                        height: 40,
                                                        backgroundColor: 'white',
                                                        paddingRight: 15,
                                                        flex: .9,
                                                        borderTopLeftRadius: 6,
                                                        borderBottomLeftRadius: 6,
                                                        borderBottomRightRadius: .1,
                                                        borderTopRightRadius: 1,
                                                        fontSize: 14,
                                                        color: '#7A8299',
                                                        fontFamily: 'IRANSansMobile(FaNum)'
                                                    }}
                                                    onChangeText={(text) => this.setState({fname: text})}/>
                                                <View style={{
                                                    backgroundColor: '#C8C8C8',
                                                    borderTopRightRadius: 6,
                                                    borderBottomRightRadius: 6,
                                                    alignItems: 'center',
                                                    justifyContent: 'center',
                                                    height: 40
                                                }}>
                                                    <Icon name={'user'} style={{fontSize: 23, paddingRight: 15, paddingLeft: 15}}/>
                                                </View>
                                            </View>
                                            <View style={{display: 'flex', flexDirection: 'row', marginTop: 20}}>
                                                <TextInput
                                                    placeholder="نام خانوادگی"
                                                    placeholderTextColor={'#C8C8C8'}
                                                    underlineColorAndroid='transparent'
                                                    value={this.state.lname}
                                                    style={{
                                                        textAlign: 'right',
                                                        borderWidth: 1,
                                                        borderColor: '#C8C8C8',
                                                        height: 40,
                                                        backgroundColor: 'white',
                                                        paddingRight: 15,
                                                        flex: .9,
                                                        borderTopLeftRadius: 6,
                                                        borderBottomLeftRadius: 6,
                                                        borderBottomRightRadius: .1,
                                                        borderTopRightRadius: 1,
                                                        fontSize: 14,
                                                        color: '#7A8299',
                                                        fontFamily: 'IRANSansMobile(FaNum)'
                                                    }}
                                                    onChangeText={(text) => this.setState({lname: text})}/>
                                                <View style={{
                                                    backgroundColor: '#C8C8C8',
                                                    borderTopRightRadius: 6,
                                                    borderBottomRightRadius: 6,
                                                    alignItems: 'center',
                                                    justifyContent: 'center',
                                                    height: 40
                                                }}>
                                                    <Icon name={'user'} style={{fontSize: 23, paddingRight: 15, paddingLeft: 15}}/>
                                                </View>
                                            </View>
                                            <View style={{display: 'flex', flexDirection: 'row', marginTop: 20}}>
                                                <TextInput
                                                    placeholder="کد ملی"
                                                    placeholderTextColor={'#C8C8C8'}
                                                    underlineColorAndroid='transparent'
                                                    keyboardType={'numeric'}
                                                    value={this.state.nationalId}
                                                    style={{
                                                        textAlign: 'right',
                                                        borderWidth: 1,
                                                        borderColor: '#C8C8C8',
                                                        height: 40,
                                                        backgroundColor: 'white',
                                                        paddingRight: 15,
                                                        flex: .9,
                                                        borderTopLeftRadius: 6,
                                                        borderBottomLeftRadius: 6,
                                                        borderBottomRightRadius: .1,
                                                        borderTopRightRadius: 1,
                                                        fontSize: 14,
                                                        color: '#7A8299',
                                                        fontFamily: 'IRANSansMobile(FaNum)'
                                                    }}
                                                    onChangeText={(text) => this.setState({nationalId: text})}/>
                                                <View style={{
                                                    backgroundColor: '#C8C8C8',
                                                    borderTopRightRadius: 6,
                                                    borderBottomRightRadius: 6,
                                                    alignItems: 'center',
                                                    justifyContent: 'center',
                                                    height: 40
                                                }}>
                                                    <MIcon name={'confirmation-number'} style={{fontSize: 23, paddingRight: 15, paddingLeft: 15}}/>
                                                </View>
                                            </View>
                                            <View style={{display: 'flex', flexDirection: 'row', marginTop: 20}}>
                                                <TextInput
                                                    placeholder="پسورد"
                                                    secureTextEntry
                                                    placeholderTextColor={'#C8C8C8'}
                                                    underlineColorAndroid='transparent'
                                                    value={this.state.password}
                                                    style={{
                                                        textAlign: 'right',
                                                        borderWidth: 1,
                                                        borderColor: '#C8C8C8',
                                                        height: 40,
                                                        backgroundColor: 'white',
                                                        paddingRight: 15,
                                                        flex: .9,
                                                        borderTopLeftRadius: 6,
                                                        borderBottomLeftRadius: 6,
                                                        borderBottomRightRadius: .1,
                                                        borderTopRightRadius: 1,
                                                        fontSize: 14,
                                                        color: '#7A8299',
                                                        fontFamily: 'IRANSansMobile(FaNum)'
                                                    }}
                                                    onChangeText={(text) => this.setState({password: text})}/>
                                                <View style={{
                                                    backgroundColor: '#C8C8C8',
                                                    borderTopRightRadius: 6,
                                                    borderBottomRightRadius: 6,
                                                    alignItems: 'center',
                                                    justifyContent: 'center',
                                                    height: 40
                                                }}>
                                                    <Icon name={'key'} style={{fontSize: 23, paddingRight: 15, paddingLeft: 15}}/>
                                                </View>
                                            </View>
                                            <Text style={styles.title}>تاریخ تولد</Text>
                                            <View style={{flexDirection: 'row',  width: '90%', borderRadius: 10, alignItems: 'center', justifyContent: 'space-between', paddingTop: 20, paddingBottom: 20}}>
                                                <Selectbox
                                                    style={{width: '30%', height: 35, paddingTop: '3%', paddingRight: 10, backgroundColor: 'lightgray', borderRadius: 5}}
                                                    selectLabelStyle={{textAlign: 'right', color: 'black', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                                    optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                    selectedItem={this.state.birthdayYear}
                                                    cancelLabel="لغو"
                                                    onChange={(itemValue) =>{
                                                        this.setState({
                                                            birthdayYear: itemValue
                                                        })}}
                                                    items={yearArray2} />
                                                <Selectbox
                                                    style={{width: '30%', height: 35, paddingTop: '3%', paddingRight: 10, backgroundColor: 'lightgray', borderRadius: 5}}
                                                    selectLabelStyle={{textAlign: 'right', color: 'black', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                                    optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                    selectedItem={this.state.birthdayMonth}
                                                    cancelLabel="لغو"
                                                    onChange={(itemValue) =>{
                                                        this.setState({
                                                            birthdayMonth: itemValue
                                                        })}}
                                                    items={monthArray} />
                                                <Selectbox
                                                    style={{width: '30%', height: 35, paddingTop: '3%', paddingRight: 10, backgroundColor: 'lightgray', borderRadius: 5}}
                                                    selectLabelStyle={{textAlign: 'right', color: 'black', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                                    optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                    selectedItem={this.state.birthdayDay}
                                                    cancelLabel="لغو"
                                                    onChange={(itemValue) =>{
                                                        this.setState({
                                                            birthdayDay: itemValue
                                                        })}}
                                                    items={dayArray} />

                                            </View>
                                        </View> : null
                                }
                                <TouchableOpacity onPress={() => this.confirm() } style={styles.advertise}>
                                    <LinearGradient start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['rgb(0, 114, 255)', 'rgb(0, 128, 255)', 'rgb(0, 142, 255)']} style={[styles.advertise, {  borderRadius: 40}]}>
                                        <View>
                                            <Text style={styles.buttonTitle}>ورود</Text>
                                        </View>
                                    </LinearGradient>
                                </TouchableOpacity>
                                {/*<View style={styles.footer}>*/}
                                {/*<TouchableOpacity onPress={() => Actions.push('register')}>*/}
                                {/*<Text style={styles.signup}>ثبت نام کنید</Text>*/}
                                {/*</TouchableOpacity>*/}
                                {/*<Text style={styles.footerText}>کاربر جدید هستید؟ / </Text>*/}
                                {/*</View>*/}
                            </View>
                        </ScrollView>
                        <View  style={styles.footerContainer}>
                            <View style={styles.footer}>
                                <TouchableOpacity onPress={() => this.resend()}>
                                    <Text style={styles.signup}>ارسال مجدد</Text>
                                </TouchableOpacity>
                                <Text style={styles.footerText}>کد جدید دریافت نکردید؟ / </Text>
                            </View>
                            <TouchableOpacity onPress={() => Actions.push('login')}>
                                <Text style={[styles.signup, {paddingBottom: 12}]}>تغییر شماره</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                }
                {/*<Image style={{height:'20%',zIndex:-1,width:'100%',position:'absolute',bottom:0}} resizeMode={'cover'} source={require('../../assets/company-hero-3.png')}/>*/}
                <AlertView
                    closeModal={(title) => this.closeModal(title)}
                    modalVisible={this.state.modalVisible}
                    // onChange={() => this.setState({modalVisible: false})}
                    title={this.state.correct ? 'لطفا شماره موبایل خود را صحیح وارد کنید':(this.state.wifi ? 'لطفا وضعیت وصل بودن به اینترنت را بررسی کنید': (this.state.codeDetect ? 'لطفا کد صحیح را وارد کنید' :( this.state.fill ? 'لطفا تمام موارد را پر نمایید' : (this.state.national ? 'لطفا کد ملی را صحیح وارد کنید': ( this.state.passMassage ? 'تعداد کاراکتر های پسورد نباید کمتر از 6 باشد' : (this.state.codeValidate ? 'لطفا کد فعال سازی را صحیح وارد کنید' : 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'))))))}
                />
            </KeyboardAvoidingView>
        );
    }
}

export default Login;