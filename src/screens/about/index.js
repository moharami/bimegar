
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bimegaronline.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import HTML from 'react-native-render-html';
import AlertView from '../../components/modalMassage'

class About extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            status: 1,
            activeButton: 1,
            activeVacle: 1,
            showPicker: false,
            selectedStartDate: null,
            aboutData: '',
            data: null,
            modalVisible: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.insuranceBuy({openDrawer: this.props.openDrawer})
        // Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    closeModal() {
        this.setState({modalVisible: false});
    }
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        this.setState({loading: true});
            Axios.post('/request/setting/settings-site', {
                section_name: 'about_us'
            }).then(response => {
                if(response.data.data) {
                    const myHtml = '<p style="color: rgba(51, 54, 64, 1); lineHeight: 30;">' +  response.data.data.static + '</p>'
                    this.setState({loading: false, aboutData: myHtml , data:  response.data.data !== null ? response.data.data.static : null});
                }
                this.setState({loading: false});

            })
            .catch((error) =>{
                // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                this.setState({modalVisible: true, loading: false});
            });
    }
    render() {
        // const {posts, categories, user} = this.props;
        if(this.state.loading){
            return (<Loader />)
        }
        else return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <LinearGradient  start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['rgb(0, 114, 255)', 'rgb(0, 128, 255)', 'rgb(0, 142, 255)']} style={styles.linearcontainer}>
                        <View style={styles.top}>
                            <TouchableOpacity onPress={() => this.onBackPress()}>
                                <FIcon name="arrow-left" size={20} color="white"  />
                            </TouchableOpacity>
                            <Text style={styles.headerTitle}> بیمه گر چیست ؟</Text>
                            <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                                <Icon name="bars" size={20} color="white" />
                            </TouchableOpacity>
                        </View>
                    </LinearGradient>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <Text style={{   direction: "rtl",
                            fontFamily: 'IRANSansMobile(FaNum)',
                            color: 'rgba(51, 54, 64, 1)',
                            lineHeight: 20,
                            paddingBottom: 10,
                            // paddingTop: 20,
                            fontSize: 14}}>
                            {
                                this.state.data
                            }
                        </Text>
                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}

                            title='مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}
export default About;

