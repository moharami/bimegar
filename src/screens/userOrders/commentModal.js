
import React, { Component } from 'react';
import {
    Modal,
    Text,
    View,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    AsyncStorage,
    Image,
    Alert
}
    from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import Axios from 'axios';
export const url = 'http://bimegaronline.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'

import pic2 from '../../assets/insurances/2.png'
import pic3 from '../../assets/insurances/3.png'
import pic4 from '../../assets/insurances/4.png'
import pic5 from '../../assets/insurances/5.png'
import pic6 from '../../assets/insurances/6.png'
import pic7 from '../../assets/insurances/7.png'
import pic8 from '../../assets/insurances/8.png'
import pic9 from '../../assets/insurances/9.png'
import pic10 from '../../assets/insurances/10.png'
import pic11 from '../../assets/insurances/11.png'
import pic12 from '../../assets/insurances/12.png'
import pic13 from '../../assets/insurances/13.png'
import pic14 from '../../assets/insurances/14.png'
import pic15 from '../../assets/insurances/15.png'
import pic18 from '../../assets/insurances/18.png'
import pic19 from '../../assets/insurances/19.png'
import pic20 from '../../assets/insurances/20.png'
import pic21 from '../../assets/insurances/21.png'
import pic22 from '../../assets/insurances/22.png'
import pic23 from '../../assets/insurances/23.png'
import AlertView from '../../components/modalMassage'

class CommentModal extends Component {
    state = {
        text: '',
        row1Star1: false,
        row1Star2: false,
        row1Star3: false,
        row1Star4: false,
        row1Star5: false,

        row2Star1: false,
        row2Star2: false,
        row2Star3: false,
        row2Star4: false,
        row2Star5: false,

        row3Star1: false,
        row3Star2: false,
        row3Star3: false,
        row3Star4: false,
        row3Star5: false,
        loading: false,
        confirm: false,
        modalVisible: false,
    };
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false})}, 200)
        this.setState({ selectedStartDate: date });
    }
    sendComment() {
        let numberOfrow1 = 0;
        if(this.state.row1Star1) {
            ++numberOfrow1;
        }
        if(this.state.row1Star2) {
            ++numberOfrow1;
        }
        if(this.state.row1Star3) {
            ++numberOfrow1;
        }
        if(this.state.row1Star4) {
            ++numberOfrow1;
        }
        if(this.state.row1Star5) {
            ++numberOfrow1;
        }


        let numberOfrow2 = 0;
        if(this.state.row2Star1) {
            ++numberOfrow2;
        }
        if(this.state.row2Star2) {
            ++numberOfrow2;
        }
        if(this.state.row2Star3) {
            ++numberOfrow2;
        }
        if(this.state.row2Star4) {
            ++numberOfrow2;
        }
        if(this.state.row2Star5) {
            ++numberOfrow2;
        }

        let numberOfrow3 = 0;
        if(this.state.row3Star1) {
            ++numberOfrow3;
        }
        if(this.state.row3Star2) {
            ++numberOfrow3;
        }
        if(this.state.row3Star3) {
            ++numberOfrow3;
        }
        if(this.state.row3Star4) {
            ++numberOfrow3;
        }
        if(this.state.row3Star5) {
            ++numberOfrow3;
        }
        console.log('id', numberOfrow1)
        console.log('id', numberOfrow2)
        console.log('id', numberOfrow3)
        console.log('user id', this.props.userId)


        this.setState({loading: true});
        Axios.post('/request/star', {
            text_content: this.state.text,
            star_price: numberOfrow1 ,
            star_payment: numberOfrow2,
            star_request: numberOfrow3,
            user_id: this.props.userId,
            insurance_id: this.props.insId,
            insurance_type_id: null

        }).then(response=> {
            this.setState({loading: false, modalVisible: true, confirm: true});
            // this.setState({loading: false});
            // Alert.alert('نظر شما با موفقیت ثبت شد')
            // alert('موفق')
            console.log(response.data.data)

        })
        .catch((error) => {
        this.setState({modalVisible: true, loading: false});
        //     Alert.alert('مشکلی در برقراری ارتباط با سرور رخ داده لطفا مجددا تلاش نمایید')
            console.log(error.response)


        });


    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    render() {
        const id = this.props.insId;
        console.log('id', id)
        let imgSrc = null;
        switch (id) {
            case 2:
                imgSrc = pic2;
                break;
            case 3:
                imgSrc = pic3;
                break;
            case 4:
                imgSrc = pic4;
                break;
            case 5:
                imgSrc = pic5;
                break;
            case 6:
                imgSrc = pic6;
                break;
            case 7:
                imgSrc = pic7;
                break;
            case 8:
                imgSrc = pic8;
                break;
            case 9:
                imgSrc = pic9;
                break;
            case 10:
                imgSrc = pic10;
                break;
            case 11:
                imgSrc = pic11;
                break;
            case 12:
                imgSrc = pic12;
                break;
            case 13:
                imgSrc = pic13;
                break;
            case 14:
                imgSrc = pic14;
                break;
            case 15:
                imgSrc = pic5;
                break;
            case 18:
                imgSrc = pic18;
                break;
            case 19:
                imgSrc = pic19;
                break;
            case 20:
                imgSrc = pic20;
                break;
            case 21:
                imgSrc = pic21;
                break;
            case 22:
                imgSrc = pic22;
                break;
            case 23:
                imgSrc = pic23;
                break;
        }

        if(this.state.loading){
            return <Loader />;
        }
        else return (
            <View style={styles.container}>
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.props.modalVisible}
                       onRequestClose = {() => this.props.onChange(false)}
                       onBackdropPress= {() => this.props.onChange(!this.state.modalVisible)}
                >
                    <View style = {styles.modal}>
                        <View style={styles.box}>
                            <View style={styles.headerContainer}>
                                <TouchableOpacity onPress={() => this.props.closeModal()}>
                                    <Icon name="close" size={20} color={ "black"} />
                                </TouchableOpacity>
                                <Text style={styles.headerText}>نظر و امتیاز</Text>
                            </View>
                            <Text style={styles.title}>امتیاز و نظر شما درباره {this.props.insTitle}</Text>
                            <View style={styles.info}>
                                <View style={styles.rowsContainer}>
                                    <View style={styles.row}>
                                        <View style={{flexDirection: 'row'}}>
                                            <TouchableOpacity onPress={() => this.setState({row1Star1: !this.state.row1Star1})}>
                                                <Icon name={this.state.row1Star1 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.setState({row1Star2: !this.state.row1Star2})}>
                                                <Icon name={this.state.row1Star2 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.setState({row1Star3: !this.state.row1Star3})}>
                                                <Icon name={this.state.row1Star3 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.setState({row1Star4: !this.state.row1Star4})}>
                                                <Icon name={this.state.row1Star4 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.setState({row1Star5: !this.state.row1Star5})}>
                                                <Icon name={this.state.row1Star5 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                            </TouchableOpacity>
                                        </View>
                                        <Text style={styles.title}>قیمت</Text>
                                    </View>
                                    <View style={styles.row}>
                                        <View style={{flexDirection: 'row'}}>
                                            <TouchableOpacity onPress={() => this.setState({row2Star1: !this.state.row2Star1})}>
                                                <Icon name={this.state.row2Star1 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.setState({row2Star2: !this.state.row2Star2})}>
                                                <Icon name={this.state.row2Star2 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.setState({row2Star3: !this.state.row2Star3})}>
                                                <Icon name={this.state.row2Star3 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.setState({row2Star4: !this.state.row2Star4})}>
                                                <Icon name={this.state.row2Star4 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.setState({row2Star5: !this.state.row2Star5})}>
                                                <Icon name={this.state.row2Star5 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                            </TouchableOpacity>
                                        </View>
                                        <Text style={styles.title}>رضایت از پرداخت</Text>
                                    </View>
                                    <View style={styles.row}>
                                        <View style={{flexDirection: 'row'}}>
                                            <TouchableOpacity onPress={() => this.setState({row3Star1: !this.state.row3Star1})}>
                                                <Icon name={this.state.row3Star1 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.setState({row3Star2: !this.state.row3Star2})}>
                                                <Icon name={this.state.row3Star2 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.setState({row3Star3: !this.state.row3Star3})}>
                                                <Icon name={this.state.row3Star3 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.setState({row3Star4: !this.state.row3Star4})}>
                                                <Icon name={this.state.row3Star4 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.setState({row3Star5: !this.state.row3Star5})}>
                                                <Icon name={this.state.row3Star5 ? "star" : "star-o"} size={14} color={ "rgb(253, 185, 11)"} />
                                            </TouchableOpacity>
                                        </View>
                                        <Text style={styles.title}>پاسخگویی</Text>
                                    </View>
                                </View>
                                <View style={styles.imageContainer}>
                                    <Image source={imgSrc} style={{width: 50, height: 50, resizeMode: 'contain'}} />
                                </View>
                            </View>
                            <TextInput
                                multiline = {true}
                                numberOfLines = {4}
                                value={this.state.text}
                                onChangeText={(text) => this.setState({text: text})}
                                placeholderTextColor={'rgb(142, 142, 142)'}
                                underlineColorAndroid='transparent'
                                placeholder="نظر شما..."
                                style={{
                                    // height: 45,
                                    paddingRight: 15,
                                    width: '90%',
                                    fontSize: 14,
                                    color: 'rgb(50, 50, 50)',
                                    textAlign:'right',
                                    fontFamily: 'IRANSansMobile(FaNum)',
                                    borderWidth: 1,
                                    borderColor: 'rgb(150, 150, 150)',
                                    borderRadius: 10,
                                    marginTop: 20,
                                    marginBottom: 20
                                }}
                            />
                            <TouchableOpacity onPress={() => this.sendComment()} style={styles.advertise}>
                                <Text style={styles.buttonTitle}>تایید</Text>
                            </TouchableOpacity>
                            <AlertView
                                closeModal={(title) => this.closeModal(title)}
                                modalVisible={this.state.modalVisible}
                                //
                                title={this.state.confirm ? 'نظر شما با موفقیت ثبت شد' : 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'}
                            />
                        </View>

                    </View>
                </Modal>
            </View>
        )
    }
}
export default CommentModal

const styles = StyleSheet.create ({
    container: {
        alignItems: 'center',
        // backgroundColor: 'white',
        padding: 10,
        position: 'absolute',
        bottom: -200
    },
    modal: {
        position: 'relative',
        zIndex: 0,
        flexGrow: 1,
        justifyContent: 'center',
        // flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,.6)',
        paddingRight: 20,
        paddingLeft: 20
    },
    text: {
        color: '#3f2949',
        marginTop: 10
    },
    box: {
        width: '100%',
        backgroundColor: 'rgb(218, 218, 218)',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        borderRadius: 15,
        paddingBottom: 20
    },
    input: {
        width: '100%',
        fontSize: 16,
        paddingTop: 0,
        textAlign: 'right',
    },
    searchButton: {
        width: '90%',
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(20, 122, 170)'
    },
    searchText:{
        color: 'white'
    },
    picker: {
        height:50,
        width: "100%",
        alignSelf: 'flex-end'
    },
    label: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(60, 60, 60)'
    },
    headerContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 8,
        borderBottomWidth: 1,
        borderBottomColor: 'rgb(237, 237, 237)',
        marginBottom: 20
    },
    headerText: {
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    advertise: {
        width: '35%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 5,
        // marginTop: 0
    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    inputLabel: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingRight: '10%',
        paddingBottom: 5
    },
    insContainer: {
        width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        // alignItems: 'center',
        // justifyContent: 'space-between',
        padding: 8
    },
    info: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: 20,
        paddingBottom: 20,
        borderBottomColor: 'white',
        borderBottomWidth: 1
    },
    rowsContainer: {
        width: '70%'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    itemContainer: {
        width: '48%'
    },
    title: {
        color: 'rgb(60, 60, 60)',
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingRight: 10
    },
    imageContainer: {
        borderRadius: 15,
        padding: 7,
        backgroundColor: 'white',
        width: '20%',
        marginRight: 10,
        marginLeft: 10
    },
})