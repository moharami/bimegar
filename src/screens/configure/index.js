
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bimegaronline.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import { CheckBox } from 'react-native-elements'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import AlertView from '../../components/modalMassage'

class Configure extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            status: 1,
            activeButton: 1,
            activeVacle: 1,
            showPicker: false,
            selectedStartDate: null,
            checked: false,
            settingData: [],
            modalVisible: false,
            setting: false,
            notif: false,
            item: {}

        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.insuranceBuy({openDrawer: this.props.openDrawer})
        // Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        this.setState({loading: true});
        AsyncStorage.getItem('token').then((info) => {
            const newInfo = JSON.parse(info);
            Axios.post('/request/setting/settings-user', {
                user_id: newInfo.user_id,
                source: 'android'
            }).then(response => {
                this.setState({loading: false, settingData: response.data.data});
                {
                    this.state.settingData.map((item, index)=> {
                        if(item.section === "notifacation" && this.state.notif === false){
                            this.setState({notif : true, item: item })
                        }
                    })
                }
            })
            .catch((error) =>{
                console.log(error)
                // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                // this.setState({loading: false});
                this.setState({modalVisible: true, loading: false});
            });
        })
    }
    sendSetting() {
        AsyncStorage.getItem('token').then((info) => {
            const newInfo = JSON.parse(info);
            Axios.post('/request/setting/set-setting-user', {
                user_id: newInfo.user_id,
                section:'notifacation',
                value: 1,
                bool_value:this.state.checked ? 1: 0,
                static_value: 5755778
            }).then(response => {
                this.setState({modalVisible: true, loading: false, setting: true});
                // Alert.alert('','تنظیمات شمااعمال شد');

            })
            .catch((error) =>{
                console.log(error)
                // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                // this.setState({loading: false});
                this.setState({modalVisible: true, loading: false});
            });
        })
    }
    render() {

        // const {posts, categories, user} = this.props;
        const myHtml = '<p style="color: rgba(51, 54, 64, 1); lineHeight: 30;">' + 'تسنشتیتس مننتسی تسای ' + '</p>';
        if(this.state.loading){
            return (<Loader />)
        }
        else return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgba(60, 177, 232, 1)', 'rgba(62, 64, 219, 1)']} style={styles.linearcontainer}>
                        <View style={styles.top}>
                            <TouchableOpacity onPress={() => this.onBackPress()}>
                                <FIcon name="arrow-left" size={20} color="white"  />
                            </TouchableOpacity>
                            <Text style={styles.headerTitle}>تنظیمات</Text>
                            <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                                <Icon name="bars" size={20} color="white" />
                            </TouchableOpacity>
                        </View>
                    </LinearGradient>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        {
                            this.state.notif ?
                                <View style={styles.timeContainer}>
                                    <Text style={styles.label}>آیا مایل به دریافت اعلانات هستید؟</Text>
                                    <CheckBox
                                        containerStyle={{backgroundColor: 'transparent', borderWidth: 0, padding: 0}}
                                        // center
                                        // title='Click Here'
                                        checkedIcon='dot-circle-o'
                                        uncheckedIcon='circle-o'
                                        checked={this.state.checked}
                                        onPress={() => this.setState({checked: !this.state.checked}, ()=> this.sendSetting())}
                                    />
                                </View> :
                                null
                        }


                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}

                            title={this.state.setting ? 'تنظیمات شمااعمال شد' : 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}
export default Configure;


