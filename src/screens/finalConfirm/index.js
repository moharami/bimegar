import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput} from 'react-native';
import styles from './styles'
import FIcon from 'react-native-vector-icons/Feather';
import {Actions} from 'react-native-router-flux'
import Axios from 'axios';
export const url = 'http://bimegaronline.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import HomeHeader from '../../components/homeHeader'
import moment_jalaali from 'moment-jalaali'
import AlertView from '../../components/modalMassage'
import check from '../../assets/check.png'

class FinalConfirm extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            status: 1,
            activeButton: 1,
            activeVacle: 1,
            showPicker: false,
            selectedStartDate: null,
            title: '',
            modalVisible: false,
            order: false,
            priceText: ''
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    closeModal() {
        this.setState({modalVisible: false});
    }
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        console.log('final factor',this.props.factor)

        console.log('insu id', this.props.factor.insurance_id )
        Axios.post('/request/get_insurance_detail', {
            id: this.props.factor.insurance_id
        }).then(response=> {
            this.setState({title: response.data.data[0].title, loading: false})
            console.log('insu response', response)
            console.log('insu response', response.data.data[0].title)

        })
            .catch((error) => {
                console.log(error)
                this.setState({modalVisible: true, loading: false});

            });
    }
    confirmData() {
        console.log('insurType:this.props.insurType',this.props.insurType)
        this.setState({loading: true});
        AsyncStorage.getItem('token').then((info) => {
            const newInfo = JSON.parse(info);
            // Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
            Axios.defaults.headers.common['Content-Type'] = 'multipart/form-data' ;
            console.log('imagesss', this.props.images)
            console.log('factor', this.props.factor)
            console.log('user_details', this.props.user_details)
            let formdata = new FormData();
            formdata.append("id",newInfo.user_id)
            formdata.append("factorInfo",JSON.stringify(this.props.factor));
            // formdata.append("dataSelect", dataSelect)
            formdata.append("user_details",JSON.stringify(this.props.user_details));
            formdata.append("type",this.props.insurType);
            if(this.props.insurType === 'body') {
                console.log('this.props.factor.dataSelect.third_car_model.length', this.props.factor.dataSelect.body_car_model.length)
                if(this.props.images.src1 !== null) {
                    formdata.append("overCardBody",{uri:this.props.images.src1,type:"image/jpeg",name:"overCardBody"});
                }
                else if(this.props.images.src2 !== null) {
                    formdata.append("underCardBody",{uri:this.props.images.src2,type:"image/jpeg",name:"underCardBody"});
                }
                else if(this.props.images.src3 !== null) {
                    formdata.append("greenCardBody",{uri:this.props.images.src3,type:"image/jpeg",name:"greenCardBody"});
                }
                else if(this.props.images.src4 !== null) {
                    formdata.append("nationalCardBody",{uri:this.props.images.src4,type:"image/jpeg",name:"nationalCardBody"});
                }
                else if(this.props.images.src5 !== null) {
                    formdata.append("prevInsuranceBody",{uri:this.props.images.src5,type:"image/jpeg",name:"prevInsuranceBody"});
                }

            }
            if(this.props.insurType === 'Third') {
                console.log('this.props.factor.dataSelect.third_car_model.length', this.props.factor.dataSelect.third_car_model.length)

                if(this.props.images.src1 !== null) {
                    formdata.append("overCardThird",{uri:this.props.images.src1,type:"image/jpeg",name:"overCardThird"});
                }
                else if(this.props.images.src2 !== null) {
                    formdata.append("underCardThird",{uri:this.props.images.src2,type:"image/jpeg",name:"underCardThird"});
                }
            }
            if(this.props.insurType === 'motor') {

                if(this.props.images.src1 !== null) {
                    formdata.append("overCardMotor",{uri:this.props.images.src1,type:"image/jpeg",name:"overCardMotor"});
                }
                else if(this.props.images.src2 !== null) {
                    formdata.append("underCardMotor",{uri:this.props.images.src2,type:"image/jpeg",name:"underCardMotor"});
                }
                else if(this.props.images.src3 !== null) {
                    formdata.append("greenCardMotor",{uri:this.props.images.src3,type:"image/jpeg",name:"greenCardMotor"});
                }
                else if(this.props.images.src4 !== null) {
                    formdata.append("nationalCardMotor",{uri:this.props.images.src4,type:"image/jpeg",name:"nationalCardMotor"});
                }
                else if(this.props.images.src5 !== null) {
                    formdata.append("prevInsuranceMotor",{uri:this.props.images.src5,type:"image/jpeg",name:"prevInsuranceMotor"});
                }
            }
            else if(this.props.insurType === 'fire' || this.props.insurType === 'earth') {

                if(this.props.images.src1 !== null) {
                    formdata.append("nationalCardFire",{uri:this.props.images.src1,type:"image/jpeg",name:"nationalCardFire"});
                }
            }
            else if(this.props.insurType === 'complete') {
                if(this.props.images.src1 !== null) {
                    formdata.append("insurancerComplete",{uri:this.props.images.src1,type:"image/jpeg",name:"insurancerComplete"});
                }
                else if(this.props.images.src2 !== null) {
                    formdata.append("healthQuestionComplete",{uri:this.props.images.src2,type:"image/jpeg",name:"underCardMotorComplete"});
                }
                else if(this.props.images.src3 !== null) {
                    formdata.append("identifyCardComplete",{uri:this.props.images.src3,type:"image/jpeg",name:"identifyCardComplete"});
                }
                else if(this.props.images.src4 !== null) {
                    formdata.append("nationalCardComplete",{uri:this.props.images.src4,type:"image/jpeg",name:"nationalCardComplete"});
                }
            }
            else if(this.props.insurType === 'travel') {

                if(this.props.images.src1 !== null) {
                    formdata.append("passport",{uri:this.props.images.src1,type:"image/jpeg",name:"passport"});
                }
            }
            else if(this.props.insurType === 'responsible') {

                if(this.props.images.src1 !== null) {
                    formdata.append("identifyCardResponsible",{uri:this.props.images.src1,type:"image/jpeg",name:"identifyCardResponsible"});
                }
                else if(this.props.images.src2 !== null) {
                    formdata.append("healthQuestionResponsible",{uri:this.props.images.src2,type:"image/jpeg",name:"underCardMotorResponsible"});
                }
                else if(this.props.images.src3 !== null) {
                    formdata.append("nationalCardResponsible",{uri:this.props.images.src3,type:"image/jpeg",name:"nationalCardResponsible"});
                }

            }
            else if(this.props.insurType === 'life') {
                if(this.props.images.src1 !== null) {
                    formdata.append("nationalCardLife",{uri:this.props.images.src1,type:"image/jpeg",name:"nationalCardLife"});
                }
            }
            else if(this.props.insurType === 'asansor') {
                if(this.props.images.src1 !== null) {
                    formdata.append("nationalCardAsansor",{uri:this.props.images.src1,type:"image/jpeg",name:"nationalCardAsansor"});
                }
            }
            else if(this.props.instalment) {
                if(this.props.images.src1 !== null) {
                    formdata.append("instalment1",{uri:this.props.images.src1,type:"image/jpeg",name:"instalment1"});
                }
                else if(this.props.images.src2 !== null) {
                    formdata.append("instalment2",{uri:this.props.images.src2,type:"image/jpeg",name:"instalment2"});
                }
            }
            console.log('formdata', formdata)
            Axios.post('/request/set_final_body', formdata
            ).then(response=> {
                // Alert.alert('','سفارش شما تایید شد');
                this.setState({loading: false, order: true, modalVisible: true});

                const insuranceInfo = {'title': this.props.insTitle, 'id': this.props.factor.insurance_id};
                const newwwwAsync = AsyncStorage.setItem('insuranceInfo', JSON.stringify(insuranceInfo));
                console.log('newwwwAsync 2222', newwwwAsync);

                console.log('edited item', response.data.invoice_id);

                Actions.payment({openDrawer: this.props.openDrawer, pageTitle: 'پرداخت', factor:this.props.factor,user_details:this.props.user_details,insurType:this.props.insurType, images: this.props.images, instalment: this.props.instalment, invoice_id: response.data.invoice_id})

            })
                .catch((error) => {
                    console.log('edited item', error);
                    Alert.alert('','خطا');
                    this.setState({loading: false});
                    console.log(error)
                });
        });

    }
    toPersian(num){
        let NumToPersian = (function () {
            /**
             *
             * @type {string}
             */
            spliter = " و ";

            /**
             *
             * @type {string}
             */
            zero = "صفر";

            /**
             *
             * @type {*[]}
             */
            Letters = [
                ["", "یك", "دو", "سه", "چهار", "پنج", "شش", "هفت", "هشت", "نه"],
                ["ده", "یازده", "دوازده", "سیزده", "چهارده", "پانزده", "شانزده", "هفده", "هجده", "نوزده","بیست"],
                ["","","بیست", "سی", "چهل", "پنجاه", "شصت", "هفتاد", "هشتاد", "نود"],
                ["", "یكصد", "دویست", "سیصد", "چهارصد", "پانصد", "ششصد", "هفتصد", "هشتصد", "نهصد"],
                ['', " هزار ", " میلیون ", " میلیارد ", " بیلیون ", " بیلیارد ", " تریلیون ", " تریلیارد ",
                    " کوآدریلیون ", " کادریلیارد ", " کوینتیلیون ", " کوانتینیارد ", " سکستیلیون ", " سکستیلیارد ", " سپتیلیون ",
                    " سپتیلیارد ", " اکتیلیون ", " اکتیلیارد ", " نانیلیون ", " نانیلیارد ", " دسیلیون ", " دسیلیارد "]
            ];

            /**
             * Clear number and split to 3th sections
             * @param {*} num
             */
            function PrepareNumber(num){
                if(typeof num === "number"){
                    num = num.toString();
                }
                NumberLength = num.length%3;
                if(NumberLength === 1){
                    num = "00"+num;
                }else if(NumberLength === 2){
                    num = "0"+num;
                }
                //Explode to array
                return num.replace(/\d{3}(?=\d)/g, "$&*").split('*');
            }

            /**
             * Convert 3 numbers into letter
             * @param {*} num
             */
            function ThreeNumbersToLetter(num){
                //return zero
                if(parseInt(num) === 0){
                    return "";
                }
                parsedInt = parseInt(num);
                if(parsedInt < 10){
                    return Letters[0][parsedInt];
                }
                if(parsedInt <= 20){
                    return Letters[1][parsedInt-10];
                }
                if(parsedInt < 100){
                    one = parsedInt%10;
                    ten = (parsedInt-one)/10;
                    if(one > 0){
                        return Letters[2][ten] + spliter + Letters[0][one];
                    }
                    return Letters[2][ten];
                }
                one = parsedInt%10;
                hundreds = (parsedInt-parsedInt%100)/100;
                ten = (parsedInt-((hundreds*100)+one))/10;
                out = [Letters[3][hundreds]];
                SecendPart = ((ten*10)+one);
                if(SecendPart > 0){
                    if(SecendPart < 10){
                        out.push(Letters[0][SecendPart]);
                    }else if(SecendPart <= 20){
                        out.push(Letters[1][SecendPart-10]);
                    }else{
                        out.push(Letters[2][ten]);
                        if(one > 0){
                            out.push(Letters[0][one]);
                        }
                    }
                }
                return out.join(spliter);
            }

            /**
             * Main function
             */
            return function(num){
                //return zero
                if(parseInt(num) === 0){
                    return zero;
                }
                if(num.length > 66){
                    return "خارج از محدوده";
                }
                //Split to sections
                SplitedNumber = PrepareNumber(num);

                //Fetch Sections and convert
                funcout = [];
                SplitLength = SplitedNumber.length;
                for(i=0;i < SplitLength;i++){
                    SectionTitle = Letters[4][SplitLength-(i+1)];
                    converted = ThreeNumbersToLetter(SplitedNumber[i]);
                    if(converted !== ""){
                        funcout.push(converted+SectionTitle);
                    }
                }
                return funcout.join(spliter);
            };
        })();
        return NumToPersian(num)

    }
    render() {
        let now = Date.now();

        console.log('now', now);
        console.log('this.props.this.props.instalment', this.props.instalment);
        console.log('this.props.instalment && this.props.installmentType === tree', this.props.instalment && this.props.installmentType === 'tree');
        now = now +(24 * 3600 * 1000* 30);
        console.log('now +30', now)
        console.log('moment_jalaali(new Date()).forma', moment_jalaali(now).format('jYYYY/jM/jD'))

        console.log('selectedStartDateselectedStartDateselectedStartDate', moment_jalaali(new Date()).format('jYYYY/jM/jD').split('/'))
        const g = moment_jalaali(new Date()).format('jYYYY/jMM/jDD').split('/');
        console.log('g', g)

        const g1 = moment_jalaali(now +(24 * 3600 * 1000* 30)).format('jYYYY/jMM/jDD').split('/');
        console.log('g1', g1)

        const g2 = moment_jalaali(now +(24 * 3600 * 1000* 60)).format('jYYYY/jMM/jDD').split('/');
        console.log('g2', g2)


        console.log('selectedStartDateselectedStartDateselectedStartDate', g[0].split(''))
        const part1 = g[0].split('')
        const part2 = g[1].split('')
        const part3 = g[2].split('')

        const part1g1 = g1[0].split('')
        const part2g1 = g1[1].split('')
        const part3g1 = g1[2].split('')

        const part1g2 = g2[0].split('')
        const part2g2 = g2[1].split('')
        const part3g2 = g2[2].split('')


        console.log('part1[0]', part1[0])
        const priceNumeric = this.props.factor.dataSelect.fianl_price; ////////////////////// here should change
        console.log('this.props.factor.dataSelect.fianl_price', this.props.factor.dataSelect.fianl_price)
        console.log('priceNumeric.toString()', priceNumeric.toString())
        const priceNumericArray = priceNumeric.toString().split('');

        let arrayLeft = [];
        let baseLeft = 4
        for (let i = 0; i< priceNumeric.toString().length ; i++) {
            baseLeft = baseLeft+ 2.6;
            arrayLeft.push(baseLeft)
        }
        // const {posts, categories, user} = this.props;
        if(this.state.loading){
            return (<Loader />)
        }
        else return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <HomeHeader active={6} openDrawer={this.props.openDrawer} pageTitle={this.props.pageTitle}/>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <View style={{width: '100%', alignItems: 'center', justifyContent: 'center', position: 'relative', zIndex: 5}}>
                            <Text style={styles.titleTop}>مسئولیت تمام اطلاعات وارد شده بر عهده کاربر می باشد</Text>
                            {
                                this.props.instalment && this.props.installmentType === 'one' || this.props.instalment && this.props.installmentType === 'two' || this.props.instalment && this.props.installmentType === 'tree' ?
                                    <View style={{width: '100%',alignSelf: 'center'}}>
                                        <Image source={check} style={{width: '100%',alignSelf: 'center', marginBottom: 10}} />
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '30%', left: '11%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 6}}>{this.toPersian(this.props.factor.dataSelect.fianl_price)}</Text>

                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '66%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part1[0]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '68%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part1[1]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '71%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part1[2]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '73%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part1[3]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '76%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part2[0]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '78%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part2[1]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '81%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part3[0]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '83%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part3[1]}</Text>
                                        {
                                            priceNumericArray.map((item , index) => <Text key={index} style={{position: 'absolute', zIndex: 999999, top: '62%', left: arrayLeft[index]+'%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9}}>{item}</Text>
                                            )
                                        }
                                    </View> : null
                            }
                            {
                                this.props.instalment && this.props.installmentType === 'two' || this.props.instalment && this.props.installmentType === 'tree'?
                                    <View style={{width: '100%',alignSelf: 'center'}}>
                                        <Image source={check} style={{width: '100%',alignSelf: 'center', marginBottom: 10}} />
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '30%', left: '11%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 6}}>{this.toPersian(this.props.factor.dataSelect.fianl_price)}</Text>

                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '66%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part1g1[0]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '68%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part1g1[1]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '71%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part1g1[2]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '73%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part1g1[3]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '76%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part2g1[0]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '78%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part2g1[1]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '81%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part3g1[0]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '83%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part3g1[1]}</Text>
                                        {
                                            priceNumericArray.map((item , index) => <Text key={index} style={{position: 'absolute', zIndex: 999999, top: '62%', left: arrayLeft[index]+'%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9}}>{item}</Text>
                                            )
                                        }
                                    </View> : null
                            }
                            {
                                this.props.instalment && this.props.installmentType === 'tree' ?
                                    <View style={{width: '100%',alignSelf: 'center'}}>
                                        <Image source={check} style={{width: '100%',alignSelf: 'center', marginBottom: 10}} />
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '30%', left: '11%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 6}}>{this.toPersian(this.props.factor.dataSelect.fianl_price)}</Text>

                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '66%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part1g2[0]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '68%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part1g2[1]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '71%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part1g2[2]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '73%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part1g2[3]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '76%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part2g2[0]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '78%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part2g2[1]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '81%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part3g2[0]}</Text>
                                        <Text style={{position: 'absolute', zIndex: 999999, top: '10%', left: '83%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9, color: 'rgb(50, 197, 117)'}}>{part3g2[1]}</Text>
                                        {
                                            priceNumericArray.map((item , index) => <Text key={index} style={{position: 'absolute', zIndex: 999999, top: '62%', left: arrayLeft[index]+'%', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 9}}>{item}</Text>
                                            )
                                        }
                                    </View> : null
                            }
                        </View>
                        <View style={styles.labelContainer}>
                            <Text style={styles.topLabel}> مشخصات خرید</Text>
                            <FIcon name="box" size={20} color="gray" />
                        </View>
                        <View style={styles.body}>
                            <View style={styles.left}>
                                <View style={styles.bodyLeft}>
                                    <Text style={styles.bodyValue}>{this.state.title}</Text>
                                    <Text style={styles.bodyLabel}> شرکت بیمه: </Text>
                                </View>
                                <View style={styles.bodyLeft}>
                                    <Text style={styles.bodyValue}>{Math.ceil(this.props.factor.dataSelect.fianl_price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} ریال</Text>
                                    <Text style={styles.bodyLabel}> مبلغ کل بیمه: </Text>
                                </View>
                                {
                                    this.props.insurType === 'Third' || this.props.insurType === 'motor' ?
                                        <View style={styles.innerLeft}>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.commitments}</Text>
                                                <Text style={styles.bodyLabel}>حداکثر مقدار پوشش: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.third_car_years} سال</Text>
                                                <Text style={styles.bodyLabel}> تخفیف بیمه: </Text>
                                            </View>
                                        </View>
                                        : null
                                }
                            </View>

                            <View style={styles.right}>
                                {
                                    this.props.insurType ===  'life'?
                                        <View style={styles.innerRight}>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.life_pay}</Text>
                                                <Text style={styles.bodyLabel}>نحوه پرداخت: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.life_years}</Text>
                                                <Text style={styles.bodyLabel}> مدت قرارداد: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.life_cost}</Text>
                                                <Text style={styles.bodyLabel}> حق بیمه: </Text>
                                            </View>
                                        </View>
                                        : null
                                }
                                {
                                    this.props.insurType ===  'asansor'?
                                        <View style={styles.innerRight}>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.type}</Text>
                                                <Text style={styles.bodyLabel}>نوع آسانسور:: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.controler}</Text>
                                                <Text style={styles.bodyLabel}> کاربری آسانسور: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.capacity}</Text>
                                                <Text style={styles.bodyLabel}> ظرفیت آسانسور (نفر): </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.medical_commitment}</Text>
                                                <Text style={styles.bodyLabel}> حداکثر تعهد هزینه های پزشکی: </Text>
                                            </View>
                                        </View>
                                        : null
                                }
                                {
                                    this.props.insurType ===  'travel'?
                                        <View style={styles.innerRight}>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.travel_country}</Text>
                                                <Text style={styles.bodyLabel}>کشور: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.travel_type}</Text>
                                                <Text style={styles.bodyLabel}>نوع بیمه: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.travel_time}</Text>
                                                <Text style={styles.bodyLabel}>مدت اقامت: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.travel_age}</Text>
                                                <Text style={styles.bodyLabel}>سن: </Text>
                                            </View>
                                        </View>
                                        : null
                                }
                                {
                                    this.props.insurType ===  'fire' || this.props.insurType === 'earth'?
                                        <View style={styles.innerRight}>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.fire_home_type}</Text>
                                                <Text style={styles.bodyLabel}>نوع ملک: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.fire_structure}</Text>
                                                <Text style={styles.bodyLabel}>نوع سازه: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{Math.ceil(this.props.factor.dataSelect.fire_home_price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>

                                                <Text style={styles.bodyLabel}>ارزش لوازم خانگی(تومان): </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.fire_home_count}</Text>
                                                <Text style={styles.bodyLabel}>تعداد واحد: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.fire_meters}</Text>
                                                <Text style={styles.bodyLabel}>متراژ مورد بیمه: </Text>
                                            </View>
                                        </View>
                                        : null
                                }
                                {
                                    this.props.insurType ===  'complete'?
                                        <View style={styles.innerRight}>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.resp_type}</Text>
                                                <Text style={styles.bodyLabel}>نوع بیمه: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.insurancer}</Text>
                                                <Text style={styles.bodyLabel}>بیمه گر پایه: </Text>
                                            </View>
                                        </View>
                                        : null
                                }
                                {
                                    this.props.insurType ===  'responsible'?
                                        <View style={styles.innerRight}>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.resp_type_text}</Text>
                                                <Text style={styles.bodyLabel}>نوع بیمه: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.res_cost}</Text>
                                                <Text style={styles.bodyLabel}>نوع تخصص: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.res_job}</Text>
                                                <Text style={styles.bodyLabel}>تعداد دیه: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.res_time}</Text>
                                                <Text style={styles.bodyLabel}>مدت بیمه نامه: </Text>
                                            </View>
                                        </View>
                                        : null
                                }
                                {
                                    this.props.insurType === 'Third' || this.props.insurType === 'motor' ?
                                        <View style={styles.innerRight}>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.third_car_cate}</Text>
                                                <Text style={styles.bodyLabel}> نوع وسیله نقلیه: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.third_car_name}</Text>
                                                <Text style={styles.bodyLabel}>نام وسیله نقلیه:  </Text>
                                            </View>
                                            {
                                                this.props.factor.dataSelect.third_car_model.length > 16 ?
                                                    <View style={[styles.bodyLeft, {flexDirection: 'column'}]}>
                                                        <Text style={styles.bodyLabel}>مدل وسیله نقلیه:  </Text>
                                                        <Text style={styles.bodyValue}>{this.props.factor.dataSelect.third_car_model}</Text>
                                                    </View>
                                                    :
                                                    <View style={[styles.bodyLeft, {flexDirection: 'row'}]}>
                                                        <Text style={styles.bodyValue}>{this.props.factor.dataSelect.third_car_model}</Text>
                                                        <Text style={styles.bodyLabel}>مدل وسیله نقلیه:  </Text>
                                                    </View>
                                            }
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.third_car_years}</Text>
                                                <Text style={styles.bodyLabel}>سال ساخت: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.third_dmg_count}</Text>
                                                <Text style={styles.bodyLabel}>تخفیف عدم خسارت: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.third_dmg_years}</Text>
                                                <Text style={styles.bodyLabel}>سالهای عدم خسارت: </Text>
                                            </View>
                                        </View>
                                        : null
                                }
                                {
                                    this.props.insurType === 'body' ?
                                        <View style={styles.innerRight}>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.body_car_cate}</Text>
                                                <Text style={styles.bodyLabel}> نوع وسیله نقلیه: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.body_car_name}</Text>
                                                <Text style={styles.bodyLabel}>نام وسیله نقلیه:  </Text>
                                            </View>
                                            {
                                                this.props.factor.dataSelect.body_car_model.length > 16 ?
                                                    <View style={[styles.bodyLeft, {flexDirection: 'column'}]}>
                                                        <Text style={styles.bodyLabel}>مدل وسیله نقلیه:  </Text>
                                                        <Text style={styles.bodyValue}>{this.props.factor.dataSelect.body_car_model}</Text>
                                                    </View>
                                                    :
                                                    <View style={[styles.bodyLeft, {flexDirection: 'row'}]}>
                                                        <Text style={styles.bodyValue}>{this.props.factor.dataSelect.body_car_model}</Text>
                                                        <Text style={styles.bodyLabel}>مدل وسیله نقلیه:  </Text>
                                                    </View>
                                            }
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.factor.dataSelect.body_car_years}</Text>
                                                <Text style={styles.bodyLabel}>سال ساخت: </Text>
                                            </View>
                                        </View>
                                        : null
                                }
                            </View>
                        </View>


                        <View style={styles.labelContainer}>
                            <Text style={[styles.topLabel, {paddingBottom: 10}]}> مشخصات فردی</Text>
                            <FIcon name="user" size={20} color="gray" />
                        </View>

                        <View style={styles.body}>
                            <View style={styles.address}>
                                <View style={styles.bodyLeft}>
                                    <Text style={styles.bodyValue}>{this.props.user_details.address}</Text>
                                    <Text style={styles.bodyLabel}> نشانی درج: </Text>
                                </View>
                                <View style={styles.bodyLeft}>
                                    <Text style={styles.bodyValue}>{this.props.user_details.new_address}</Text>
                                    <Text style={styles.bodyLabel}> نشانی ارسال: </Text>
                                </View>

                            </View>
                            <View style={styles.right}>
                                <View style={styles.bodyLeft}>
                                    <Text style={styles.bodyValue}>{this.props.user_details.fname}</Text>
                                    <Text style={styles.bodyLabel}> نام: </Text>
                                </View>
                                <View style={styles.bodyLeft}>
                                    <Text style={styles.bodyValue}>{this.props.user_details.lname}</Text>
                                    <Text style={styles.bodyLabel}>نام خانوادگی: </Text>
                                </View>
                                <View style={styles.bodyLeft}>
                                    <Text style={styles.bodyValue}>{this.props.user_details.national_id}</Text>
                                    <Text style={styles.bodyLabel}>کدملی: </Text>
                                </View>
                                <View style={styles.bodyLeft}>
                                    <Text style={styles.bodyValue}>{this.props.user_details.mobile}</Text>
                                    <Text style={styles.bodyLabel}>تلفن همراه: </Text>
                                </View>
                                <View style={styles.bodyLeft}>
                                    <Text style={styles.bodyValue}>{this.props.user_details.tel}</Text>
                                    <Text style={styles.bodyLabel}>تلفن: </Text>
                                </View>
                                <View style={styles.bodyLeft}>
                                    <Text style={styles.bodyValue}>{this.props.birth}</Text>
                                    <Text style={styles.bodyLabel}>تاریخ تولد: </Text>
                                </View>
                            </View>
                        </View>
                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}
                            title={this.state.order ?  'سفارش شما ثبت شد': 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'}
                        />
                    </View>
                </ScrollView>
                <View style={styles.footer}>
                    <TouchableOpacity onPress={() => this.confirmData()} style={styles.iconLeftContainer}>
                        <FIcon name="arrow-left" size={18} color="white" style={{borderColor: 'white', borderWidth: 1, borderRadius: 20, padding: 3, marginRight: 5}} />
                        <Text style={styles.label}>بعدی</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.iconRightContainer} onPress={() => this.onBackPress()}>
                        <FIcon name="arrow-right" size={18} color="rgba(17, 103, 253, 1)" />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
export default FinalConfirm;

