import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, ImageBackground, AsyncStorage, BackHandler, TextInput} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bimegaronline.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import FooterMenu from "../../components/footerMenu/index";
// import {List, SearchBar} from "react-native-elements";
import SearchBar from 'react-native-material-design-searchbar';
import NewsItem from '../../components/newsItem';
import AlertView from '../../components/modalMassage'

class ImportantNews extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            loading: false,
            active: 1,
            modalVisible: false,
            empty: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    closeModal() {
        this.setState({modalVisible: false});
    }
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        // componentWillMount() {
        //     this.props.headerCategories.map((item , index)=> {
        //             if(item.id === ){
        //
        //             }
        //     })
        // }
    }
    startSearch(text) {
        if(text.length === 0) {
            this.setState({modalVisible: true, loading: false, empty: true});
        }
        else {
            this.setState({loading: true, search: true});
            Axios.get('/post/search/'+text).then(response=> {
                this.setState({loading: false, searchedItem: response.data.data.data, searchResponse: true});
            })
                .catch((error) => {
                    // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    // this.setState({loading: false, search: false});
                    this.setState({modalVisible: true, loading: false});

                });
        }
    }
    render() {
        // const {posts, categories, user} = this.props;
        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.container}>
                            <View style={styles.top}>
                                <View style={{width: '80%', }}>
                                    <TextInput
                                        maxLength={15}
                                        placeholder={'جستجو در بلاگ'}
                                        placeholderTextColor={'gray'}
                                        underlineColorAndroid='transparent'
                                        value={this.state.text}
                                        style={{
                                            height: 35,
                                            width: '90%',
                                            color: 'gray',
                                            fontSize: 14,
                                            textAlign: 'right',
                                            // elevation: 4,
                                            shadowColor: 'lightgray',
                                            borderRadius: 20,
                                            backgroundColor: 'white',
                                            elevation: 5,

                                            paddingRight: 10
                                        }}
                                        onChangeText={(text) => this.setState({text: text})}
                                    />
                                </View>
                                <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                                    <Icon name="bars" size={20} color="rgba(122, 130, 153, 1)" style={{paddingTop: 15}} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <View style={styles.labelContainer}>
                                <Text style={styles.topLabel}>مهم ترین اخبار</Text>
                            </View>


                            {
                                this.state.search ? this.state.searchResponse && (this.state.searchedItem.length !== 0 ?
                                        <View style={{width: '100%'}}>
                                            <View style={styles.body}>
                                                { this.state.searchedItem.map((item, index) => {
                                                    return <NewsItem item={item} key={index}/>
                                                })
                                                }
                                            </View>
                                        </View>
                                        : <Text style={{textAlign: 'center'}}>موردی برای نمایش وجود ندارد</Text>)

                                    : (
                                    <View style={{width: '100%'}}>
                                        { this.props.spacialPosts[0].attachments[0] !== undefined ?
                                            <TouchableOpacity onPress={() => Actions.blogDetail({item: this.props.spacialPosts[0]})} style={{width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                <ImageBackground source={{uri: 'https://bimegar.ir/files?uid='+this.props.spacialPosts[0].attachments[0].uid+'&width=350&height=350'}}

                                                                 style={styles.subImage}>
                                                    <View style={{backgroundColor:'rgba(0,0,0,.6)',
                                                        height: 240,
                                                        width: "100%",
                                                        paddingRight: 10,
                                                        paddingBottom: 10,
                                                        alignItems: "flex-end",
                                                        justifyContent: "flex-end",
                                                    }}>
                                                        <Text style={styles.imageText}>{this.props.spacialPosts[0].title}</Text>
                                                    </View>
                                                </ImageBackground>
                                            </TouchableOpacity>

                                            : null}
                                        <View style={styles.body}>
                                            {
                                                this.props.spacialPosts ? this.props.spacialPosts.map((item, index)=> {
                                                    if(index !== 0) {
                                                        return <NewsItem item={item} key={item.id} />
                                                    }
                                                }): null
                                            }
                                        </View>

                                    </View>

                                )
                            }
                            <AlertView
                                closeModal={(title) => this.closeModal(title)}
                                modalVisible={this.state.modalVisible}
                                // onChange={() => this.setState({modalVisible: false})}
                                title={this.state.moreAlert ? 'موارد بیشتری یافت نشد' : (this.state.empty ? 'عبارت مورد نظر نباید خالی باشد' : 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید  ')}
                            />
                        </View>
                    </ScrollView>
                    <TouchableOpacity onPress={() => this.startSearch(this.state.text)} style={{position: 'absolute', zIndex: 9992, top: '6%', left: '8%'}}>
                        <FIcon name="search" size={20} color="gray"  />
                    </TouchableOpacity>
                    <FooterMenu active="blog" openDrawer={this.props.openDrawer} />
                </View>
            );
    }
}
export default ImportantNews;
