
import React, {Component} from 'react';
import {TextInput, View, Text, TouchableOpacity, Share, Alert, AsyncStorage, ActivityIndicator} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import FIcon from 'react-native-vector-icons/dist/Feather';
import EIcon from 'react-native-vector-icons/dist/Entypo';
import IIcon from 'react-native-vector-icons/dist/Ionicons';
import MIcon from 'react-native-vector-icons/dist/MaterialIcons';
import MCIcon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import {Actions} from 'react-native-router-flux'
import Axios from 'axios';
export const url = 'http://bimegaronline.com/api/v1';
 Axios.defaults.baseURL = url;
import Loader from '../loader'
import AlertView from '../../components/modalMassage'
import {store} from '../../config/store';

class SidebarItem extends Component {

    constructor(props){
        super(props);
        this.state = {
            text: 'علی',
            checked: false,
            show: false,
            icon: null,
            modalVisible: false,
            register: false,
        };
    }
    componentWillMount() {
        let icon = null;
        if(this.props.icon === "home") {
            icon = <EIcon color="rgba(255, 193, 39, 1)" size={20} name="home"/>
            this.setState({icon: icon})
        }
        else  if(this.props.icon === "shield") {
            icon = <MCIcon color="rgba(255, 193, 39, 1)" size={20} name="shield" />
            this.setState({icon: icon})
        }
        else  if(this.props.icon === "md-wallet") {
            icon = <IIcon color="rgba(255, 193, 39, 1)" size={20} name="md-wallet" />
            this.setState({icon: icon})
        }
        else  if(this.props.icon === "receipt") {
            icon = <MIcon color="rgba(255, 193, 39, 1)" size={20} name="receipt" />
            this.setState({icon: icon})
        }
        else  if(this.props.icon === "event-note") {
            icon = <MIcon color="rgba(255, 193, 39, 1)" size={20} name="event-note" />
            this.setState({icon: icon})
        }
        else  if(this.props.icon === "users") {
            icon = <Icon color="rgba(255, 193, 39, 1)" size={20} name="users" />
            this.setState({icon: icon})
        }
        else  if(this.props.icon === "question-circle") {
            icon = <Icon color="rgba(255, 193, 39, 1)" size={20} name="question-circle" />
            this.setState({icon: icon})
        }
        else  if(this.props.icon === "balance-scale") {
            icon = <Icon color="rgba(255, 193, 39, 1)" size={20} name="balance-scale" />
            this.setState({icon: icon})
        }
        else  if(this.props.icon === "md-settings") {
            icon = <IIcon color="rgba(255, 193, 39, 1)" size={20} name="md-settings" />
            this.setState({icon: icon})
        }
        else  if(this.props.icon === "logout") {
            icon = <MCIcon color="rgba(255, 193, 39, 1)" size={20} name="logout" />
            this.setState({icon: icon})
        }
        else  if(this.props.icon === "wallet") {
            icon = <EIcon color="rgba(255, 193, 39, 1)" size={20} name="wallet" />
            this.setState({icon: icon})
        }
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    testRoot(item) {
        switch (item) {
            case 'wallet':
                Actions.wallet({openDrawer: this.props.openDrawer})
                Actions.drawerClose()
                // this.props.closeDrawer();
                break;
            case 'userOrders':
                Actions.userOrders({openDrawer: this.props.openDrawer, modalOpen: false})
                Actions.drawerClose()
                // this.props.closeDrawer();
                break;
            case 'reminders':
                Actions.reminders({openDrawer: this.props.openDrawer})
                Actions.drawerClose()
                // this.props.closeDrawer();
                break;
            case 'introduce':
                Actions.refferer({openDrawer: this.props.openDrawer});
                Actions.drawerClose()
                // this.props.closeDrawer();
                break;
            case 'shield':
                Actions.insuranceBuy({openDrawer: this.props.openDrawer});
                Actions.drawerClose()
                // this.props.closeDrawer();
                break;
        }
    }
    rootHandle() {
        if(this.props.icon === "home") {
            Actions.insuranceBuy({openDrawer: this.props.openDrawer})
            Actions.drawerClose()
            // this.props.closeDrawer();
        }
        else  if(this.props.icon === "shield") {
            this.testRoot('shield')
            // Actions.wallet({openDrawer: this.props.openDrawer})
        }
        else  if(this.props.icon === "md-wallet") {
            this.testRoot('wallet')
            // Actions.wallet({openDrawer: this.props.openDrawer})
            // // this.props.closeDrawer();
        }
        else  if(this.props.icon === "receipt") {
            this.testRoot('userOrders')
            // Actions.userOrders({openDrawer: this.props.openDrawer})
            // // this.props.closeDrawer();
        }
        else  if(this.props.icon === "event-note") {
            this.testRoot('reminders')

            // Actions.reminders({openDrawer: this.props.openDrawer})
            // // this.props.closeDrawer();
        }
        else  if(this.props.icon === "users") {
            this.testRoot('introduce')
            // Actions.wallet({openDrawer: this.props.openDrawer});
            // // this.props.closeDrawer();
            // Share.share(
            //     {
            //         title: "برای اشتراک با دوستان روی لینک زیر کلیک کنید",
            //         message: "http://dibit.ir/API",
            //     },
            // )
        }
        else  if(this.props.icon === "question-circle") {
            Actions.about({openDrawer: this.props.openDrawer})
            Actions.drawerClose()
            // this.props.closeDrawer();
        }
        else  if(this.props.icon === "balance-scale") {
            Actions.rules({openDrawer: this.props.openDrawer})
            Actions.drawerClose()
            // this.props.closeDrawer();
        }
        else  if(this.props.icon === "md-settings") {
            Actions.configure({openDrawer: this.props.openDrawer})
            Actions.drawerClose()
            // this.props.closeDrawer();
        }
        else  if(this.props.icon === "logout") {
            AsyncStorage.removeItem('token');
            Alert.alert(
                'خروج از حساب کاربری',
                'آیا از  خروج خود مطمئن هستید؟',
                [
                    {text: 'خیر', onPress: () => this.setState({logout: false}), style: 'cancel'},
                    {text: 'بله', onPress: () =>  this.setState({logout: true}, () => {Actions.reset('insuranceBuy');  AsyncStorage.removeItem('loged');  AsyncStorage.removeItem('showMore'); this.props.userLogOut();  Actions.drawerClose()})},
                    // {text: 'بله', onPress: () =>  this.setState({logout: true}, () => {Actions.login(); // this.props.closeDrawer();})},
                ]
            );

        }
        else if(this.props.icon === "wallet") {
            Actions.marketing({openDrawer: this.props.openDrawer})
            Actions.drawerClose()
            // this.props.closeDrawer();
        }
    }
    render() {
         return (
            <TouchableOpacity onPress={() => this.rootHandle()} style={styles.container}>
                {
                    this.state.loading ?
                        <ActivityIndicator
                            size={20}
                            // animating={props.animating}
                            color={'white'}
                        /> : null
                }

                <Text style={styles.label}>{this.props.label}</Text>
                {this.state.icon}
                <AlertView
                    closeModal={(title) => this.closeModal(title)}
                    modalVisible={this.state.modalVisible}
                    title={this.state.register ? 'برای مشاهده این بخش ابتدا ثبت نام کنید ': 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'}
                />
            </TouchableOpacity>
        );
    }
}
export default SidebarItem;
