import React, { Component } from 'react';
import {
    Modal,
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    TextInput,
    Alert,
    ActivityIndicator
}
    from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import Axios from 'axios';
export const url = 'http://bimegaronline.com/api/v1';
Axios.defaults.baseURL = url;
import AppSearchBar from '../../components/searchBar'
import VacleType from '../../components/vacleType'

class AlertView extends Component {
    state = {
        modalVisible: false,
        value: '',
        lineAmount: 1,
        text: ''
    };
    render() {
        return (
            <View style = {styles.container}>
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.props.modalVisible}
                       onRequestClose = {() => this.props.onChange(false)}
                       onBackdropPress= {() => this.props.onChange(!this.state.modalVisible)}
                >
                    <View style={styles.modal}>
                        <View style={[styles.box, { paddingTop: 10,paddingBottom: 10, padding: 10}]}>
                            {/*<ActivityIndicator*/}
                                {/*size={25}*/}
                                {/*// animating={props.animating}*/}
                                {/*color={'gray'}*/}
                            {/*/>*/}
                            <Text style={styles.text}>{this.props.title}</Text>
                            <View style={{alignItems: 'center', justifyContent: 'center', paddingTop: 30}}>
                                <TouchableOpacity onPress={() => this.props.closeModal()} style={styles.advertise}>
                                    <Text style={styles.buttonTitle}>بستن</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}
export default AlertView

const styles = StyleSheet.create ({
    container: {
        alignItems: 'center',
        backgroundColor: 'white',
        padding: 100,
        position: 'absolute',
        bottom: -200,
        zIndex: -2
    },
    modal: {
        flexGrow: 1,
        justifyContent: 'center',
        // flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,.5)',
        paddingRight: 15,
        paddingLeft: 15,
        zIndex: -2

    },
    text: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingTop: 15,
        textAlign: 'right'
    },
    box: {
        width: '90%',
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        borderRadius: 10,
        padding: 10,
        paddingTop: 20,
        paddingBottom: 20,

    },
    buttonText: {
        fontSize: 15,
        color: 'white',
        fontWeight: 'bold',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    vacleContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    advertise: {
        width: '60%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15,
        backgroundColor: 'rgba(255, 193, 39, 1)',
        padding: 7,
        marginBottom: 20
    },
    buttonTitle: {
        fontSize: 15,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingRight: 20,
        paddingLeft: 20,
    },
});