
import React, {Component} from 'react';
import {TextInput, View, Text, TouchableOpacity} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { CheckBox } from 'react-native-elements'

class TimeItem extends Component {

    constructor(props){
        super(props);
        this.state = {
            text: 'علی',
            checked: null,
            show: false
        };
    }
    render() {
        // console.log('time id', this.props.id)
        // console.log('time active id',  this.props.activeId)
        const hours = new Date().getHours();
        let hourinfo = '';
        hourinfo = (hours >= 12) ? '16:59:59' : '11:59:59';

        const item1 = this.props.partDate[this.props.id]+ ' ' + hourinfo;
        const item2 = this.props.partDate[this.props.id]+ ' ' + hourinfo;
        const item3 = this.props.partDate[this.props.id]+ ' ' + hourinfo;

        // console.log('!this.state.part2', !this.state.part2)
        return (
            <TouchableOpacity onPress={() => this.props.part2 ? this.props.check(item3, this.props.id ) : this.setState({show: !this.state.show})} style={[styles.container, {backgroundColor: this.props.id === this.props.activeId  ? 'rgba(116, 220, 163, 1)' : 'white', height: !this.state.show? 70: undefined}]}>
                <Text style={styles.label}>{this.props.item}</Text>
                {
                    this.state.show && !this.props.part2 ?
                        <View style={styles.allTime}>
                            <TouchableOpacity onPress={() =>  this.setState({checked: 1}, () => {this.props.check(item1, this.props.id )})} style={styles.timeContainer}>
                                <Text style={[styles.label, {position: 'absolute', left : 34}]}>ساعت 9 تا 12</Text>
                                <CheckBox
                                    iconRight
                                    containerStyle={{backgroundColor: 'transparent', borderWidth: 0, padding: 0}}
                                    // center
                                    // title='Click Here'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={ this.props.id === this.props.activeId && this.props.activeId !== null && this.state.checked === 1}
                                    onPress={() => this.setState({checked: 1}, () => {this.props.check(item1, this.props.id)})} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() =>  this.setState({checked: 2}, () => {this.props.check(item2, this.props.id)})} style={styles.timeContainer}>
                                <Text style={[styles.label, {position: 'absolute', left : 30}]}>ساعت 12 تا 17</Text>
                                <CheckBox
                                    iconRight
                                    containerStyle={{backgroundColor: 'transparent', borderWidth: 0, padding: 0}}
                                    // center
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={ this.props.id === this.props.activeId && this.props.activeId !== null && this.state.checked === 2}
                                    onPress={() =>  {this.setState({ checked: 2}); this.props.check(item2, this.props.id)}} />
                            </TouchableOpacity>
                        </View>
                        : null
                }
            </TouchableOpacity>
        );
    }
}
export default TimeItem;