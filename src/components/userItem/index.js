import React, {Component} from 'react';
import {TextInput, View, Text, TouchableOpacity} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { CheckBox } from 'react-native-elements'
import ModalFilterPicker from 'react-native-modal-filter-picker'
import Axios from 'axios';
export const url = 'http://bimegaronline.com/api/v1';
Axios.defaults.baseURL = url;

class UserItem extends Component {

    constructor(props){
        super(props);
        this.state = {
            text: '',
            provinces: [],
            province: 0,
            loading: true,
            checked: this.props.status === 'female' ? 2 : (this.props.status === 'male' ? 1 : 0),
            visibleinput2: false
        };
    }
    handleChange(text){
        if(this.props.label !== 'شماره موبایل') {
            this.setState({text: text});
            this.props.check(text);
        }

    }
    componentWillMount(){
        Axios.get('/request/get_insurance').then(response => {
            this.setState({insurances: response.data.data});
            Axios.post('/request/get-province', {}).then(response => {
                console.log('provinces', response.data.data)
                this.setState({provinces: response.data.data, loading: false});
            })
                .catch((error) => {
                    // Alert.alert('','خطا')
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});
                    console.log(error);
                });
        })
            .catch((error) => {
                // Alert.alert('','خطا')
                // this.setState({loading: false});
                this.setState({modalVisible: true, loading: false});
                console.log(error);
            });

    }
    focuse() {
        if(this.props.birthday) {
            this.props.openPicker();
        }
    }
    onShowinput2 = () => {
        this.setState({ visibleinput2: true });
    }

    onSelectInput2 = (picked) => {
        console.log('picked', picked)
        this.setState({
            province: picked,
            visibleinput2: false
        }, () =>{
            console.log('ppppicked', picked)
            this.state.provinces.map((item) => {
                if(picked === item.id) {
                    this.setState({text: item.name}, () =>  this.handleChange(item.name));
                }
            })
        })
    }
    onCancelinput2 = () => {
        this.setState({
            visibleinput2: false
        })
    }
    render() {
        console.log('checcccccck', this.props.status === 'female' ? 2 : (this.props.status === 'male' ? 1 : 0))
        return (
            <View style={[styles.container, {borderBottomColor: this.props.border ? 'rgb(237, 237, 237)' : 'transparent', borderBottomWidth: this.props.border ? 1 : 0,borderRadius: 10}]}>
                <View style={styles.left}>
                    {/*<Icon name="chevron-left" size={12} color="rgb(180, 180, 180)" style={{paddingRight: 30}} />*/}
                    {
                        this.props.sex ?
                            <View style={styles.allTime}>
                                <TouchableOpacity onPress={() =>  {this.setState({ checked: 1}, ()=> {this.props.check('male')})}} style={styles.timeContainer}>
                                    <Text style={styles.label}>مرد</Text>
                                    <CheckBox
                                        containerStyle={{backgroundColor: 'transparent', borderWidth: 0, padding: 0}}
                                        // center
                                        // title='Click Here'
                                        checkedIcon='dot-circle-o'
                                        uncheckedIcon='circle-o'
                                        checked={this.state.checked === 1}
                                        onPress={() =>  {this.setState({ checked: 1}, ()=> {this.props.check('male')})}} />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() =>  {this.setState({ checked: 2}, ()=> {this.props.check('female')})}}  style={styles.timeContainer}>
                                    <Text style={styles.label}>زن</Text>
                                    <CheckBox
                                        containerStyle={{backgroundColor: 'transparent', borderWidth: 0, padding: 0}}
                                        // center
                                        // title='Click Here'
                                        checkedIcon='dot-circle-o'
                                        uncheckedIcon='circle-o'
                                        checked={this.state.checked === 2}
                                        onPress={() =>  {this.setState({ checked: 2}, ()=> {this.props.check('female')})}} />
                                </TouchableOpacity>
                            </View> :
                            this.props.province ?
                                <TouchableOpacity onPress={this.onShowinput2} style={{position: 'relative', zIndex: 3, width: 180, backgroundColor: 'white', height: 32, borderColor: this.state.redBorder && this.state.province === 0 ? 'red' : 'transparent', borderWidth: 1, borderRadius: 10}}>
                                    {/*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                                    <Text style={{paddingTop: 5, paddingRight: 10,  fontFamily: 'IRANSansMobile(FaNum)', color: 'gray' ,fontSize: 12 }}>{this.state.loading ? "در حال بارگذاری..." : this.state.province === 0 ? "انتخاب نشده" : this.state.text}</Text>
                                    <ModalFilterPicker
                                        visible={this.state.visibleinput2}
                                        onSelect={this.onSelectInput2}
                                        onCancel={this.onCancelinput2}
                                        options={  this.state.provinces.map((item) => {return {key: item.id, label: item.name, value: item.id}})}
                                        placeholderText="جستجو ..."
                                        cancelButtonText="لغو"
                                        filterTextInputStyle={{textAlign: 'right', fontFamily: 'IRANSansMobile(FaNum)'}}
                                        optionTextStyle={{textAlign: 'right', width: 180, fontFamily: 'IRANSansMobile(FaNum)'}}
                                        titleTextStyle={{textAlign: 'right', fontFamily: 'IRANSansMobile(FaNum)'}}
                                    />
                                </TouchableOpacity>
                                :
                                <TextInput
                                    keyboardType={(this.props.label === 'کد ملی' || this.props.label === 'کد پستی') ? 'numeric' : undefined}
                                    onFocus={() => this.focuse()}
                                    placeholder={this.props.value}
                                    placeholderTextColor={'gray'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.text}
                                    style={{
                                        height: 40,
                                        backgroundColor: 'white',
                                        paddingRight: 15,
                                        width: 180,
                                        color: 'gray',
                                        fontSize: 14,
                                        textAlign: 'right',
                                    }}
                                    // onChangeText={(text) => this.setState({text: text, fill: ++this.state.fill})}
                                    onChangeText={(text) => this.handleChange(text)}
                                />
                    }

                </View>
                <Text style={styles.label}>{this.props.label}</Text>
            </View>
        );
    }
}
export default UserItem;