import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import FFIcon from 'react-native-vector-icons/dist/FontAwesome';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import pic2 from '../../assets/insurances/2.png'
import pic3 from '../../assets/insurances/3.png'
import pic4 from '../../assets/insurances/4.png'
import pic5 from '../../assets/insurances/5.png'
import pic6 from '../../assets/insurances/6.png'
import pic7 from '../../assets/insurances/7.png'
import pic8 from '../../assets/insurances/8.png'
import pic9 from '../../assets/insurances/9.png'
import pic10 from '../../assets/insurances/10.png'
import pic11 from '../../assets/insurances/11.png'
import pic12 from '../../assets/insurances/12.png'
import pic13 from '../../assets/insurances/13.png'
import pic14 from '../../assets/insurances/14.png'
import pic15 from '../../assets/insurances/15.png'
import pic18 from '../../assets/insurances/18.png'
import pic19 from '../../assets/insurances/19.png'
import pic20 from '../../assets/insurances/20.png'
import pic21 from '../../assets/insurances/21.png'
import pic22 from '../../assets/insurances/22.png'
import pic23 from '../../assets/insurances/23.png'
import payment from '../../assets/payment.png'
import instalment1 from '../../assets/instalment1.png'
import instalment2 from '../../assets/instalment2.png'
import AlertView from '../../components/modalMassage'

class InsuranceInfo extends Component {
    constructor(props){
        super(props);
        this.state = {
            open: false,
            buy: false,
            activeBuy: null,
            instalment: false,
            instalmentBuy: false,
            modalVisible: false,
            describe: false,
            stateIns: 'one'
        };
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    render() {
        console.log('this.state.active buy', this.state.activeBuy)
        console.log('this.props.nnnn.active buy', this.props.buyId)
        const newValue =  Math.ceil(this.props.item.price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        const thirdBasePrice = this.props.insurType === 'Third' ? Math.ceil(this.props.item.base_third_party).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : null;
        const id = this.props.item.insurance.id;
        let imgSrc = null;
        switch (id) {
            case 2:
                imgSrc = pic2;
                break;
            case 3:
                imgSrc = pic3;
                break;
            case 4:
                imgSrc = pic4;
                break;
            case 5:
                imgSrc = pic5;
                break;
            case 6:
                imgSrc = pic6;
                break;
            case 7:
                imgSrc = pic7;
                break;
            case 8:
                imgSrc = pic8;
                break;
            case 9:
                imgSrc = pic9;
                break;
            case 10:
                imgSrc = pic10;
                break;
            case 11:
                imgSrc = pic11;
                break;
            case 12:
                imgSrc = pic12;
                break;
            case 13:
                imgSrc = pic13;
                break;
            case 14:
                imgSrc = pic14;
                break;
            case 15:
                imgSrc = pic5;
                break;
            case 16:
                imgSrc = pic22;
                break;
            case 18:
                imgSrc = pic18;
                break;
            case 19:
                imgSrc = pic19;
                break;
            case 20:
                imgSrc = pic20;
                break;
            case 21:
                imgSrc = pic21;
                break;
            case 22:
                imgSrc = pic22;
                break;
            case 23:
                imgSrc = pic23;
                break;
        }
        let instalmentOne = null, instalmentTwo = null, instalmentTree = null;
        console.log('this.props.item.', this.props.item)
        if(this.props.item.installments === 1) {
            if(this.state.stateIns === 'one') {
                instalmentOne = (100 - this.props.item.installment_percent)/100;
            }
            else if(this.state.stateIns === 'two') {
                instalmentOne = (100 - this.props.item.installment_percent)/200;
                instalmentTwo = (100 - this.props.item.installment_percent)/200;

            }
            else if(this.state.stateIns === 'tree') {
                instalmentOne = (100 - this.props.item.installment_percent)/300;
                instalmentTwo = (100 - this.props.item.installment_percent)/300;
                instalmentTree = (100 - this.props.item.installment_percent)/300;
            }
        }

        return (
            <View style={styles.topContainer}  onPress={() => null}>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View>
                            {
                                this.state.activeBuy ===  this.props.buyId && this.props.buyId !== null ?
                                    // this.state.buy?
                                    <View style={styles.iconRightEditContainer}>
                                        <FIcon name="check" size={18} color="white" />
                                    </View> :
                                    <TouchableOpacity onPress={() => {this.props.handleNext(this.state.stateIns,Math.ceil((this.props.item.price*this.props.item.installment_percent/100)/100)*100, Math.ceil((this.props.item.price*instalmentOne)/1000)*1000, Math.ceil((this.props.item.price*instalmentTwo)/1000)*1000, Math.ceil((this.props.item.price*instalmentTree)/1000)*1000, this.props.item.insurance.title, Math.ceil(this.props.item.price), this.props.item.insurance.id, false, this.props.item.insurance.id);  this.setState({buy: true, activeBuy: this.props.item.insurance.id})}} style={styles.iconRightContainer}>
                                        <FIcon name="shopping-cart" size={16} color="white" style={{borderColor: 'white', borderWidth: 1, borderRadius: 20, padding: 5, marginRight: 5, alignItems: 'center', justifyContent: 'center'}} />
                                        <Text style={styles.label}>خرید </Text>
                                    </TouchableOpacity>
                            }
                            {
                                (this.props.item.installments &&  this.props.item.installments === 1) ?
                                    <TouchableOpacity onPress={() => {this.setState({instalment: !this.state.instalment})}} style={[styles.iconRightContainer, {backgroundColor: 'rgb(255, 192, 51)'}]}>
                                        <FFIcon name="money" size={18} color="white" style={{borderColor: 'white', borderWidth: 1, borderRadius: 20, padding: 3, marginRight: 5}} />
                                        <Text style={styles.label}>اقساط </Text>
                                    </TouchableOpacity>
                                    : null
                            }
                        </View>
                        {/*<View style={styles.right}>*/}
                        {/*</View>*/}
                        <View style={styles.left}>
                            <View style={styles.info}>
                                <Text style={styles.labelInfo}>{this.props.item.insurance.title}</Text>
                                <View style={styles.price}>
                                    <Text style={styles.priceLabel}>تومان</Text>
                                    <Text style={styles.amount}>{newValue}</Text>
                                </View>
                            </View>
                            <View style={{ alignItems: 'flex-end', justifyContent: 'flex-end'}}>
                                <Image source={imgSrc} style={styles.Image} />
                                <TouchableOpacity onPress={() => this.setState({modalVisible: true, describe: true})} style={styles.describeContainer}>
                                    <Text style={[styles.label, {fontSize: 10}]}>توضیحات</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    {
                        this.state.instalment?
                            <View style={styles.body}>
                                <Text style={styles.instalmentLabel}> شرایط قسط : </Text>
                                <Text style={styles.instalmentValue}> در حال حاضر خرید قسطی تنها در شهر تهران امکان‌پذیر است.
                                    فرایند صدور پس از واریز پیش پرداخت از درگاه بیمه بانی، آغاز خواهد شد.
                                    برای سایر اقساط، چک‌ها با "آرم صیاد"، با فاصله یک ماه در وجه شرکت بیمه بانی ، هنگام ارسال
                                    بیمه‌نامه از شما دریافت می‌شود.
                                </Text>
                                <View style={styles.buttonContainer}>
                                    <TouchableOpacity onPress={() => {this.setState({stateIns: 'tree'})}} style={[styles.btnContainer, {backgroundColor: this.state.stateIns === 'tree' ? 'rgba(51, 197, 117, 1)' : 'rgba(101, 247, 167, 1)' }]}>
                                        <Text style={styles.label2}>اقساط سه ماهه</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => {this.setState({stateIns: 'two'})}} style={[styles.btnContainer, {backgroundColor: this.state.stateIns === 'two' ? 'rgba(51, 197, 117, 1)' : 'rgba(101, 247, 167, 1)' }]}>
                                        <Text style={styles.label2}>اقساط دو ماهه</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => {this.setState({stateIns: 'one'})}} style={[styles.btnContainer, {backgroundColor: this.state.stateIns === 'one' ? 'rgba(51, 197, 117, 1)' : 'rgba(101, 247, 167, 1)' }]}>
                                        <Text style={styles.label2}>اقساط یک ماهه</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.row} >
                                    <Text style={styles.instalmentLabel}>پیش پرداخت: {Math.ceil((this.props.item.price*this.props.item.installment_percent/100)/100)*100} تومان</Text>
                                    <Image source={payment} style={styles.image} />
                                </View>
                                <View style={styles.row} >
                                    <Text style={styles.instalmentLabel}>قسط اول: {Math.ceil((this.props.item.price*instalmentOne)/1000)*1000} تومان</Text>
                                    <Image source={instalment1} style={styles.image} />
                                </View>
                                {
                                    this.state.stateIns !== 'one' ?
                                        <View style={styles.row} >
                                            <Text style={styles.instalmentLabel}>قسط دوم: {Math.ceil((this.props.item.price*instalmentTwo)/1000)*1000} تومان</Text>
                                            <Image source={instalment2} style={styles.image} />
                                        </View> : null
                                }
                                {
                                    this.state.stateIns === 'tree' ?
                                        <View style={styles.row} >
                                            <Text style={styles.instalmentLabel}>قسط سوم: {Math.ceil((this.props.item.price*instalmentTree)/1000)*1000} تومان</Text>
                                            <Image source={instalment2} style={styles.image} />
                                        </View>
                                        : null
                                }
                                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                                    <TouchableOpacity onPress={() => {this.setState({instalmentBuy: true}, () => {this.props.handleNext(this.state.stateIns, Math.ceil((this.props.item.price*this.props.item.installment_percent/100)/100)*100, Math.ceil((this.props.item.price*instalmentOne)/1000)*1000, Math.ceil((this.props.item.price*instalmentTwo)/1000)*1000, Math.ceil((this.props.item.price*instalmentTree)/1000)*1000, this.props.item.insurance.title, Math.ceil(this.props.item.price), this.props.item.insurance.id, this.state.instalmentBuy)})}} style={styles.btnContainer}>
                                        <Text style={styles.label}>خرید اقساط</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            : null
                    }
                    {
                        this.state.open ?
                            <View style={styles.body}>
                                <View style={[styles.regimContainer,{justifyContent: this.props.insurType === 'Third' ? 'space-between' : 'flex-end'} ]}>
                                    <View style={styles.bodyLeft}>
                                        <Text style={styles.bodyValue}>{this.props.item.insurance.shop_share} درصد</Text>
                                        <Text style={styles.bodyLabel}> سهم از بازار : </Text>
                                    </View>
                                    {
                                        this.props.insurType === 'Third'?
                                            <View style={[styles.bodyLeft, {alignSelf: 'flex-end'}]}>
                                                <Text style={styles.bodyValue}>{thirdBasePrice} تومان</Text>
                                                <Text style={styles.bodyLabel}>مبلغ پایه بیمه:  </Text>
                                            </View>
                                            : null
                                    }
                                </View>
                                <View style={styles.regimContainer}>
                                    <View style={styles.bodyLeft}>
                                        <Text style={styles.bodyValue}>ندارد</Text>
                                        <Text style={styles.bodyLabel}>پرداخت خسارت سیار: </Text>
                                    </View>
                                    <View style={styles.bodyLeft}>
                                        <Text style={styles.bodyValue}>{this.props.item.insurance.off} تومان</Text>
                                        <Text style={styles.bodyLabel}>مبلغ تخفیف بیمه: </Text>
                                    </View>
                                </View>
                                <View style={styles.regimContainer}>
                                    <View style={styles.bodyLeft}>
                                        <Text style={styles.bodyValue}>{this.props.item.insurance.branches}</Text>
                                        <Text style={styles.bodyLabel}>تعداد مراکز پرداخت خسارت: </Text>
                                    </View>
                                    <View style={styles.bodyLeft}>
                                        <Text style={styles.bodyValue}>{this.props.item.insurance.customer_rate}</Text>
                                        <Text style={styles.bodyLabel}>رضایت مشتری(رتبه): </Text>
                                    </View>
                                </View>
                                <View style={styles.regimContainer}>
                                    <View style={styles.bodyLeft}>
                                        <View style={styles.startContainer}>
                                            {
                                                [...Array(parseInt( this.props.item.insurance.summary_rank)).keys()].map((item, index)=> <Icon key={index} name="star" size={10} color="rgba(255, 193, 39, 1)" />
                                                )
                                            }
                                        </View>
                                        <Text style={styles.bodyLabel}>سطوح توانگر مالی: </Text>
                                    </View>
                                    <View style={styles.bodyLeft}>
                                        <Text style={styles.bodyValue}>{this.props.item.insurance.delay}</Text>
                                        <Text style={styles.bodyLabel}>مدت پاسخگویی: </Text>
                                    </View>
                                </View>
                            </View>
                            : null
                    }
                    <View style={styles.footer}>
                        {
                            this.state.open ?
                                <TouchableOpacity onPress={() => this.setState({open: false})}>
                                    <Text style={styles.bodyLabel}>بستن اطلاعات بیشتر</Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => this.setState({open: true})}>
                                    <Text style={styles.bodyLabel}>اطلاعات بیشتر</Text>
                                </TouchableOpacity>
                        }
                    </View>
                </View>
                <AlertView
                    closeModal={(title) => this.closeModal(title)}
                    modalVisible={this.state.modalVisible}
                    long={this.props.item.modal_desc !== null}
                    title={this.state.describe ? this.props.item.modal_desc ? this.props.item.modal_desc : 'توضیحاتی برای نمایش وجود ندارد' : 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'}
                />
            </View>
        );
    }
}
export default InsuranceInfo;