import React, {Component} from 'react';
import {Text, View, ScrollView, Image, TouchableOpacity} from 'react-native';
import styles from './styles'
import 'moment/locale/fa';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {Actions} from 'react-native-router-flux'
import moment_jalaali from 'moment-jalaali'

class SlideItems extends Component {
    // componentWillMount() {
    //     this.props.headerCategories.map((item , index)=> {
    //             if(item.id === ){
    //
    //             }
    //     })
    // }
    render() {
        // console.log(' category title', this.props.item.category.title)
        return (
            <TouchableOpacity style={styles.container} onPress={() => Actions.blogDetail({item: this.props.item})}>
                {
                    this.props.item.attachments[0] !== undefined ?
                        <Image source={{uri: 'https://bimegar.azarinpro.info/files?uid='+this.props.item.attachments[0].uid+'&width=350&height=350'}} style={styles.image} />
                        : null
                }
                <View style={styles.content}>
                    <Text style={styles.contentText}>{this.props.item.title} </Text>
                    <View style={styles.advRow}>
                        <View style={styles.iconContainer}>
                            {/*<Icon name="heart" size={20} color="red" style={{paddingLeft: 10}} />*/}
                            {/*<Icon name="bookmark-o" size={20} color="black" style={{paddingLeft: 25}} />*/}
                        </View>
                        <View style={styles.labelContainer}>
                            <Text style={styles.bodyText}>{this.props.item.category.title}</Text>
                            <Text style={styles.footerText}>{moment_jalaali(this.props.item.created_at).format('jYYYY/jM/jD')}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}
export default SlideItems;