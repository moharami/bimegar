import React, {Component} from 'react';
import {View, Text, Picker, Image, TextInput, TouchableOpacity, AsyncStorage, ActivityIndicator} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import MIcon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import styles from './styles'
import Axios from 'axios';
export const url = 'http://bimegaronline.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import {Actions} from 'react-native-router-flux'
import moment_jalaali from 'moment-jalaali'
import moment from 'moment'
import PersianCalendarPicker from '../../components/persian_calender/src/index';
import AlertView from '../../components/modalMassage'
import AlertModal from '../../components/alertModal'

class Remind extends Component {
    constructor(props){
        super(props);
        this.state = {
            fname: '',
            lname: '',
            mobile: '',
            month: 'مهر',
            day: 1,
            dayy: 1,
            edit: false,
            showPicker: false,
            selectedStartDate: null,
            loading: false,
            modalVisible: false,
            dateR: false,
            updateR: false,
            deleteR: false
        };
    }
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false})}, 100);
        this.setState({selectedStartDate: date });
    }
    closeModal() {
        this.setState({modalVisible: false});
        if(this.state.deleteR){
            Actions.reset('reminders', {openDrawer: this.props.openDrawer});
        }
    }
    onChange(){
        this.setState({showPicker: false});
    }
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false})}, 200)
        this.setState({ selectedStartDate: date });
    }
    closeModal2() {
        this.setState({showPicker: false});
    }
    updateReminder() {
        if(this.state.selectedStartDate !== null) {
            this.setState({loading: true, updateR: false, dateR: false});
            AsyncStorage.getItem('token').then((info) => {
                const newInfo = JSON.parse(info);
                Axios.post('/request/remember/update', {
                    user_id: newInfo.user_id,
                    fname: this.state.fname !== '' ? this.state.fname : this.props.item.fname,
                    lname: this.state.lname !== '' ? this.state.lname : this.props.item.lname,
                    mobile: this.state.mobile !== '' ? this.state.mobile : this.props.item.mobile,
                    expired_date: this.state.selectedStartDate ? this.state.selectedStartDate : this.props.item.expired_date,
                    id: this.props.item.id
                }).then(response => {
                    // Alert.alert('', 'یادآوری با موفقیت به روزرسانی شد');
                    this.setState({modalVisible: true, updateR: true, loading: false});

                })
                    .catch((error) => {
                        // Alert.alert('', 'خطایی رخ داده مجددا تلاش نمایید');
                        // this.setState({loading: false});
                        this.setState({modalVisible: true, loading: false});

                    });

            });
        }
        else {
            // Alert.alert('','لطفا تاریخ انقضا را وارد نمایید');
            this.setState({modalVisible: true, dateR: true, loading: false});
        }
    }
    deleteReminder() {
        this.setState({loading: true});
        Axios.post('/request/remember/delete', {
            id: this.props.item.id
        }).then(response => {
            // Alert.alert('','یادآوری با موفقیت حذف شد');
            this.setState({modalVisible: true, deleteR: true, loading: false});
        })
            .catch((error) =>{
                // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                // this.setState({loading: false});
                this.setState({modalVisible: true, loading: false});
            });
    }
    render() {
        const d = this.props.item.expired_date;
        console.log('datae jari', d);
        const months = ['مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند', 'فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور'];
        // if(this.state.loading){
        //     return (<Loader />)
        // }
        // else
        return (
            <View style={styles.topContainer}  onPress={() => null}>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <Text style={styles.headerLabel}>بیمه شخص ثالث</Text>
                    </View>
                    {
                        this.state.edit?
                            <View>
                                <View style={[styles.textContainer, {borderBottomColor:  'rgb(237, 237, 237)', borderBottomWidth:  1 ,borderRadius: 10}]}>
                                    <View style={styles.textLeft}>
                                        {/*<Icon name="chevron-left" size={12} color="rgb(180, 180, 180)" style={{paddingRight: 30}} />*/}
                                        <TextInput
                                            placeholder={this.props.item.fname}
                                            placeholderTextColor={'gray'}
                                            underlineColorAndroid='transparent'
                                            value={this.state.fname}
                                            maxLength={12}
                                            style={{
                                                height: 40,
                                                backgroundColor: 'white',
                                                paddingRight: 15,
                                                width: 180,
                                                color: 'gray',
                                                fontSize: 14,
                                                textAlign: 'right'
                                            }}
                                            onChangeText={(text) => this.setState({fname: text})}
                                            // onChangeText={(text) => this.handleChange(text)}
                                        />
                                    </View>
                                    <Text style={styles.label}>نام </Text>
                                </View>
                                <View>
                                    <View style={[styles.textContainer, {borderBottomColor:  'rgb(237, 237, 237)', borderBottomWidth:  1 ,borderRadius: 10}]}>
                                        <View style={styles.textLeft}>
                                            {/*<Icon name="chevron-left" size={12} color="rgb(180, 180, 180)" style={{paddingRight: 30}} />*/}
                                            <TextInput
                                                placeholder={this.props.item.lname}
                                                placeholderTextColor={'gray'}
                                                underlineColorAndroid='transparent'
                                                value={this.state.lname}
                                                maxLength={12}
                                                style={{
                                                    height: 40,
                                                    backgroundColor: 'white',
                                                    paddingRight: 15,
                                                    width: 180,
                                                    color: 'gray',
                                                    fontSize: 14,
                                                    textAlign: 'right',
                                                }}
                                                onChangeText={(text) => this.setState({lname: text})}
                                            />
                                        </View>
                                        <Text style={styles.label}>نام خانوادگی</Text>
                                    </View>
                                    <View style={[styles.textContainer, {borderBottomColor:  'rgb(237, 237, 237)', borderBottomWidth:  1 ,borderRadius: 10}]}>
                                        <View style={styles.textLeft}>
                                            {/*<Icon name="chevron-left" size={12} color="rgb(180, 180, 180)" style={{paddingRight: 30}} />*/}
                                            <TextInput
                                                placeholder={this.props.item.mobile}
                                                placeholderTextColor={'gray'}
                                                underlineColorAndroid='transparent'
                                                value={this.state.mobile}
                                                maxLength={11}
                                                style={{
                                                    height: 40,
                                                    backgroundColor: 'white',
                                                    paddingRight: 15,
                                                    width: 180,
                                                    color: 'gray',
                                                    fontSize: 14,
                                                    textAlign: 'right',

                                                }}
                                                // onChangeText={(text) => this.setState({text: text, fill: ++this.state.fill})}
                                                // onChangeText={(text) => this.handleChange(text)}

                                            />
                                        </View>
                                        <Text style={styles.label}>شماره موبایل</Text>
                                    </View>
                                </View>
                                <View style={[styles.textContainer, {borderBottomColor:  'rgb(237, 237, 237)', borderBottomWidth:  1 ,borderRadius: 10}]}>
                                    <TouchableOpacity onPress={()=> this.setState({showPicker: true})} style={{width: '50%'}}>
                                        {
                                            this.state.selectedStartDate !== null ?
                                                <Text style={[styles.bodyValue, {textAlign: 'right', fontSize: 13, color: 'gray', paddingLeft: 15}]}>{this.state.selectedStartDate !== null ? moment_jalaali(this.state.selectedStartDate).format('jYYYY/jM/jD') : null}</Text>
                                                :
                                                <Text style={[styles.bodyValue, {textAlign: 'right', fontSize: 13, color: 'gray', paddingLeft: 15}]}>تاریخ انقضا یادآوری </Text>
                                        }
                                    </TouchableOpacity>
                                    <Text style={styles.label}>تاریخ انقضا</Text>
                                </View>

                            </View> :
                            <View style={styles.body}>
                                <View style={[styles.regimContainer, {justifyContent: 'flex-end'}]}>
                                    <View style={styles.bodyLeft}>
                                        <Text style={[styles.bodyValue, {fontSize: 12}]}>{this.props.item.fname} {this.props.item.lname}</Text>
                                        <Text style={styles.bodyLabel}>نام و نام خانوادگی :  </Text>
                                    </View>
                                    <View style={styles.bodyLeft}>
                                        <Text style={styles.bodyValue}>{this.props.item.mobile}</Text>
                                        <Text style={styles.bodyLabel}>شماره موبایل : </Text>
                                    </View>
                                    <View style={styles.bodyLeft}>
                                        <Text style={styles.bodyValue}>{moment_jalaali(this.props.item.expired_date).format('jYYYY/jM/jD')}</Text>
                                        <Text style={styles.bodyLabel}>تاریخ انقضا یادآوری :  </Text>
                                    </View>

                                </View>
                            </View>
                    }
                    <View style={styles.footer}>
                        {
                            this.state.edit?
                                <TouchableOpacity onPress={() => this.setState({edit: false})} style={styles.iconLeftEditContainer}>
                                    <MIcon name="close" size={18} color="white" />
                                </TouchableOpacity> :
                                <TouchableOpacity onPress={() => this.deleteReminder()}  style={styles.iconLeftContainer}>
                                    <Icon name="trash-o" size={18} color="white" />
                                </TouchableOpacity>
                        }
                        {
                            this.state.edit?
                                <TouchableOpacity onPress={() => this.setState({edit: false}, () => {this.updateReminder()})} style={styles.iconRightEditContainer}>
                                    <FIcon name="check" size={18} color="white" />
                                </TouchableOpacity> :
                                this.state.loading ?
                                    <View>
                                        <ActivityIndicator
                                            size={30}
                                            animating={true}
                                            color='blue'
                                        />
                                    </View> :
                                    <TouchableOpacity onPress={() => this.setState({edit: !this.state.edit})} style={styles.iconRightContainer}>
                                        <FIcon name="edit" size={10} color="white" style={{borderColor: 'white', borderWidth: 1, borderRadius: 20, padding: 3, marginRight: 5}} />
                                        <Text style={styles.edit}>ویرایش </Text>
                                    </TouchableOpacity>
                        }
                    </View>
                    <AlertModal
                        closeModal={(title) => this.closeModal2(title)}
                        modalVisible={this.state.showPicker}
                        onDateChange={(date) => this.onDateChange(date)}
                        onChange={()=> this.onChange()}
                    />
                    <AlertView
                        closeModal={(title) => this.closeModal(title)}
                        modalVisible={this.state.modalVisible}
                        onChange={(visible) => this.setState({modalVisible: false})}
                        title={this.state.dateR ? 'لطفا تاریخ انقضا را وارد نمایید' : (this.state.updateR ? 'یادآوری با موفقیت به روزرسانی شد' : (this.state.deleteR ? 'یادآوری با موفقیت حذف شد' : 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید' )) }
                    />
                </View>
            </View>
        );
    }
}
export default Remind;