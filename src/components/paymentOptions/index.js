import React from 'react';
import {Text, View, TouchableOpacity, Image, TextInput} from 'react-native';
import styles from './styles'
import saman from '../../assets/payment/saman.jpg'
import melat from '../../assets/payment/melat.png'
import ap from '../../assets/payment/ap.png'

export default class PaymentOptions extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            activeButton: 'right'
        };
    }
    render(){
        return (
            <View style={styles.container}>
                <View style={styles.paymentButtonContainer}>
                    <TouchableOpacity onPress={() => this.setState({activeButton: 'left'}, ()=> {this.props.paymentWay('card')})} style={[styles.paymentButton, {backgroundColor: this.state.activeButton === 'left' ? 'rgba(17, 103, 253, 1)' : 'white' ,  borderTopLeftRadius: 7, borderBottomLeftRadius: 7}]}>
                        <View>
                            <Text style={[styles.paymentText, {color: this.state.activeButton === 'left' ? 'white' : 'rgba(17, 103, 253, 1)', fontFamily: this.state.activeButton === 'left' ? 'IRANSansMobile(FaNum)' : 'IRANSansMobile(FaNum)' }]}>کارت به کارت</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({activeButton: 'center'}, ()=> { this.props.paymentWay('wallet')})} style={[styles.paymentButton, {backgroundColor: this.state.activeButton === 'center' ? 'rgba(17, 103, 253, 1)' : 'white' }]}>
                        <View>
                            <Text style={[styles.paymentText, {color: this.state.activeButton === 'center' ? 'white' : 'rgba(17, 103, 253, 1)', fontFamily: this.state.activeButton === 'center' ? 'IRANSansMobile(FaNum)' : 'IRANSansMobile(FaNum)' }]}>پرداخت از کیف پول</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({activeButton: 'right'}, ()=> { this.props.paymentWay('online')})} style={[styles.paymentButton, {backgroundColor: this.state.activeButton === 'right' ? 'rgba(17, 103, 253, 1)' : 'white',  borderTopRightRadius: 7, borderBottomRightRadius: 7 }]}>
                        <View>
                            <Text style={[styles.paymentText, {color: this.state.activeButton === 'right' ? 'white' : 'rgba(17, 103, 253, 1)', fontFamily: this.state.activeButton === 'right' ? 'IRANSansMobile(FaNum)' : 'IRANSansMobile(FaNum)' }]}>پرداخت اینترنتی</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                {
                    this.state.activeButton === 'right' ?
                        <View>
                            {/*<Text style={styles.label}>درگاه مورد نظر خود را انتخاب کنید</Text>*/}
                            {/*<View style={styles.portContainer}>*/}
                            {/*<TouchableOpacity onPress={() => null}>*/}
                            {/*<View style={styles.navContainer}>*/}
                            {/*<Image source={ap} style={styles.image} />*/}
                            {/*<Text style={styles.label}>آسان پرداخت</Text>*/}
                            {/*</View>*/}
                            {/*</TouchableOpacity>*/}
                            {/*<TouchableOpacity onPress={() => null}>*/}
                            {/*<View style={styles.navContainer}>*/}
                            {/*<Image source={melat} style={styles.image} />*/}
                            {/*<Text style={styles.label}>ملت</Text>*/}
                            {/*</View>*/}
                            {/*</TouchableOpacity>*/}
                            {/*<TouchableOpacity onPress={() => null}>*/}
                            {/*<View style={styles.navContainer}>*/}
                            {/*<Image source={saman} style={styles.image} />*/}
                            {/*<Text style={styles.label}>سامان</Text>*/}
                            {/*</View>*/}
                            {/*</TouchableOpacity>*/}
                            {/*</View>*/}
                        </View>
                        :
                        (
                            this.state.activeButton === 'center' ?
                                <View style={{alignItems: 'center', justifyContent: 'center', width: '100%'}}>
                                    <View style={{alignItems: 'center',
                                        justifyContent: 'center',
                                        width: '90%',
                                        height: 36,
                                        backgroundColor: 'rgb(246, 246, 246)',
                                        paddingRight: 15,
                                        borderWidth: 1,
                                        borderColor: 'lightgray',
                                        direction: 'rtl',
                                        marginBottom: 10,

                                        marginTop: 15}}>
                                        <Text style={{   fontSize: 13,
                                            color: 'black',
                                            fontFamily: 'IRANSansMobile(FaNum)'
                                        }}>
                                            {this.props.paymentValue}
                                        </Text>
                                    </View>
                                    <TouchableOpacity style={styles.searchButton} onPress={()=>null}>
                                        <Text style={styles.searchText}>پرداخت</Text>
                                    </TouchableOpacity>
                                </View>
                                :
                                <View style={{alignItems: 'center', justifyContent: 'center', width: '100%'}}>
                                    <Text style={{ fontFamily: 'IRANSansMobile(FaNum)', fontSize: 12, color: 'rgb(70, 70, 0)', paddingRight: 15, paddingBottom: 10}} >
                                        مبلغ قابل پرداخت را به شماره کارت زیر واریز نموده، و
                                        شماره پیگیری
                                        تراکنش را در کادر زیر وارد کنید.

                                    </Text>
                                    <View style={{width: '100%', flexDirection:'row', alignItems: 'center', justifyContent: 'space-around'}} >
                                        <Text style={{ fontFamily: 'IRANSansMobile(FaNum)', fontSize: 12, color: 'rgb(30, 30, 30)'}} >
                                            6037-1245-1324-1245
                                        </Text>
                                        <Text style={{ fontFamily: 'IRANSansMobile(FaNum)', fontSize: 10, color: 'rgb(50, 50, 50)'}} >
                                            به نام شرکت مادر بیمه گر
                                        </Text>
                                    </View>
                                    <View style={{alignItems: 'center',
                                        justifyContent: 'center',
                                        width: '90%',
                                        height: 36,
                                        backgroundColor: 'rgb(246, 246, 246)',
                                        paddingRight: 15,
                                        borderWidth: 1,
                                        borderColor: 'lightgray',
                                        direction: 'rtl',
                                        marginBottom: 10,
                                        marginTop: 5}}>
                                        <TextInput
                                            keyboardType="numeric"
                                            placeholder="کد پیگیری"
                                            placeholderTextColor={'gray'}
                                            underlineColorAndroid='transparent'
                                            value={this.state.text}
                                            style={{
                                                height: 42,
                                                paddingRight: 15,
                                                width: '90%',
                                                textAlign: 'center',
                                                // marginBottom: 10,
                                                // marginTop: 15,
                                                fontFamily: 'IRANSansMobile(FaNum)',
                                                fontSize: 13,
                                                color: 'black',
                                                borderRadius: 5
                                            }}
                                            onChangeText={(text) => this.setState({text})}
                                        />
                                    </View>
                                    <TouchableOpacity style={styles.searchButton} onPress={()=>null}>
                                        <Text style={styles.searchText}>پرداخت</Text>
                                    </TouchableOpacity>
                                </View>
                        )
                }
            </View>
        );
    }
}